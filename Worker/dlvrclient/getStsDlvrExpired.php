#!/usr/bin/php -q
<?php
require_once __DIR__ . "/../../Config/config.php";
require_once __DIR__ . "/../../App/Utils/Loging.php";
require_once __DIR__ . "/../../App/Model/FuncDB.php";
require_once __DIR__ . "/../../App/Utils/ParsData.php";
require_once __DIR__ . "/../../App/Utils/GetValue.php";
require_once __DIR__ . "/../../App/Utils/SendHttp.php";
require_once __DIR__ . "/../../App/Controller/GetFromDB.php";
require_once __DIR__ . "/../../App/Controller/SendToDB.php";
require_once __DIR__ . "/../../App/Controller/GetDlvrExpired.php";

$tmp = array();
if ($_SERVER['argc'] > 0) {
	for ($i=0;$i<$_SERVER['argc'];$i++) {
		$getArgv = explode("=",$_SERVER['argv'][$i]);
		switch(strtolower($getArgv[0])){
			case "threadno":
				$tmps = explode(",", $getArgv[1]);
				$tm1 = array("threadno" => $tmps);
				$tmp = array_merge($tmp, $tm1);
			break;
			case "srvc":
				$tm1 = array("srvc" => strtolower(str_replace(" ","",$getArgv[1])));
				$tmp = array_merge($tmp, $tm1);
			break;
			case "client":
				$tm1 = array("client" => strtolower(str_replace(" ","",$getArgv[1])));
				$tmp = array_merge($tmp, $tm1);
			break;
			case "prefix":
				$tm1 = array("prefix" => strtolower(str_replace(" ","",$getArgv[1])));
				$tmp = array_merge($tmp, $tm1);
			break;
		}// end switch
	}// end for
}
$this_year  = date("Y");
$this_month = date("m");
$prev_month = $this_month - 1;
if ($prev_month == "0") {
	$prev_month = "12";
	$prev_year  = $this_year - 1;
}else{
	$prev_month = str_pad($prev_month,2,"0",STR_PAD_LEFT);
	$prev_year  = $this_year;
}
$nowdate = $this_year."_".$this_month;
$prevdate= $prev_year."_".$prev_month;
$tbldate = array($nowdate, $prevdate);
// Define parameter
$arrParam = array(
	"logname"	=> "GETDLVREXPIRED_".strtoupper($tmp["prefix"])."_".strtoupper($tmp["client"])."_".strtoupper($tmp["srvc"]),
	"tbldate"	=> $tbldate,
	"thread_no"	=> $tmp["threadno"],
	"prefix"	=> $tmp["prefix"],
	"client"	=> $tmp["client"],
	"service"	=> $tmp["srvc"]
);
$sendData = new \App\Controller\GetDlvrExpired;
for($i=0;$i<$GLOBALS["maxloopdlvr"];$i++){
	$sendData->getDbDlvr($arrParam);
}
sleep(1);
?>