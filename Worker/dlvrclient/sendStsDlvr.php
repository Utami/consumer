#!/usr/bin/php -q
<?php
require_once __DIR__ . "/../../Config/config.php";
require_once __DIR__ . "/../../App/Utils/Loging.php";
require_once __DIR__ . "/../../App/Utils/ParsData.php";
require_once __DIR__ . "/../../App/Utils/SendHttp.php";
require_once __DIR__ . "/../../App/Utils/vendor/autoload.php";
require_once __DIR__ . "/../../App/Controller/SendDlvrFunc.php";
require_once __DIR__ . "/../../App/Controller/SendDlvr.php";

// Define parameter
$arrParam = array(
	"logname"	=> "SENDDLVR_BBCA",
	"queue"		=> "q.stsclient.bbca",
	"exchange"	=> "x.stsclient",
	"funcdata"	=> "processDlvr",
	"confmq"	=> $GLOBALS["CONF_MQ_SMS"]
);
$sendData = new \App\Controller\SendDlvr;
$sendData->sendMqDlvr($arrParam);
?>
