#!/usr/bin/php -q
<?php
require_once __DIR__ . "/../../Config/config.php";
require_once __DIR__ . "/../../App/Utils/Loging.php";
require_once __DIR__ . "/../../App/Model/FuncDB.php";
require_once __DIR__ . "/../../App/Utils/GetValue.php";
require_once __DIR__ . "/../../App/Utils/SendHttp.php";
require_once __DIR__ . "/../../App/Controller/GetFromDB.php";
require_once __DIR__ . "/../../App/Controller/SendToDB.php";
require_once __DIR__ . "/../../App/Controller/GetDlvr.php";

// Define parameter
$arrParam = array(
	"logname"	=> "GETDLVR_BROD_ISAT",
	"tahun"	=> date("Y"),
	"bulan"	=> date("m"),
	"thread_no"	=> "1",
	"service"	=> "brod",
	"prefix"	=> "isat"
);
$sendData = new \App\Controller\GetDlvr;
$sendData->getDbDlvr($arrParam);
?>
