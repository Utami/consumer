#!/usr/bin/php -q
<?php
require_once __DIR__ . "/../../Config/config.php";
require_once __DIR__ . "/../../App/Utils/Loging.php";
require_once __DIR__ . "/../../App/Model/FuncDB.php";
require_once __DIR__ . "/../../App/Utils/ParsData.php";
require_once __DIR__ . "/../../App/Utils/GetValue.php";
require_once __DIR__ . "/../../App/Utils/SendHttp.php";
require_once __DIR__ . "/../../App/Controller/GetFromDB.php";
require_once __DIR__ . "/../../App/Controller/SendToDB.php";
require_once __DIR__ . "/../../App/Controller/GetDlvrWhatsapp.php";

$tmp = array();
if ($_SERVER['argc'] > 0) {
	for ($i=0;$i<$_SERVER['argc'];$i++) {
		$getArgv = explode("=",$_SERVER['argv'][$i]);
		switch(strtolower($getArgv[0])){
			case "threadno":
				$tmps = explode(",", $getArgv[1]);
				$tm1 = array("threadno" => $tmps);
				$tmp = array_merge($tmp, $tm1);
			break;
			case "srvc":
				$tm1 = array("srvc" => strtolower(str_replace(" ","",$getArgv[1])));
				$tmp = array_merge($tmp, $tm1);
			break;
			case "client":
				$tm1 = array("client" => strtolower(str_replace(" ","",$getArgv[1])));
				$tmp = array_merge($tmp, $tm1);
			break;
			case "prefix":
				$tm1 = array("prefix" => strtolower(str_replace(" ","",$getArgv[1])));
				$tmp = array_merge($tmp, $tm1);
			break;
		}// end switch
	}// end for
}

// Define parameter
$arrParam = array(
	"logname" => "GETDLVR_SOCMED_".strtoupper($tmp["client"])."_".strtoupper($tmp["srvc"]),
	"tahun"	=> date("Y"),
	"bulan"	=> date("m"),
	"thread_no" => $tmp["threadno"],
	"service" => $tmp["srvc"],
	"client"	=> $tmp["client"],
	"prefix" => "socmed"
);
$sendData = new \App\Controller\GetDlvrWhatsapp;
for($i=0;$i<$GLOBALS["maxloopdlvr"];$i++){
	$sendData->getDbDlvr($arrParam);
}
sleep(1);
?>
