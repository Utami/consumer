#!/usr/bin/php -q
<?php
require_once __DIR__ . "/../../Config/config.php";
require_once __DIR__ . "/../../App/Utils/Loging.php";
require_once __DIR__ . "/../../App/Model/FuncDB.php";
require_once __DIR__ . "/../../App/Utils/ParsData.php";
require_once __DIR__ . "/../../App/Utils/GetValue.php";
require_once __DIR__ . "/../../App/Utils/SendHttp.php";
require_once __DIR__ . "/../../App/Utils/FuncTelegram.php";
require_once __DIR__ . "/../../App/Utils/FuncLine.php";
require_once __DIR__ . "/../../App/Utils/FuncFacebook.php";
require_once __DIR__ . "/../../App/Utils/vendor/autoload.php";
require_once __DIR__ . "/../../App/Controller/SendToDB.php";
require_once __DIR__ . "/../../App/Controller/SendFunc.php";
require_once __DIR__ . "/../../App/Controller/SendData.php";

// Define parameter
$arrParam = array(
	"logname"	=> "SMSNOTITSEL",
	"queue"		=> "q.noti.tsel",
	"exchange"	=> "x.recv",
	"funcdata"	=> "processSms",
	"confmq"	=> $GLOBALS["CONF_MQ_SMS"]
);
$sendData = new \App\Controller\SendData;
$sendData->sendMqData($arrParam);
?>
