#!/usr/bin/php -q
<?php
require_once __DIR__ . "/../../Config/config.php";
require_once __DIR__ . "/../../App/Utils/Loging.php";
require_once __DIR__ . "/../../App/Utils/vendor/autoload.php";
require_once __DIR__ . "/../../App/Utils/SendHttp.php";
require_once __DIR__ . "/../../App/Controller/StatusSent.php";

// Define parameter
$tmp = array();
if ($_SERVER['argc'] > 0) {
	for ($i=0;$i<$_SERVER['argc'];$i++) {
		$getArgv = explode("=",$_SERVER['argv'][$i]);
		switch(strtolower($getArgv[0])){
			case "prefix":
				$tm1 = array("prefix" => strtolower(str_replace(" ","",$getArgv[1])));
				$tmp = array_merge($tmp, $tm1);
			break;
		}// end switch
	}// end for
}
$arrParam = array(
	"logname"	=> "sendStatusDlvr_".$tmp["prefix"],
	"queue"		=> "q.stsclient.".$tmp["prefix"],
	"exchange"	=> "x.stsclient",
	"funcdata"	=> "processStsDlvr",
	"prefix"	=> strtolower($tmp["prefix"]),
	"confmq"	=> $GLOBALS["CONF_MQ_SMS"]
);
$sendData = new \App\Controller\StatusSent;
$sendData->sentStatus($arrParam);
?>