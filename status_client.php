<?php
require_once __DIR__ . "/Config/config.php";
require_once __DIR__ . "/App/Model/FuncDB.php";
require_once __DIR__ . "/App/Utils/Loging.php";
require_once __DIR__ . "/App/Controller/SaveSent.php";

$json_request = json_decode(file_get_contents('php://input'), true);
$saveSts = new \App\Controller\SaveSent;
$saveSts->saveStatus($json_request);
?>