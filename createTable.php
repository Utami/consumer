#!/usr/bin/php -q
<?php
require_once __DIR__ . "/Config/config.php";
require_once __DIR__ . "/App/Utils/Loging.php";
require_once __DIR__ . "/App/Model/FuncDB.php";
require_once __DIR__ . "/App/Controller/CreateTable.php";

$this_year  = date("Y");
$this_month = date("m");
$next_month = $this_month + 1;
if ($next_month == "13") {
	$next_month = "01";
	$next_year  = $this_year + 1;
}else{
	$next_month = str_pad($next_month,2,"0",STR_PAD_LEFT);
	$next_year  = $this_year;
}
//$next_month="09";

$arrParam = array(
	"m"			=> $next_month,
	"y"			=> $next_year,
	//"dbreport"	=> array("bbri","bbca","bnii","bnli","cidx"),
	"dbreport"	=> array("cidx"),
	//"dbhistory"	=> array("bbri_history","bbca_history","bnii_history","bnli_history","cidx_history"),
	"dbhistory"	=> array("cidx_history"),
	"dbtrxdlvr"	=> $GLOBALS["DB_CONFIG_TRX"]
);
$settable = new \App\Controller\CreateTable;
$settable->setTable($arrParam);
?>
