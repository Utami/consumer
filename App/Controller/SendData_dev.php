<?php

namespace App\Controller;
use \App\Utils\Loging;
use \App\Utils\ParsData;
use \App\Utils\GetValue;
use \App\Utils\FuncTelegram;
use \App\Utils\FuncLine;
use \App\Utils\FuncFacebook;
use \App\Utils\SendHttp;
use PhpAmqpLib\Connection\AMQPConnection;

class SendData extends \App\Controller\SendFunc {
	protected $consumer_tag = "consumer";
	protected $logdata;
	protected $parsVal;
	protected $getVal;
	protected $sendProvider;
	protected $sendTelegram;
	protected $sendLine;
	protected $sendFb;
	
	public function __construct () {
		$this->logdata	= new \App\Utils\Loging;
		$this->parsVal	= new \App\Utils\ParsData;
		$this->getVal	= new \App\Utils\GetValue;
		$this->sendTelegram	= new \App\Utils\FuncTelegram;
		$this->sendLine	= new \App\Utils\FuncLine;
		$this->sendFb	= new \App\Utils\FuncFacebook;
		$this->sendProvider	= new \App\Utils\SendHttp;
	}

	public function sendMqData ($arrData) {
		$GLOBALS["logname"] = $arrData["logname"];
		//$this->logdata->write(__FUNCTION__, "REQUEST=".json_encode($arrData), $GLOBALS["logname"]);
		$conn = new AMQPConnection($arrData["confmq"]["host"], $arrData["confmq"]["port"], $arrData["confmq"]["user"], $arrData["confmq"]["pass"], $arrData["confmq"]["vhost"]);
		if($conn){
			$ch = $conn->channel();
			$ch->queue_declare($arrData["queue"], false, true, false, false);
			$ch->exchange_declare($arrData["exchange"], 'topic', false, true, false);
			$ch->queue_bind($arrData["queue"], $arrData["exchange"]);
			$ch->basic_consume($arrData["queue"], $this->consumer_tag, false, false, false, false, array($this, $arrData["funcdata"]));
			register_shutdown_function(array($this, 'shutdown'), $ch, $conn);
			
			while (count($ch->callbacks)) {
				$ch->wait();
			}
		}
	}// end sendMqData
	
	public function processSms ($msg) {
		$data	= json_decode($msg->body);
		$arrData=$this->parsVal->getDataSms($data);
		
		$nmlog = strtoupper($arrData["client_prefix_dbname"]."_".$arrData["operator_prefix_db"]);
		$GLOBALS["logproces"] = "SMS_".$nmlog;
		if (!file_exists("/var/www/appSend/stopworker")) {
			$this->setDataSend($arrData);
			$sendToOpr = $this->sendToOpr();
			
			// Recv delete count
			$msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
			//die();
			// quit
			if(strtolower($arrData["operator_prefix_db"]) == "isat"){
				$msg->delivery_info['channel']->basic_cancel($msg->delivery_info['consumer_tag']);
			}
		}
	}// end processSms
	
	public function processEmail ($msg) {
		$data	= json_decode($msg->body);
		$arrData=$this->parsVal->getDataEmail($data);
		
		$nmlog = strtoupper($arrData["client_prefix_dbname"]."_".$arrData["operator_prefix_db"]);
		$GLOBALS["logproces"] = "EMAIL_".$nmlog;
		
		$this->setDataSend($arrData);
		$sendEmail = $this->sendToEmail();
		
		// Recv delete count
		$msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
		// quit
		//$msg->delivery_info['channel']->basic_cancel($msg->delivery_info['consumer_tag']);
	}
	
	public function processSocmed ($msg) {
		$data	= json_decode($msg->body);
		//print_r($data);echo "\n";die();
		$arrData=$this->parsVal->getDataArrSocmed($data);
		//print_r($arrData);echo "\n";die();
		$tipe = strtolower($arrData["operator_prefix_db"]);
		$nmlog = strtoupper($arrData["client_prefix_dbname"]."_".$tipe);
		$GLOBALS["logproces"] = "SOCMED_".$nmlog;
		$this->setDataSend($arrData);
		switch($tipe){
			//case "telegram": $sendSocmed = $this->sendToTelegram(); break;
			//case "line": $sendSocmed = $this->sendToLine(); break;
			//case "facebook": $sendSocmed = $this->sendToFacebook(); break;
			case "whatsapp": $sendSocmed = $this->sendToWhatsapp(); break;
		}// end switch
		// Recv delete count
		$msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
		// quit
		//$msg->delivery_info['channel']->basic_cancel($msg->delivery_info['consumer_tag']);
	}// end processEmail
	
	public function shutdown($ch, $conn) {
		$ch->close();
		$conn->close();
	}
}
?>
