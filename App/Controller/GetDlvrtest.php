<?php

namespace App\Controller;
use \App\Utils\Loging;
use \App\Utils\GetValue;
use \App\Utils\SendHttp;
use \App\Controller\SendToDB;

class GetDlvr extends \App\Controller\GetFromDB{
	protected $consumer_tag = "consumer";
	protected $logdata;
	protected $getVal;
	protected $insData;
	protected $sendhttp;
	
	public function __construct () {
		$this->logdata	= new \App\Utils\Loging;
		$this->getVal	= new \App\Utils\GetValue;
		$this->insData	= new \App\Controller\SendToDB;
		$this->sendhttp	= new \App\Utils\SendHttp;
	}

	public function getDbDlvr ($arrData) {
		$GLOBALS["logname"] = $arrData["logname"];
		$tblDlvr	= array("thn"=>$arrData["tahun"],"bln"=>$arrData["bulan"],"srvc"=>"","prefix"=>$arrData['prefix'],"nmtbl"=>"delivery");
		$nmTblDlvr	= $this->getVal->getNameTable($tblDlvr);
		
		//$sQdlvr	= "SELECT * FROM `".$nmTblDlvr."` where `engine_name_recv`='".$arrData['service']."' AND ( RIGHT(`id`,1) IN (".$arrData['thread_no'].")) ";
		$sQdlvr	= "SELECT * FROM `".$nmTblDlvr."` where `time_expire` >= NOW() ORDER BY `id` ASC LIMIT 100";
		$cQdlvr= array(
			"q"	=> $sQdlvr,
			"dbname"	=> $GLOBALS["DB_CONFIG_TRX"],
			"confdb"	=> $GLOBALS["DB_TRX_CONFIG"]
		);
		$this->setData($cQdlvr);
		$gdata = $this->get_data_db();
		$tFromOpr = "q-deliveryfromoperator";
		if(count($gdata[0]) > 0){
			foreach($gdata as $v){
				$sFromOpr = "SELECT * FROM `".$tFromOpr."` where `prvd_message_id`='".$v["prvd_message_id"]."'";
				$cFromOpr = array(
					"q"	=> $sFromOpr,
					"dbname"	=> $GLOBALS["DB_STATUS_CONFIG2"]["database_name"],
					"confdb"	=> $GLOBALS["DB_STATUS_CONFIG2"]
				);
				$this->setData($cFromOpr);
				$gstatus = $this->get_data_db();
				if(count($gstatus[0]) > 0){ // search from 172.17.0.147
					$arrDlvr = array(
						"fqdlvr" => $v,
						"fqfromopr" => $gstatus,
						"prefix" => $arrData['prefix'],
						"tbldlvr" => $nmTblDlvr,
						"oprdlvr" => $tFromOpr,
						"confdb" => $GLOBALS["DB_STATUS_CONFIG2"]
					);
					$this->genDlvr($arrDlvr);
				}else{ // search from 172.17.0.89
					if($v["operator_prefix_db"] != "other"){
						$tFromOpr = "q-deliveryfromoperator-".$v["operator_prefix_db"];
					}
					$sFromOpr = "SELECT * FROM `".$tFromOpr."` where `prvd_message_id`='".$v["prvd_message_id"]."'";
					//$sFromOpr = "SELECT * FROM `".$tFromOpr."` where `prvd_message_id`='628569787371720180426151428700'";
					$cFromOpr = array(
						"q"	=> $sFromOpr,
						"dbname"	=> $GLOBALS["DB_STATUS_CONFIG"]["database_name"],
						"confdb"	=> $GLOBALS["DB_STATUS_CONFIG"]
					);
					$this->setData($cFromOpr);
					$gstatus = $this->get_data_db();
					if(count($gstatus[0]) > 0){
						$arrDlvr = array(
							"fqdlvr" => $v,
							"fqfromopr" => $gstatus,
							"prefix" => $arrData['prefix'],
							"tbldlvr" => $nmTblDlvr,
							"oprdlvr" => $tFromOpr,
							"confdb" => $GLOBALS["DB_STATUS_CONFIG"]
						);
						$this->genDlvr($arrDlvr);
					}else{
						$arrUnsent = array();
						$arrUnsent[0] = array(
							"prvd_status_delivery" => "6",
							"time_delivery" => date("Y-m-d H:i:s"),
							"prvd_status_other"=>"",
							"prvd_message_id_real"=>"",
							"prvd_status_delivery_real"=>"",
							"prvd_status_delivery_err_real"=>"",
							"ip_remote_addr"=>"",
							"ip_remote_host"=>"",
							"id"=>"kosong"
						);
						$arrDlvr = array(
							"fqdlvr" => $v,
							"fqfromopr" => $arrUnsent,
							"prefix" => $arrData['prefix'],
							"tbldlvr" => $nmTblDlvr,
							"oprdlvr" => $tFromOpr,
							"confdb" => $GLOBALS["DB_STATUS_CONFIG"]
						);
						$this->genDlvr($arrDlvr);
					}
				}// end if getstsdlvr
			}// end foreach gdata
		}// end if count gdata
		sleep(1);
	}// end getDbDlvr
	
	public function genDlvr ($arrData) {
		$v = $arrData["fqdlvr"];
		$nmTblDlvr = $arrData["tbldlvr"];
		$tFromOpr = $arrData["oprdlvr"];
		$urldlvr = $v["url_client_delivery_report"];
		$tblSmsDlvr = array("thn"=>$v["tahun"],"bln"=>$v["bulan"],"srvc"=>$v['engine_name_recv'],"prefix"=>$arrData['prefix'],"nmtbl"=>"smsdelivery");
		$nmTblSmsDlvr = $this->getVal->getNameTable($tblSmsDlvr);
		$nmClient = $v["client_prefix_dbname"];
		$sendParam=unserialize($v['sendingParam']);
		foreach($arrData["fqfromopr"] as $vs){
			$paramVal["delivery"] = $vs["prvd_status_delivery"];
			$paramVal["type_delivery"] = $v['type_delivery'];
			$paramVal["statusdlvr_delivered"] = $sendParam["statusdlvr_delivered"];
			$paramVal["statusdlvr_undelivered"] = $sendParam["statusdlvr_undelivered"];
			$paramVal["statusdlvr_pending"] = $sendParam["statusdlvr_pending"];
			$stsdlvr = $this->getVal->GetValDeliveryResponse($paramVal);
			$timeDlvr= $vs["time_delivery"];
			
			$arrSmsDlvr = array(
				"time_delivery"=>$timeDlvr,
				"prvd_status_delivery"=>$vs["prvd_status_delivery"],
				"prvd_status_others"=>$vs["prvd_status_other"],
				"prvd_message_id_real"=>$vs["prvd_message_id_real"],
				"prvd_status_delivery_real"=>$vs["prvd_status_delivery_real"],
				"prvd_status_delivery_err_real"=>$vs["prvd_status_delivery_err_real"],
				"status_delivery"=>$stsdlvr["status_delivery"],
				"code_sms"=>$v['code_sms'],
				"f7_tid"=>$v["f7_tid"],
				"prvd_message_id"=>$v['prvd_message_id'],
				"ip_remote_addr"=>$vs["ip_remote_addr"],
				"ip_remote_host"=>$vs["ip_remote_host"],
				"operator_prefix_db"=>$v["operator_prefix_db"],
				"client_prefix_dbname"=>$nmClient,
				"client_code_serv"=>$v['client_code_serv'],
				"engine_name_recv"=>$v['engine_name_recv'],
				"ref_id"=>$v['ref_id']
			);
			$inSmsDlvr	= array(
				"arrdata"	=> $arrSmsDlvr,
				"tablename"	=> $nmTblSmsDlvr,
				"dbname"	=> $nmClient,
				"confdb"	=> $GLOBALS["DB_REPORT_CONFIG"]
			);
			$GLOBALS["logproces"] = $GLOBALS["logname"];
			$this->insData->setData($inSmsDlvr);
			//$this->insData->insertData();
			
			//==== start delete from qdelivery
			$sDelDlvr = "DELETE FROM `".$nmTblDlvr."` where `id`='".$v["id"]."'";
			$delDlvr = array(
				"q"	=> $sDelDlvr,
				"dbname"	=> $GLOBALS["DB_CONFIG_TRX"],
				"confdb"	=> $GLOBALS["DB_TRX_CONFIG"]
			);
			$this->insData->setData($delDlvr);
			//$this->insData->deleteData();
			//==== end delete from qdelivery
			
			//==== start delete from qdeliveryfromoperator
			if($vs["id"] != "kosong"){
				$sDelFromOpr = "DELETE FROM `".$tFromOpr."` where `id`='".$vs["id"]."'";
				$delFromOpr = array(
					"q"	=> $sDelFromOpr,
					"dbname"	=> $arrData["confdb"]["database_name"],
					"confdb"	=> $arrData["confdb"]
				);
			}
			//$this->insData->setData($delFromOpr);
			//$this->insData->deleteData();
			//==== end delete from qdeliveryfromoperator
			
			$arrStsDlvr = array(
				"ref_id" => $v['ref_id'],
				"code_sms" => $v['code_sms'],
				"type_status" => "dlvr",
				"status" => $stsdlvr["status_delivery"],
				"channel" => "SMS",
				"time_dlvr" => strtotime($timeDlvr)
			);
			$dtStsDlvr = json_encode($arrStsDlvr);
			//$hitCallBack = $this->sendhttp->sendBackData($dtStsDlvr, $urldlvr);
			$tmplog = "ref_id :".$v['ref_id'].", Callback : url ".$urldlvr." ".$dtStsDlvr."\n";
			$this->logdata->write(__FUNCTION__, $tmplog, $GLOBALS["logproces"]);
		}// end foreach gstatus
	}// end genDlvr
}// end GetDlvr
?>