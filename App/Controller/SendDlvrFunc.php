<?php
namespace App\Controller;
class SendDlvrFunc {
	protected $dataSend;
	
	public function setDataSend($dt){ $this->dataSend = $dt; }// end setData
	public function getDataSend(){ return $this->dataSend; }// end getData
	
	public function sendToClient () {
		$arrData= $this->getDataSend();
		$dtSend	= json_encode($arrData);
		if(isset($arrData["url"]) && !empty($arrData["url"]) && $arrData["url"] != ""){
			$dt		= json_decode($arrData["dtsts"]);
			$check	= array_filter(get_object_vars($dt));
			if(count($check) > 1){
				$hitDlvr= $this->sendProvider->sendBackData($arrData["dtsts"], $arrData["url"]);
				$tmplog	= "Status Delivery :".$dtSend."\n";
				$this->logdata->write(__FUNCTION__, $tmplog, $GLOBALS["logproces"]);
			}else{
				$tmplog	= "Status Delivery Data Status Empty : ".$dtSend."\n";
				$this->logdata->write(__FUNCTION__, $tmplog, $GLOBALS["logproces"]);
			}// end if check data status
		}else{
			$tmplog	= "Status Delivery Url Empty : ".$dtSend."\n";
			$this->logdata->write(__FUNCTION__, $tmplog, $GLOBALS["logproces"]);
		}// end if check url
	}// end sendToClient
}// end class SendFunc
?>