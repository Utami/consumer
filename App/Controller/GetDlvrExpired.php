<?php

namespace App\Controller;
use \App\Utils\Loging;
use \App\Utils\ParsData;
use \App\Utils\GetValue;
use \App\Utils\SendHttp;
use \App\Controller\SendToDB;

class GetDlvrExpired extends \App\Controller\GetFromDB{
	protected $consumer_tag = "consumer";
	protected $logdata;
	protected $parsVal;
	protected $getVal;
	protected $insData;
	protected $sendProvider;

	public function __construct () {
		$this->logdata	= new \App\Utils\Loging;
		$this->parsVal	= new \App\Utils\ParsData;
		$this->getVal	= new \App\Utils\GetValue;
		$this->insData	= new \App\Controller\SendToDB;
		$this->sendProvider	= new \App\Utils\SendHttp;
	}

	public function getDbDlvr ($arrData) {
		$GLOBALS["logname"] = $arrData["logname"];
		$GLOBALS["logproces"] = $GLOBALS["logname"];
		$threadno = "";
		foreach($arrData["thread_no"] as $v ){ $threadno.= "'".$v."',";}
		$threadno = substr($threadno, 0, -1);

		foreach($arrData["tbldate"] as $vdt){
			$edate = explode("_", $vdt);
			$tblDlvr = array("thn"=>$edate[0],"bln"=>$edate[1],"srvc"=>$arrData['service'],"prefix"=>$arrData['prefix'],"nmtbl"=>"delivery");
			$nmTblDlvr	= $this->getVal->getNameTable($tblDlvr);

			$sQdlvr	= "SELECT * FROM `".$nmTblDlvr."` where ( RIGHT(`id`,1) IN (".$threadno.")) ";
			$sQdlvr.= "AND `client_prefix_dbname`='".$arrData['client']."' AND `time_expire` < NOW() ORDER BY `id` ASC LIMIT 100";
			$cQdlvr= array(
				"q"	=> $sQdlvr,
				"dbname" => $GLOBALS["DB_CONFIG_TRX"],
				"confdb" => $GLOBALS["DB_TRX_CONFIG"]
			);
			$this->setData($cQdlvr);
			$gdata = $this->get_data_db();
			if($gdata != "failed" && count($gdata) > 0){
				foreach($gdata as $v){
					switch($arrData['prefix']){
						case "tsel":case "isat":case "xl":case "other":
							$arrSearch = array(
								"prm" => $arrData,
								"tbldlvr" => $nmTblDlvr,
								"dtinq" => $v
							);
							$this->getSmsDlvr($arrSearch);
						break;
						case "email":
							$arrSearch = array(
								"prm" => $arrData,
								"tbldlvr" => $nmTblDlvr,
								"dtinq" => $v
							);
							$this->getEmailSocmedDlvr($arrSearch);
						break;
						default:
							$arrSearch = array(
								"prm" => $arrData,
								"tbldlvr" => $nmTblDlvr,
								"dtinq" => $v
							);
							$this->getEmailSocmedDlvr($arrSearch);
					}// end switch
				}// end foreach gdata
			}// end if count gdata
			sleep(1);
		}// get sts dlvr
	}// end getDbDlvr

	public function getEmailSocmedDlvr ($arrData) {
		$prefixtbl = "socmed";
		if($arrData["dtinq"]["operator_prefix_db"] == "email"){ $prefixtbl = "email"; }
		$tFromOpr = "q-deliveryfromoperator-".$prefixtbl;
		$sFromOpr = "SELECT * FROM `".$tFromOpr."` where `prvd_message_id`='".$arrData["dtinq"]["prvd_message_id"]."'";
		$cFromOpr = array(
			"q"	=> $sFromOpr,
			"dbname" => $GLOBALS["DB_CONFIG_TRX"],
			"confdb" => $GLOBALS["DB_TRX_CONFIG"]
		);
		$this->setData($cFromOpr);
		$gstatus = $this->get_data_db();
		if($gstatus != "failed" && count($gstatus) > 0){
			$arrDlvr = array(
				"dt" => $arrData["prm"],
				"dlvr" => $arrData["dtinq"],
				"sts" => $gstatus,
				"tbldlvr" => $arrData["tbldlvr"],
				"oprdlvr" => $tFromOpr
			);
			$this->genEmailSocmedDlvr($arrDlvr);
		}else{ // sts unkwn
			if(isset($arrData["dtinq"]["prvd_status_delivery"])){
				$arrUnsent = array();
				$arrUnsent[0] = array(
					"prvd_status_delivery" => $arrData["dtinq"]["prvd_status_delivery"],
					"time_delivery" => $arrData["dtinq"]["time_delivery"],
					"prvd_status_other"=> $arrData["dtinq"]["prvd_status_other"],
					"prvd_message_id_real"=> $arrData["dtinq"]["prvd_message_id_real"],
					"prvd_status_delivery_real"=> $arrData["dtinq"]["prvd_status_delivery_real"],
					"prvd_status_delivery_err_real"=> $arrData["dtinq"]["prvd_status_delivery_err_real"],
					"ip_remote_addr"=> $arrData["dtinq"]["ip_remote_addr"],
					"ip_remote_host"=> $arrData["dtinq"]["ip_remote_host"],
					"id"=> $arrData["dtinq"]["id"]
				);
				$arrDlvr = array(
					"dt" => $arrData["prm"],
					"dlvr" => $arrData["dtinq"],
					"sts" => $arrUnsent,
					"tbldlvr" => $arrData["tbldlvr"],
					"oprdlvr" => $tFromOpr
				);
			}else{
				$arrUnsent = array();
				$arrUnsent[0] = array(
					"prvd_status_delivery" => "6",
					"time_delivery" => date("Y-m-d H:i:s"),
					"prvd_status_other"=>"",
					"prvd_message_id_real"=>"",
					"prvd_status_delivery_real"=>"",
					"prvd_status_delivery_err_real"=>"",
					"ip_remote_addr"=>"",
					"ip_remote_host"=>"",
					"id"=>"kosong"
				);
				$arrDlvr = array(
					"dt" => $arrData["prm"],
					"dlvr" => $arrData["dtinq"],
					"sts" => $arrUnsent,
					"tbldlvr" => $arrData["tbldlvr"],
					"oprdlvr" => $tFromOpr
				);
			}
			$this->genEmailSocmedDlvr($arrDlvr);
		}
	}// end getEmailSocmedDlvr

	public function genEmailSocmedDlvr ($v) {
		$tblSmsDlvr = array("thn"=>$v["dlvr"]["tahun"],"bln"=>$v["dlvr"]["bulan"],"srvc"=>$v["dt"]['service'],"prefix"=>$v["dt"]['prefix'],"nmtbl"=>"smsdelivery");
		$nmTblSmsDlvr = $this->getVal->getNameTable($tblSmsDlvr);
		$urldlvr = $v["dlvr"]["url_client_delivery_report"];
		$nmClient = $v["dlvr"]["client_prefix_dbname"];
		foreach($v["sts"] as $vdlvr){
			// $stsdlvr = $this->getVal->getEmailSts($vdlvr["prvd_status_delivery"]); //sebelum perubahan
			if($vdlvr["prvd_status_delivery"] == "6" || $vdlvr["prvd_status_delivery"] == 6){
				$vdlvr["prvd_status_delivery"] = "SPRINT DELIVERED";
				$stsdlvr="1"; //perubahan dari Ahmad
			}else {
				$stsdlvr = $this->getVal->getEmailSts($vdlvr["prvd_status_delivery"]);
			}
			/*if($stsdlvr == "1"){
				$uptSmsDlvr	= array(
					"arrdata"	=> array("prvd_status_delivery" => $stsdlvr),
					"dtwhere"	=> array("id" => $v["dlvr"]['id']),
					"tablename"	=> $v["tbldlvr"],
					"dbname"	=> "mobile_trx",
					"confdb"	=> $GLOBALS["DB_REPORT_CONFIG"]
				);
				$this->setData($uptSmsDlvr);
				$this->updateData();
			}*/

			$arrSmsDlvr = array(
				"time_delivery"=>$vdlvr["time_delivery"],
				"prvd_status_delivery"=>$vdlvr["prvd_status_delivery"],
				"prvd_status_others"=>$vdlvr["prvd_status_other"],
				"prvd_message_id_real"=>$vdlvr["prvd_message_id_real"],
				"prvd_status_delivery_real"=>$vdlvr["prvd_status_delivery_real"],
				"prvd_status_delivery_err_real"=>$vdlvr["prvd_status_delivery_err_real"],
				"status_delivery"=>$stsdlvr,
				"code_sms"=>$v["dlvr"]['code_sms'],
				"f7_tid"=>$v["dlvr"]["f7_tid"],
				"prvd_message_id"=>$v["dlvr"]['prvd_message_id'],
				"ip_remote_addr"=>$vdlvr["ip_remote_addr"],
				"ip_remote_host"=>$vdlvr["ip_remote_host"],
				"operator_prefix_db"=>$v["dlvr"]["operator_prefix_db"],
				"client_prefix_dbname"=>$v["dlvr"]["client_prefix_dbname"],
				"client_code_serv"=>$v["dlvr"]['client_code_serv'],
				"engine_name_recv"=>$v["dlvr"]['engine_name_recv'],
				"ref_id"=>$v["dlvr"]['ref_id']
			);
			$inSmsDlvr	= array(
				"arrdata"	=> $arrSmsDlvr,
				"tablename"	=> $nmTblSmsDlvr,
				"dbname"	=> $v["dlvr"]["client_prefix_dbname"],
				"confdb"	=> $GLOBALS["DB_REPORT_CONFIG"]
			);
			$this->insData->setData($inSmsDlvr);
			$this->insData->insertData();

			//==== start delete from qdelivery
			//if($vdlvr["prvd_status_delivery"] == "CLICKED"){
				$sDelDlvr = "DELETE FROM `".$v["tbldlvr"]."` where `id`='".$v["dlvr"]["id"]."'";
				$delDlvr = array(
					"q"	=> $sDelDlvr,
					"dbname" => $GLOBALS["DB_CONFIG_TRX"],
					"confdb" => $GLOBALS["DB_TRX_CONFIG"]
				);
				$this->insData->setData($delDlvr);
				$this->insData->deleteData();
			//}
			//==== end delete from qdelivery

			//==== start delete from qdeliveryfromoperator
			if($vdlvr["id"] != "kosong"){
				$sDelFromOpr = "DELETE FROM `".$v["oprdlvr"]."` where `id`='".$vdlvr["id"]."'";
				$delFromOpr = array(
					"q"	=> $sDelFromOpr,
					"dbname" => $GLOBALS["DB_CONFIG_TRX"],
					"confdb" => $GLOBALS["DB_TRX_CONFIG"]
				);
				$this->insData->setData($delFromOpr);
				$this->insData->deleteData();
			}
			//==== end delete from qdeliveryfromoperator

			$arrStsDlvr = array(
				"ref_id" => $v["dlvr"]['ref_id'],
				"code_sms" => $v["dlvr"]['code_sms'],
				"type_status" => "dlvr",
				"status" => $stsdlvr,
				"channel" => $v["dlvr"]["operator_prefix_db"],
				"time_dlvr" => strtotime($vdlvr["time_delivery"])
			);
			$router = "x.stsclient";
			$severity = $router.".".$nmClient;
			$dtDlvr = json_encode($arrStsDlvr);
			$arrDlvr = array("url"=>$urldlvr,"dtSts"=>$dtDlvr);
			$jsondata = $this->parsVal->CreateJson($arrDlvr);
			$arrInMq = array(
				"router"	=> $router,
				"severity"	=> $severity,
				"jsonmq"	=> $jsondata,
				"confmq"	=> $GLOBALS["CONF_MQ_SMS"]
			);
			$this->setData($arrInMq);
			$this->insertMq();
		}// end foreach sts
	}// end genEmailSocmedDlvr

	public function getSmsDlvr ($arrData) {
		$tFromOpr = "q-deliveryfromoperator";
		$sFromOpr = "SELECT * FROM `".$tFromOpr."` where `prvd_message_id`='".$arrData["dtinq"]["prvd_message_id"]."'";
		$cFromOpr = array(
			"q" => $sFromOpr,
			"dbname" => $GLOBALS["DB_STATUS_CONFIG2"]["database_name"],
			"confdb" => $GLOBALS["DB_STATUS_CONFIG2"]
		);
		$this->setData($cFromOpr);
		$gstatus = $this->get_data_db();
		if($gstatus != "failed" && count($gstatus) > 0){ // search from 172.17.0.147
			$arrDlvr = array(
				"fqdlvr" => $arrData["dtinq"],
				"fqfromopr" => $gstatus,
				"prefix" => $arrData["prm"]['prefix'],
				"tbldlvr" => $arrData["tbldlvr"],
				"oprdlvr" => $tFromOpr,
				"confdb" => $GLOBALS["DB_STATUS_CONFIG2"]
			);
			$this->genSmsDlvr($arrDlvr);
		}else{ // search from 172.17.0.89
			$tFromOpr2 = "q-deliveryfromoperator";
			switch(strtolower($arrData["dtinq"]["operator_prefix_db"])){
				case "tsel":case "isat":case "xl":
					$tFromOpr2 = "q-deliveryfromoperator-".$arrData["dtinq"]["operator_prefix_db"];
				break;
			}// end switch
			$sFromOpr2 = "SELECT * FROM `".$tFromOpr2."` where `prvd_message_id`='".$arrData["dtinq"]["prvd_message_id"]."'";
			$cFromOpr2 = array(
				"q" => $sFromOpr2,
				"dbname" => $GLOBALS["DB_STATUS_CONFIG"]["database_name"],
				"confdb"	=> $GLOBALS["DB_STATUS_CONFIG"]
			);
			$this->setData($cFromOpr2);
			$gstatus = $this->get_data_db();
			if($gstatus != "failed" && count($gstatus) > 0){
				$arrDlvr = array(
					"fqdlvr" => $arrData["dtinq"],
					"fqfromopr" => $gstatus,
					"prefix" => $arrData["prm"]['prefix'],
					"tbldlvr" => $arrData["tbldlvr"],
					"oprdlvr" => $tFromOpr2,
					"confdb" => $GLOBALS["DB_STATUS_CONFIG"]
				);
				$this->genSmsDlvr($arrDlvr);
			}else{ // sts unkwn
				$arrUnsent = array();
				$arrUnsent[0] = array(
					"prvd_status_delivery" => "6",
					"time_delivery" => date("Y-m-d H:i:s"),
					"prvd_status_other"=>"",
					"prvd_message_id_real"=>"",
					"prvd_status_delivery_real"=>"",
					"prvd_status_delivery_err_real"=>"",
					"ip_remote_addr"=>"",
					"ip_remote_host"=>"",
					"id"=>"kosong"
				);
				$arrDlvr = array(
					"fqdlvr" => $arrData["dtinq"],
					"fqfromopr" => $arrUnsent,
					"prefix" => $arrData["prm"]['prefix'],
					"tbldlvr" => $arrData["tbldlvr"],
					"oprdlvr" => $tFromOpr,
					"confdb" => $GLOBALS["DB_STATUS_CONFIG2"]
				);
				$this->genSmsDlvr($arrDlvr);
			}
		}
	}// end getSmsDlvr

	public function genSmsDlvr ($arrData) {
		$v = $arrData["fqdlvr"];
		$nmTblDlvr = $arrData["tbldlvr"];
		$tFromOpr = $arrData["oprdlvr"];
		$urldlvr = $v["url_client_delivery_report"];
		$tblSmsDlvr = array("thn"=>$v["tahun"],"bln"=>$v["bulan"],"srvc"=>$v['engine_name_recv'],"prefix"=>$arrData['prefix'],"nmtbl"=>"smsdelivery");
		$nmTblSmsDlvr = $this->getVal->getNameTable($tblSmsDlvr);
		$nmClient = $v["client_prefix_dbname"];
		$sendParam=unserialize($v['sendingParam']);
		foreach($arrData["fqfromopr"] as $vs){
			$paramVal["delivery"] = $vs["prvd_status_delivery"];
			$paramVal["type_delivery"] = $v['type_delivery'];
			$paramVal["statusdlvr_delivered"] = $sendParam["statusdlvr_delivered"];
			$paramVal["statusdlvr_undelivered"] = $sendParam["statusdlvr_undelivered"];
			$paramVal["statusdlvr_pending"] = $sendParam["statusdlvr_pending"];
			$stsdlvr = $this->getVal->GetValDeliveryResponse($paramVal);
			$timeDlvr= $vs["time_delivery"];
			$prvdsprint = $vs["prvd_status_delivery"];
			$prvdsprint = $vs["prvd_status_delivery"];
			if($vs["prvd_status_delivery"] == "6" || $vs["prvd_status_delivery"] == 6){
				$prvdsprint = "SPRINT DELIVERED";
				$stsdlvr["status_delivery"]="1"; //perubahan dari Ahmad
			}
			$arrSmsDlvr = array(
				"time_delivery"=>$vs["time_delivery"],
				"prvd_status_delivery"=>$prvdsprint,
				"prvd_status_others"=>$vs["prvd_status_other"],
				"prvd_message_id_real"=>$vs["prvd_message_id_real"],
				"prvd_status_delivery_real"=>$vs["prvd_status_delivery_real"],
				"prvd_status_delivery_err_real"=>$vs["prvd_status_delivery_err_real"],
				"status_delivery"=>$stsdlvr["status_delivery"],
				"code_sms"=>$v['code_sms'],
				"f7_tid"=>$v["f7_tid"],
				"prvd_message_id"=>$v['prvd_message_id'],
				"ip_remote_addr"=>$vs["ip_remote_addr"],
				"ip_remote_host"=>$vs["ip_remote_host"],
				"operator_prefix_db"=>$v["operator_prefix_db"],
				"client_prefix_dbname"=>$nmClient,
				"client_code_serv"=>$v['client_code_serv'],
				"engine_name_recv"=>$v['engine_name_recv'],
				"ref_id"=>$v['ref_id']
			);
			$inSmsDlvr	= array(
				"arrdata"	=> $arrSmsDlvr,
				"tablename"	=> $nmTblSmsDlvr,
				"dbname"	=> $nmClient,
				"confdb"	=> $GLOBALS["DB_REPORT_CONFIG"]
			);
			$GLOBALS["logproces"] = $GLOBALS["logname"];
			$this->insData->setData($inSmsDlvr);
			$this->insData->insertData();

			//==== start delete from qdelivery
			$sDelDlvr = "DELETE FROM `".$nmTblDlvr."` where `id`='".$v["id"]."'";
			$delDlvr = array(
				"q"	=> $sDelDlvr,
				"dbname"	=> $GLOBALS["DB_CONFIG_TRX"],
				"confdb"	=> $GLOBALS["DB_TRX_CONFIG"]
			);
			$this->insData->setData($delDlvr);
			$this->insData->deleteData();
			//==== end delete from qdelivery

			//==== start delete from qdeliveryfromoperator
			$sDelFromOpr = "DELETE FROM `".$tFromOpr."` where `id`='".$vs["id"]."'";
			$delFromOpr = array(
				"q"	=> $sDelFromOpr,
				"dbname"	=> $arrData["confdb"]["database_name"],
				"confdb"	=> $arrData["confdb"]
			);
			/*$this->insData->setData($delFromOpr);
			$this->insData->deleteData();*/
			//==== end delete from qdeliveryfromoperator

			$arrStsDlvr = array(
				"ref_id" => $v['ref_id'],
				"code_sms" => $v['code_sms'],
				"type_status" => "dlvr",
				"status" => $stsdlvr["status_delivery"],
				"channel" => "SMS",
				"time_dlvr" => strtotime($timeDlvr)
			);
			$router = "x.stsclient";
			$severity = $router.".".$nmClient;
			$dtDlvr = json_encode($arrStsDlvr);
			$arrDlvr = array("url"=>$urldlvr,"dtSts"=>$dtDlvr);
			$jsondata = $this->parsVal->CreateJson($arrDlvr);
			$arrInMq = array(
				"router"	=> $router,
				"severity"	=> $severity,
				"jsonmq"	=> $jsondata,
				"confmq"	=> $GLOBALS["CONF_MQ_SMS"]
			);
			$this->setData($arrInMq);
			$this->insertMq();
		}// end foreach fqfromopr
	}// end genSmsDlvr
}// end GetDlvrExpired
?>
