<?php
namespace App\Controller;
use \App\Model\FuncDB;

class SendToDB {
	protected $dataInsert;
	
	public function setData($dt){ $this->dataInsert = $dt; }// end setData
	public function getData(){ return $this->dataInsert; }// end getData
	
	protected function insert_to_db () {
		$rs = false;
		$db		= new \App\Model\FuncDB;
		$GLOBALS["logname"] = $GLOBALS["logproces"];
		$getDtInsert = $this->getData();
		$conn = $db->connectDb($getDtInsert["dbname"], $getDtInsert["confdb"]);
		if($conn){
			$qset = "SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED ;";
			$db->queryDB($qset, $conn);
			$query = "";
			foreach($getDtInsert["arrdata"] as $key => $val){
				$query	.= "`".$key."`='".addslashes($val)."', ";
			}// end foreach
			$query	= substr($query, 0, -2);
			$q		= "INSERT INTO `".$getDtInsert["tablename"]."` SET ".$query;
			$inDB	= $db->queryDB($q, $conn);
			if($inDB){ $rs = "ok"; }
			else{ $rs	= $q; }
			$db->closeDB($conn);
		}
//$tmplogtest = "Digebukin : param ".json_encode($rs)." param DB ".json_encode($getDtInsert);
//$this->logdata->write(__FUNCTION__, $tmplogtest, $GLOBALS["logproces"]);		
		
		return $rs;
	}// end insert_to_db
	
	protected function delete_from_db () {
		$rs = false;
		$db = new \App\Model\FuncDB;
		$GLOBALS["logname"] = $GLOBALS["logproces"];
		$getDt = $this->getData();
		$conn = $db->connectDb($getDt["dbname"], $getDt["confdb"]);
		if($conn){
			$qset = "SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED ;";
			$db->queryDB($qset, $conn);
			$delDB = $db->queryDB($getDt["q"], $conn);
			if($delDB){ $rs = "ok"; }
			else{ $rs = $getDt["q"]; }
			$db->closeDB($conn);
		}
		
		return $rs;
	}// end delete_from_db
	
	public function insertMq(){
		$getDtMq	= $this->getData();
		$dataTmp	= array("datamq" => $getDtMq, "logname" => $GLOBALS["logproces"]);
		$dataInsert	= json_encode($dataTmp);
		$urlinsert	= $GLOBALS["URLSTSSEND"]."sms";
		$insData	= $this->sendProvider->sendBackData($dataInsert, $urlinsert);
	}// end insertMqFailed
	
	public function insertData(){
		$rsSend = $this->insert_to_db();
		if($rsSend != "ok"){// failed insert db
			$getDtMq = $this->getData();
			$arrFailed	= array(
							"q"			=> $rsSend,
							"dbname"	=> $getDtMq["dbname"],
							"confdb"	=> $getDtMq["confdb"]
						);
			$jsondata	= $this->parsVal->CreateJson($arrFailed);
			$router		= "x.query";
			$severity	= $router.".failed";
			$tmplog		= "Failed Insert : ".$jsondata;
			$this->logdata->write(__FUNCTION__, $tmplog, $GLOBALS["logproces"]);
			$arrInMq	= array(
				"router"	=> $router,
				"severity"	=> $severity,
				"jsonmq"	=> $jsondata,
				"confmq"	=> $GLOBALS["CONF_MQ_SMS"]
			);
			$this->setData($arrInMq);
			$this->insertMq();
		}// end failed insert rsSend
	}// end insertData
	
	public function deleteData(){
		$rsDel = $this->delete_from_db();
		if($rsDel != "ok"){// failed delete db
			$getDtMq = $this->getData();
			$arrFailed	= array(
							"q"			=> $getDtMq["q"],
							"dbname"	=> $getDtMq["dbname"],
							"confdb"	=> $getDtMq["confdb"]
						);
			$jsondata	= $this->parsVal->CreateJson($arrFailed);
			$router		= "x.query";
			$severity	= $router.".failed";
			$tmplog		= "Failed Insert : ".$jsondata;
			$this->logdata->write(__FUNCTION__, $tmplog, $GLOBALS["logproces"]);
			$arrInMq	= array(
				"router"	=> $router,
				"severity"	=> $severity,
				"jsonmq"	=> $jsondata,
				"confmq"	=> $GLOBALS["CONF_MQ_SMS"]
			);
			$this->setData($arrInMq);
			$this->insertMq();
		}// end failed delete rsDel
	}// end deleteData
}// end class SendToDB
?>
