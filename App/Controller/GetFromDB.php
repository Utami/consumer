<?php
namespace App\Controller;
use \App\Model\FuncDB;

class GetFromDB {
	protected $paramdt;
	
	public function setData($dt){ $this->paramdt = $dt; }// end setData
	public function getData(){ return $this->paramdt; }// end getData
	
	protected function get_data_db () {
		$rs = "failed";
		$db	= new \App\Model\FuncDB;
		$getDt = $this->getData();
		$conn = $db->connectDb($getDt["dbname"], $getDt["confdb"]);
		if($conn){
			$qset	= "SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED ;";
			$db->queryDB($qset, $conn);
			$s	= $db->queryDB($getDt["q"], $conn);
			if(@$s->num_rows > 0){
				$rs = array();
				$r = $db->rowDB($s);
				foreach($r as $v){
					$parsarr = array();
					foreach($v as $kdt => $vdt){
						if(!is_numeric($kdt)){
							$tmp = array($kdt => $vdt);
							$parsarr = array_merge($parsarr, $tmp);
						}
					}
					array_push($rs, $parsarr);
				}
			}/*else{
				$tmplog = "Empty Data From DB ".$getDt["q"];
				//$this->logdata->write(__FUNCTION__, $tmplog, $GLOBALS["logname"]);
			}*/
			$db->closeDB($conn);
		}else{
			$tmplog = "Failed to connect DB ".$getDt["dbname"];
			$this->logdata->write(__FUNCTION__, $tmplog, $GLOBALS["logname"]);
		}
		
		return $rs;
	}// end insert_to_report
	
	public function insertMq(){
		$getDtMq	= $this->getData();
		$dataTmp	= array("datamq" => $getDtMq, "logname" => $GLOBALS["logproces"]);
		$dataInsert	= json_encode($dataTmp);
		$urlinsert	= $GLOBALS["URLSTSSEND"]."sms";
		$insData	= $this->sendProvider->sendBackData($dataInsert, $urlinsert);
	}// end insertMq
	
	protected function update_to_db () {
		$rs = "failed";
		$db = new \App\Model\FuncDB;
		
		$GLOBALS["logname"] = $GLOBALS["logproces"];
		$getDt = $this->getData();
		$conn = $db->connectDb($getDt["dbname"], $getDt["confdb"]);
		if($conn){
			$qset = "SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED ;";
			$a = $db->queryDB($qset, $conn);
			$query = "";
			foreach($getDt["arrdata"] as $key => $val){
				$query	.= "`".$key."`='".addslashes($val)."', ";
			}// end foreach
			$query	= substr($query, 0, -2);
			$dtwhere= "";
			foreach($getDt["dtwhere"] as $key => $val){
				$dtwhere .= "`".$key."`='".addslashes($val)."', ";
			}// end foreach
			$dtwhere = substr($dtwhere, 0, -2);
			$q = "UPDATE `".$getDt["tablename"]."` SET ".$query." WHERE ".$dtwhere;
			$sDB = $db->queryDB($q, $conn);
			if($sDB){ $rs = "ok"; }
			else{ $rs	= $q; }
			$db->closeDB($conn);
		}
		
		return $rs;
	}// end updateData
	
	public function updateData(){
		$rs = $this->update_to_db();
		if($rs != "ok"){// failed insert db
			$getDtMq = $this->getData();
			$arrFailed	= array(
							"q"			=> $rs,
							"dbname"	=> $getDtMq["dbname"],
							"confdb"	=> $getDtMq["confdb"]
						);
			$jsondata	= $this->parsVal->CreateJson($arrFailed);
			$router		= "x.query";
			$severity	= $router.".failed";
			$tmplog		= "Failed Insert : ".$jsondata;
			$this->logdata->write(__FUNCTION__, $tmplog, $GLOBALS["logproces"]);
			$arrInMq	= array(
				"router"	=> $router,
				"severity"	=> $severity,
				"jsonmq"	=> $jsondata,
				"confmq"	=> $GLOBALS["CONF_MQ_SMS"]
			);
			$this->setData($arrInMq);
			$this->insertMq();
		}// end failed insert rs
	}// end updateData
}// end GetFromDB
?>