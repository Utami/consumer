<?php

namespace App\Controller;
use \App\Model\FuncDB;

class truncTable {
	protected $opr		= array("other","tsel","isat","xl","prior","email","socmed");
	protected $oprtriger= array("tsel","isat","xl","prior","email","socmed");
	protected $dlvr		= array("other","tsel","isat","xl","prior");
	protected $db;
	
	public function __construct () {
		$this->db	= new \App\Model\FuncDB;
		$GLOBALS["logname"]	= "truncatetable";
	}
	
	public function setTable ($arrData) {
		//$this->renameTable($arrData);
		$this->dropTable($arrData);
		/*foreach($arrData["dbreport"] as $vdb){
			$conn = $this->db->connectDb($vdb, $GLOBALS["DB_REPORT_CONFIG"]);
			$qUnion= "TRUNCATE `i-".$arrData["y"]."_".$arrData["m"]."-union_all`";
			$this->db->queryDB($qUnion, $conn);
			$qRecv = "";
			$qSend = "";
			$qDlvr = "";
			$qHist = "";
			$qTrx = "";
			foreach($this->opr as $vopr){
				switch($vopr){
					case "other":
						$qRecv = "TRUNCATE `i-".$arrData["y"]."_".$arrData["m"]."-smsrecv`";
						$qSend = "TRUNCATE `i-".$arrData["y"]."_".$arrData["m"]."-smssend`";
						$qDlvr = "TRUNCATE `i-".$arrData["y"]."_".$arrData["m"]."-smsdelivery`";
					break;
					default:
						$qRecv = "TRUNCATE `i-".$arrData["y"]."_".$arrData["m"]."-smsrecv-".$vopr."`";
						$qSend = "TRUNCATE `i-".$arrData["y"]."_".$arrData["m"]."-smssend-".$vopr."`";
						$qDlvr = "TRUNCATE `i-".$arrData["y"]."_".$arrData["m"]."-smsdelivery-".$vopr."`";
				}// end switch
				$this->db->queryDB($qRecv, $conn);
				$this->db->queryDB($qSend, $conn);
				$this->db->queryDB($qDlvr, $conn);
			}// end foreach
			$this->db->closeDB($conn);
		}*/
		/*$conn = $this->db->connectDb($arrData["dbtrxdlvr"], $GLOBALS["DB_REPORT_CONFIG"]);
		foreach($this->dlvr as $vtbl){
			switch($vtbl){
				case "other": $qTrx = "TRUNCATE `i-".$arrData["y"]."_".$arrData["m"]."-delivery"; break;
				default:
					$qTrx = "TRUNCATE `i-".$arrData["y"]."_".$arrData["m"]."-delivery-".$vtbl."`";
			}
			$this->db->queryDB($qTrx, $conn);
		}
		$this->db->closeDB($conn);
		foreach($arrData["dbhistory"] as $vdb){
			$conn = $this->db->connectDb($vdb, $GLOBALS["DB_REPORT_CONFIG"]);
			foreach($this->opr as $vopr){
				$tblName = "";
				switch($vopr){
					case "other": $tblName = "TRUNCATE `i-".$arrData["y"]."_".$arrData["m"]."-sendingsms`"; break;
					default:
						$tblName = "TRUNCATE `i-".$arrData["y"]."_".$arrData["m"]."-sendingsms-".$vopr."`";
				}// end switch
				$this->db->queryDB($tblName, $conn);
			}
			$this->db->closeDB($conn);
		}*/
	}// end setTable
	
	public function renameTable ($arrData) {
		foreach($arrData["dbreport"] as $vdb){
			$conn = $this->db->connectDb($vdb, $GLOBALS["DB_REPORT_CONFIG"]);
			$qUnion= "RENAME TABLE `i-".$arrData["y"]."_".$arrData["m"]."-union_all` TO `i-".$arrData["y"]."_".$arrData["m"]."-union_all_back`";
			$this->db->queryDB($qUnion, $conn);
			$qRecv = "";
			$qSend = "";
			$qDlvr = "";
			$qHist = "";
			$qTrx = "";
			foreach($this->opr as $vopr){
				switch($vopr){
					case "other":
						$qRecv = "RENAME TABLE `i-".$arrData["y"]."_".$arrData["m"]."-smsrecv` TO `i-".$arrData["y"]."_".$arrData["m"]."-smsrecv_back`";
						$qSend = "RENAME TABLE `i-".$arrData["y"]."_".$arrData["m"]."-smssend` TO `i-".$arrData["y"]."_".$arrData["m"]."-smssend_back`";
						$qDlvr = "RENAME TABLE `i-".$arrData["y"]."_".$arrData["m"]."-smsdelivery` TO `i-".$arrData["y"]."_".$arrData["m"]."-smsdelivery_back`";
					break;
					default:
						$qRecv = "RENAME TABLE `i-".$arrData["y"]."_".$arrData["m"]."-smsrecv-".$vopr."` TO `i-".$arrData["y"]."_".$arrData["m"]."-smsrecv-".$vopr."_back`";
						$qSend = "RENAME TABLE `i-".$arrData["y"]."_".$arrData["m"]."-smssend-".$vopr."` TO `i-".$arrData["y"]."_".$arrData["m"]."-smssend-".$vopr."_back`";
						$qDlvr = "RENAME TABLE `i-".$arrData["y"]."_".$arrData["m"]."-smsdelivery-".$vopr."` TO `i-".$arrData["y"]."_".$arrData["m"]."-smsdelivery-".$vopr."_back`";
				}// end switch
				$this->db->queryDB($qRecv, $conn);
				$this->db->queryDB($qSend, $conn);
				$this->db->queryDB($qDlvr, $conn);
			}// end foreach
			$this->db->closeDB($conn);
		}
	}// end renameTable
	
	public function dropTable ($arrData) {
		//$pref = "_back";
		$pref = "";
		foreach($arrData["dbreport"] as $vdb){
			$conn = $this->db->connectDb($vdb, $GLOBALS["DB_REPORT_CONFIG"]);
			$qUnion= "DROP TABLE `i-".$arrData["y"]."_".$arrData["m"]."-union_all".$pref."`";
			$this->db->queryDB($qUnion, $conn);
			$qRecv = "";
			$qSend = "";
			$qDlvr = "";
			$qHist = "";
			$qTrx = "";
			foreach($this->opr as $vopr){
				switch($vopr){
					case "other":
						$qRecv = "DROP TABLE `i-".$arrData["y"]."_".$arrData["m"]."-smsrecv".$pref."`";
						$qSend = "DROP TABLE `i-".$arrData["y"]."_".$arrData["m"]."-smssend".$pref."`";
						$qDlvr = "DROP TABLE `i-".$arrData["y"]."_".$arrData["m"]."-smsdelivery".$pref."`";
					break;
					default:
						$qRecv = "DROP TABLE `i-".$arrData["y"]."_".$arrData["m"]."-smsrecv-".$vopr.$pref."`";
						$qSend = "DROP TABLE `i-".$arrData["y"]."_".$arrData["m"]."-smssend-".$vopr.$pref."`";
						$qDlvr = "DROP TABLE `i-".$arrData["y"]."_".$arrData["m"]."-smsdelivery-".$vopr.$pref."`";
				}// end switch
				$this->db->queryDB($qRecv, $conn);
				$this->db->queryDB($qSend, $conn);
				$this->db->queryDB($qDlvr, $conn);
			}// end foreach
			$this->db->closeDB($conn);
		}
	}// end dropTable
}// end class truncTable
?>