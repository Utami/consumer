<?php

namespace App\Controller;
use \App\Model\FuncDB;

class CreateTable {
	protected $opr		= array("other","tsel","isat","xl","prior","email","socmed");
	protected $oprtriger= array("other","tsel","isat","xl","prior","email","socmed");
	protected $dlvr		= array("other","tsel","isat","xl","prior","email","socmed");
	protected $db;
	
	public function __construct () {
		$this->db	= new \App\Model\FuncDB;
		$GLOBALS["logname"]	= "createtable";
	}
	
	public function setTable ($arrData) {
		foreach($arrData["dbreport"] as $vdb){
			$union		= $this->CreateUNION($vdb, $arrData["m"], $arrData["y"]);
			$smsrecv	= $this->CreateSMSRECV($vdb, $arrData["m"], $arrData["y"]);
			$smssend	= $this->CreateSMSSEND($vdb, $arrData["m"], $arrData["y"]);
			$smsdelivery= $this->CreateSMSDELIVERY($vdb, $arrData["m"], $arrData["y"]);
			
			if($union == "ok" && $smsrecv == "ok" && $smssend == "ok" && $smsdelivery == "ok"){
				$trigerrecv		= $this->CreateTrigerRECV($vdb, $arrData["m"], $arrData["y"]);
				$trigerrsend	= $this->CreateTrigerSEND($vdb, $arrData["m"], $arrData["y"]);
				$trigerrdlvr	= $this->CreateTrigerDELIVERY($vdb, $arrData["m"], $arrData["y"]);
			}
		}// end foreach
		
		foreach($arrData["dbhistory"] as $vdb){
			$history = $this->CreateSENDINGSMS($vdb, $arrData["m"], $arrData["y"]);
		}// end foreach
		$this->CreateQDLVR($arrData["dbtrxdlvr"], $arrData["m"], $arrData["y"]);
	}// end setTable
	
	public function CreateUNION($fservice, $fbulan, $ftahun){
		$rs		= "failed";
		$tahun	= $ftahun;
		$bulan	= $fbulan;
		$tblName= "i-".$tahun."_".$bulan."-union_all";
		$conn = $this->db->connectDb($fservice, $GLOBALS["DB_REPORT_CONFIG"]);
		
		if($conn){
			$qset	= "SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED ;";
			$a = $this->db->queryDB($qset, $conn);
			$qc		= "SELECT id FROM `".addslashes($tblName)."` limit 1";
			
			$cekDB	= $this->db->queryDB($qc, $conn);
			if(!$cekDB){
				$qcreate ="CREATE TABLE IF NOT EXISTS `".addslashes($tblName)."` (";
				$qcreate.="`id` bigint(20) NOT NULL AUTO_INCREMENT, `id-c` mediumint(9) NOT NULL DEFAULT '0',";
				$qcreate.="`name-c` varchar(50) CHARACTER SET utf8 DEFAULT NULL, `id-div` smallint(6) DEFAULT '0',";
				$qcreate.="`name-div` varchar(100) COLLATE utf8_general_mysql500_ci DEFAULT NULL, `id-u` mediumint(9) NOT NULL DEFAULT '0',";
				$qcreate.="`name-u` varchar(20) CHARACTER SET utf8 DEFAULT NULL, `user_name_tcp` varchar(50) CHARACTER SET utf8 DEFAULT NULL,";
				$qcreate.="`id-m` mediumint(9) DEFAULT NULL, `id-p-s-cfg` smallint(6) NOT NULL DEFAULT '0',";
				$qcreate.="`name-p-s-cfg` varchar(50) CHARACTER SET utf8 DEFAULT NULL, `dest_number` varchar(25) CHARACTER SET utf8 DEFAULT NULL,";
				$qcreate.="`shortnumber` varchar(15) CHARACTER SET utf8 DEFAULT NULL, `service_id_prvd` varchar(50) CHARACTER SET utf8 DEFAULT NULL,";
				$qcreate.="`model_service` varchar(20) CHARACTER SET utf8 DEFAULT NULL, `name-sender` varchar(50) CHARACTER SET utf8 DEFAULT NULL,";
				$qcreate.="`sms_req` varchar(10000) CHARACTER SET utf8 DEFAULT NULL, `hide_sms` enum('0','1') CHARACTER SET utf8 NOT NULL DEFAULT '0',";
				$qcreate.="`subject` varchar(250) CHARACTER SET utf8 NOT NULL DEFAULT '', `sms_send` varchar(10000) CHARACTER SET utf8 DEFAULT NULL,";
				$qcreate.="`code_sms` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',";
				$qcreate.="`f7_tid` varchar(100) CHARACTER SET latin1 DEFAULT NULL, `budget_code` varchar(250) CHARACTER SET utf8 NOT NULL DEFAULT '',";
				$qcreate.="`ref_id` varchar(100) CHARACTER SET latin1 DEFAULT NULL, `prvd_status_send` varchar(60) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',";
				$qcreate.="`prvd_message_id` varchar(60) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',";
				$qcreate.="`status_send` enum('0','1','2','3') CHARACTER SET utf8 NOT NULL DEFAULT '2',";
				$qcreate.="`prvd_status_delivery` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',";
				$qcreate.="`prvd_status_delivery_real` varchar(50) CHARACTER SET utf8 DEFAULT '',";
				$qcreate.="`prvd_status_delivery_err_real` varchar(50) CHARACTER SET utf8 DEFAULT '',`status_delivery` varchar(2) CHARACTER SET utf8 DEFAULT '3',";
				$qcreate.="`time_querecv` datetime DEFAULT NULL, `time_dbrecv` datetime DEFAULT NULL, `time_sched` datetime DEFAULT NULL,";
				$qcreate.="`time_startsend` datetime DEFAULT NULL, `time_send` datetime DEFAULT NULL, `time_sent` datetime DEFAULT NULL,";
				$qcreate.="`time_delivery` datetime DEFAULT NULL, `operator_prefix_db` varchar(15) CHARACTER SET utf8 DEFAULT NULL,";
				$qcreate.="`client_prefix_dbname` varchar(4) CHARACTER SET utf8 DEFAULT NULL, `client_code_serv` varchar(2) CHARACTER SET utf8 DEFAULT NULL,";
				$qcreate.="`engine_name_recv` varchar(10) CHARACTER SET utf8 DEFAULT NULL, `engine_name_send` varchar(10) COLLATE utf8_general_mysql500_ci DEFAULT NULL,";
				$qcreate.="`time_limitsend` datetime DEFAULT NULL, `id-rptrecv` bigint(20) NOT NULL DEFAULT '0', `id-rptsend` bigint(20) NOT NULL DEFAULT '0',";
				$qcreate.="`id-rptdelivery` bigint(20) NOT NULL DEFAULT '0', `counter_sms` smallint(6) DEFAULT '1', PRIMARY KEY (`id`), ";
				$qcreate.="KEY `id-c` (`id-c`), KEY `name-c` (`name-c`), KEY `id-u` (`id-u`), KEY `name-u` (`name-u`), ";
				$qcreate.="KEY `user_name_tcp` (`user_name_tcp`), KEY `id-p-s-cfg` (`id-p-s-cfg`), KEY `name-p-s-cfg` (`name-p-s-cfg`), KEY `dest_number` (`dest_number`), ";
				$qcreate.="KEY `shortnumber` (`shortnumber`), KEY `service_id_prvd` (`service_id_prvd`), KEY `model_service` (`model_service`), ";
				$qcreate.="KEY `name-sender` (`name-sender`), KEY `sms_req` (`sms_req`), KEY `hide_sms` (`hide_sms`), ";
				$qcreate.="KEY `subject` (`subject`), KEY `sms_send` (`sms_send`), KEY `code_sms` (`code_sms`), ";
				$qcreate.="KEY `f7_tid` (`f7_tid`), KEY `budget_code` (`budget_code`), KEY `ref_id` (`ref_id`), ";
				$qcreate.="KEY `prvd_status_send` (`prvd_status_send`), KEY `prvd_message_id` (`prvd_message_id`), KEY `status_send` (`status_send`), ";
				$qcreate.="KEY `prvd_status_delivery` (`prvd_status_delivery`), KEY `prvd_status_delivery_real` (`prvd_status_delivery_real`), ";
				$qcreate.="KEY `prvd_status_delivery_err_real` (`prvd_status_delivery_err_real`), KEY `time_querecv` (`time_querecv`), KEY `time_dbrecv` (`time_dbrecv`), ";
				$qcreate.="KEY `time_sched` (`time_sched`), KEY `time_startsend` (`time_startsend`), KEY `time_send` (`time_send`), ";
				$qcreate.="KEY `time_sent` (`time_sent`), KEY `time_delivery` (`time_delivery`), KEY `operator_prefix_db` (`operator_prefix_db`), ";
				$qcreate.="KEY `client_prefix_dbname` (`client_prefix_dbname`), KEY `client_code_serv` (`client_code_serv`), KEY `engine_name_recv` (`engine_name_recv`), ";
				$qcreate.="KEY `engine_name_send` (`engine_name_send`), KEY `time_limitsend` (`time_limitsend`), KEY `id-rptrecv` (`id-rptrecv`), ";
				$qcreate.="KEY `id-rptsend` (`id-rptsend`), KEY `id-rptdelivery` (`id-rptdelivery`), KEY `counter_sms` (`counter_sms`) ";
				$qcreate.=") ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_mysql500_ci ROW_FORMAT=COMPRESSED;";
				
				$createDB	= $this->db->queryDB($qcreate, $conn);
				if($createDB){ $rs = "ok"; }
			}// end cekDB
			$this->db->closeDB($conn);
		}
		return $rs;
	}// end CreateUNION
	
	public function CreateSMSRECV($fservice, $fbulan, $ftahun){
		$rs		= "ok";
		$tahun	= $ftahun;
		$bulan	= $fbulan;
		$tblName= "";
		$tmpRs	= array();
		
		foreach($this->opr as $vopr){
			switch($vopr){
				case "other": $tblName = "i-".$tahun."_".$bulan."-smsrecv"; break;
				default:
					$tblName = "i-".$tahun."_".$bulan."-smsrecv-".$vopr;
			}// end switch
			$conn = $this->db->connectDb($fservice, $GLOBALS["DB_REPORT_CONFIG"]);
			if($conn){
				$qset	= "SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED ;";
				$this->db->queryDB($qset, $conn);
				$qc		= "SELECT id FROM `".addslashes($tblName)."` limit 1";
				$cekDB	= $this->db->queryDB($qc, $conn);
				if(!$cekDB){
					$qcreate ="CREATE TABLE IF NOT EXISTS `".addslashes($tblName)."` (";
					$qcreate.="`id` bigint(20) NOT NULL AUTO_INCREMENT, `id-c` mediumint(9) NOT NULL DEFAULT '0',";
					$qcreate.="`name-c` varchar(50) CHARACTER SET latin1 DEFAULT NULL, `id-div` smallint(6) DEFAULT '0',";
					$qcreate.="`name-div` varchar(100) COLLATE utf8_general_mysql500_ci DEFAULT NULL, `id-u` mediumint(9) NOT NULL DEFAULT '0',";
					$qcreate.="`name-u` varchar(20) CHARACTER SET latin1 DEFAULT NULL, `user_name_tcp` varchar(50) CHARACTER SET latin1 DEFAULT NULL,";
					$qcreate.="`id-m` mediumint(9) DEFAULT NULL, `id-st` smallint(6) NOT NULL DEFAULT '0', `name-st` varchar(50) CHARACTER SET latin1 DEFAULT NULL,";
					$qcreate.="`id-c-ch` smallint(6) NOT NULL DEFAULT '0', `name-c-ch` varchar(50) CHARACTER SET latin1 DEFAULT NULL,";
					$qcreate.="`id-p-s-cfg` smallint(6) NOT NULL DEFAULT '0', `name-p-s-cfg` varchar(50) CHARACTER SET latin1 DEFAULT NULL,";
					$qcreate.="`id-hss` smallint(6) NOT NULL DEFAULT '0', `name-hss` varchar(50) CHARACTER SET latin1 DEFAULT NULL,";
					$qcreate.="`id-hss-d` smallint(6) NOT NULL DEFAULT '0', `name-hss-d` varchar(50) CHARACTER SET latin1 DEFAULT NULL,";
					$qcreate.="`id-hss-m` varchar(25) CHARACTER SET latin1 DEFAULT NULL, `dest_number` varchar(25) CHARACTER SET latin1 DEFAULT NULL,";
					$qcreate.="`smscno` varchar(25) CHARACTER SET latin1 DEFAULT NULL, `shortnumber` varchar(15) CHARACTER SET latin1 DEFAULT NULL,";
					$qcreate.="`trx_id_get` varchar(50) CHARACTER SET latin1 DEFAULT NULL, `service_id_prvd` varchar(50) CHARACTER SET latin1 DEFAULT NULL,";
					$qcreate.="`model_service` varchar(20) CHARACTER SET latin1 DEFAULT NULL, `ip_http_forwarded` varchar(50) CHARACTER SET latin1 DEFAULT NULL,";
					$qcreate.="`ip_remote_addr` varchar(50) CHARACTER SET latin1 DEFAULT NULL, `name-sender` varchar(50) CHARACTER SET latin1 DEFAULT NULL,";
					$qcreate.="`sms_req` varchar(10000) CHARACTER SET utf8 DEFAULT NULL, `hide_sms` enum('0','1') CHARACTER SET latin1 NOT NULL DEFAULT '0',";
					$qcreate.="`subject` varchar(250) CHARACTER SET latin1 NOT NULL DEFAULT '', `sms_send` varchar(10000) CHARACTER SET utf8 DEFAULT NULL,";
					$qcreate.="`code_sms` varchar(50) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL DEFAULT '',";
					$qcreate.="`f7_tid` varchar(100) CHARACTER SET latin1 DEFAULT NULL, `ref_id` varchar(100) CHARACTER SET latin1 DEFAULT NULL, ";
					$qcreate.="`budget_code` varchar(250) CHARACTER SET latin1 NOT NULL DEFAULT '',";
					$qcreate.="`unique_code` varchar(16) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL DEFAULT '',";
					$qcreate.="`live_code` int(10) unsigned zerofill DEFAULT '0000000000',`prvd_status_send` varchar(60) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL DEFAULT '',";
					$qcreate.="`prvd_message_id` varchar(60) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL DEFAULT '',";
					$qcreate.="`prvd_message_id_real` varchar(100) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL DEFAULT '',";
					$qcreate.="`status_send` enum('0','1','2','3') CHARACTER SET latin1 NOT NULL DEFAULT '2',";
					$qcreate.="`prvd_status_delivery` varchar(50) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL DEFAULT '',";
					$qcreate.="`prvd_status_delivery_real` varchar(50) CHARACTER SET latin1 DEFAULT '',";
					$qcreate.="`prvd_status_delivery_err_real` varchar(50) CHARACTER SET latin1 DEFAULT '',";
					$qcreate.="`status_delivery` varchar(2) CHARACTER SET latin1 DEFAULT '3', `retry_tosend` varchar(2) CHARACTER SET latin1 NOT NULL DEFAULT '',";
					$qcreate.="`get_delivery` varchar(2) CHARACTER SET latin1 NOT NULL DEFAULT '', `retry_tosend_dlvr` varchar(2) CHARACTER SET latin1 NOT NULL DEFAULT '',";
					$qcreate.="`time_querecv` datetime DEFAULT NULL, `time_dbrecv` datetime DEFAULT NULL, `time_sched` datetime DEFAULT NULL, `time_startsend` datetime DEFAULT NULL,";
					$qcreate.="`time_send` datetime DEFAULT NULL, `time_sent` datetime DEFAULT NULL, `time_delivery` datetime DEFAULT NULL,";
					$qcreate.="`operator_prefix_db` varchar(15) CHARACTER SET latin1 DEFAULT NULL, `client_prefix_dbname` varchar(4) CHARACTER SET latin1 DEFAULT NULL,";
					$qcreate.="`client_code_serv` varchar(2) CHARACTER SET latin1 DEFAULT NULL, `engine_name_recv` varchar(10) CHARACTER SET latin1 DEFAULT NULL,";
					$qcreate.="`time_limitsend` datetime DEFAULT NULL, `counter_sms` smallint(6) DEFAULT '1', PRIMARY KEY (`id`),";
					$qcreate.="KEY `id-c` (`id-c`), KEY `name-c` (`name-c`), KEY `id-u` (`id-u`),";
					$qcreate.="KEY `name-u` (`name-u`), KEY `user_name_tcp` (`user_name_tcp`), KEY `id-c-ch` (`id-c-ch`),";
					$qcreate.="KEY `name-c-ch` (`name-c-ch`), KEY `id-p-s-cfg` (`id-p-s-cfg`), KEY `name-p-s-cfg` (`name-p-s-cfg`),";
					$qcreate.="KEY `dest_number` (`dest_number`), KEY `smscno` (`smscno`), KEY `shortnumber` (`shortnumber`),";
					$qcreate.="KEY `trx_id_get` (`trx_id_get`), KEY `service_id_prvd` (`service_id_prvd`), KEY `model_service` (`model_service`),";
					$qcreate.="KEY `ip_http_forwarded` (`ip_http_forwarded`), KEY `ip_remote_addr` (`ip_remote_addr`), KEY `name-sender` (`name-sender`),";
					$qcreate.="KEY `sms_req` (`sms_req`), KEY `hide_sms` (`hide_sms`), KEY `subject` (`subject`),";
					$qcreate.="KEY `sms_send` (`sms_send`), KEY `code_sms` (`code_sms`), KEY `f7_tid` (`f7_tid`),";
					$qcreate.="KEY `ref_id` (`ref_id`), KEY `budget_code` (`budget_code`), KEY `unique_code` (`unique_code`),";
					$qcreate.="KEY `live_code` (`live_code`), KEY `prvd_status_send` (`prvd_status_send`), KEY `prvd_message_id` (`prvd_message_id`),";
					$qcreate.="KEY `prvd_message_id_real` (`prvd_message_id_real`), KEY `status_send` (`status_send`), KEY `prvd_status_delivery` (`prvd_status_delivery`),";
					$qcreate.="KEY `prvd_status_delivery_real` (`prvd_status_delivery_real`), KEY `prvd_status_delivery_err_real` (`prvd_status_delivery_err_real`),";
					$qcreate.="KEY `status_delivery` (`status_delivery`), KEY `retry_tosend` (`retry_tosend`), KEY `get_delivery` (`get_delivery`),";
					$qcreate.="KEY `time_querecv` (`time_querecv`), KEY `time_dbrecv` (`time_dbrecv`), KEY `time_sched` (`time_sched`),";
					$qcreate.="KEY `time_startsend` (`time_startsend`), KEY `time_send` (`time_send`), KEY `time_sent` (`time_sent`),";
					$qcreate.="KEY `time_delivery` (`time_delivery`), KEY `operator_prefix_db` (`operator_prefix_db`), KEY `client_prefix_dbname` (`client_prefix_dbname`),";
					$qcreate.="KEY `client_code_serv` (`client_code_serv`), KEY `engine_name_recv` (`engine_name_recv`), KEY `time_limitsend` (`time_limitsend`),";
					$qcreate.="KEY `counter_sms` (`client_code_serv`)";
					$qcreate.=") ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_mysql500_ci ROW_FORMAT=COMPRESSED;";
					
					$createDB	= $this->db->queryDB($qcreate, $conn);
					if($createDB){ $tmpRs[$vopr] = "ok"; }
					else{ $tmpRs[$vopr] = "no"; }
				}// end cekDB
				$this->db->closeDB($conn);
			}
		}// end foreach opr
		foreach($tmpRs as $vRs){
			if($vRs == "no"){ $rs = "failed"; }
		}
		
		return $rs;
	}// end CreateSMSRECV
	
	public function CreateSMSSEND($fservice, $fbulan, $ftahun){
		$rs		= "ok";
		$tahun	= $ftahun;
		$bulan	= $fbulan;
		$tblName= "";
		$tmpRs	= array();
		
		foreach($this->opr as $vopr){
			switch($vopr){
				case "other": $tblName = "i-".$tahun."_".$bulan."-smssend"; break;
				default:
					$tblName = "i-".$tahun."_".$bulan."-smssend-".$vopr;
			}// end switch
			$conn = $this->db->connectDb($fservice, $GLOBALS["DB_REPORT_CONFIG"]);
			if($conn){
				$qset	= "SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED ;";
				$this->db->queryDB($qset, $conn);
				$qc		= "SELECT id FROM `".addslashes($tblName)."` limit 1";
				$cekDB	= $this->db->queryDB($qc, $conn);
				if(!$cekDB){
					$qcreate ="CREATE TABLE IF NOT EXISTS `".addslashes($tblName)."` (";
					$qcreate.="`id` bigint(20) NOT NULL AUTO_INCREMENT, `id-hstrecv` bigint(20) NOT NULL DEFAULT '0',";
					$qcreate.="`code_sms` varchar(50) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL DEFAULT '', `id-p-s-cfg` smallint(6) DEFAULT NULL,";
					$qcreate.="`prvd_status_send` varchar(60) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL DEFAULT '',";
					$qcreate.="`prvd_message_id` varchar(100) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL DEFAULT '',";
					$qcreate.="`status_send` enum('0','1','2','3') NOT NULL DEFAULT '1', `retry_tosend` varchar(2) DEFAULT NULL, `time_send` datetime DEFAULT NULL,";
					$qcreate.="`time_sent` datetime DEFAULT NULL, `f7_tid` varchar(100) CHARACTER SET latin1 DEFAULT NULL,`thread_no` smallint(6) DEFAULT NULL,";
					$qcreate.="`ref_id` varchar(100) CHARACTER SET latin1 DEFAULT NULL,";
					$qcreate.="`operator_prefix_db` varchar(15) DEFAULT NULL, `client_prefix_dbname` varchar(4) DEFAULT NULL, `client_code_serv` varchar(2) DEFAULT NULL,";
					$qcreate.="`engine_name_recv` varchar(10) DEFAULT NULL, `engine_name_send` varchar(10) DEFAULT NULL, `time_limitsend` datetime DEFAULT NULL,";
					$qcreate.="PRIMARY KEY (`id`), KEY `id-hstrecv` (`id-hstrecv`), KEY `code_sms` (`code_sms`), KEY `prvd_status_send` (`prvd_status_send`), ";
					$qcreate.="KEY `prvd_message_id` (`prvd_message_id`), KEY `status_send` (`status_send`), KEY `retry_tosend` (`retry_tosend`), ";
					$qcreate.="KEY `time_send` (`time_send`), KEY `time_sent` (`time_sent`), KEY `f7_tid` (`f7_tid`), ";
					$qcreate.="KEY `thread_no` (`thread_no`), KEY `ref_id` (`ref_id`), KEY `operator_prefix_db` (`operator_prefix_db`), ";
					$qcreate.="KEY `client_prefix_dbname` (`client_prefix_dbname`), KEY `client_code_serv` (`client_code_serv`), KEY `engine_name_recv` (`engine_name_recv`), ";
					$qcreate.="KEY `engine_name_send` (`engine_name_send`), KEY `time_limitsend` (`time_limitsend`) ) ";
					$qcreate.="ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPRESSED;";
					
					$createDB	= $this->db->queryDB($qcreate, $conn);
					if($createDB){ $tmpRs[$vopr] = "ok"; }
					else{ $tmpRs[$vopr] = "no"; }
				}// end cekDB
				$this->db->closeDB($conn);
			}
		}// end foreach opr
		foreach($tmpRs as $vRs){
			if($vRs == "no"){ $rs = "failed"; }
		}
		
		return $rs;
	}// end CreateSMSSEND
	
	public function CreateSMSDELIVERY($fservice, $fbulan, $ftahun){
		$rs		= "ok";
		$tahun	= $ftahun;
		$bulan	= $fbulan;
		$tblName= "";
		
		$tmpRs	= array();
		foreach($this->opr as $vopr){
			switch($vopr){
				case "other": $tblName = "i-".$tahun."_".$bulan."-smsdelivery"; break;
				default:
					$tblName = "i-".$tahun."_".$bulan."-smsdelivery-".$vopr;
			}// end switch
			$conn = $this->db->connectDb($fservice, $GLOBALS["DB_REPORT_CONFIG"]);
			if($conn){
				$qset	= "SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED ;";
				$this->db->queryDB($qset, $conn);
				$qc		= "SELECT id FROM `".addslashes($tblName)."` limit 1";
				$cekDB	= $this->db->queryDB($qc, $conn);
				if(!$cekDB){
					$qcreate ="CREATE TABLE IF NOT EXISTS `".addslashes($tblName)."` (";
					$qcreate.="`id` bigint(20) NOT NULL AUTO_INCREMENT, `id-rptsendsms` bigint(20) NOT NULL DEFAULT '0',";
					$qcreate.="`time_delivery` datetime DEFAULT '0000-00-00 00:00:00', `prvd_status_delivery` varchar(50) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT '',";
					$qcreate.="`prvd_status_others` varchar(50) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL DEFAULT '', `prvd_status_delivery_real` varchar(50) DEFAULT '',";
					$qcreate.="`prvd_status_delivery_err_real` varchar(50) DEFAULT '', `status_delivery` varchar(2) DEFAULT '1',";
					$qcreate.="`code_sms` varchar(50) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL DEFAULT '', `prvd_message_id` varchar(100) DEFAULT NULL,";
					$qcreate.="`prvd_message_id_real` varchar(100) DEFAULT NULL, ";
					$qcreate.="`operator_prefix_db` varchar(15) CHARACTER SET latin1 DEFAULT NULL, `client_prefix_dbname` varchar(4) CHARACTER SET latin1 DEFAULT NULL,";
					$qcreate.="`client_code_serv` varchar(2) CHARACTER SET latin1 DEFAULT NULL, `engine_name_recv` varchar(10) CHARACTER SET latin1 DEFAULT NULL,";
					$qcreate.="`ip_remote_addr` varchar(35) DEFAULT NULL, `ip_remote_host` varchar(35) DEFAULT NULL, ";
					$qcreate.="`f7_tid` varchar(100) CHARACTER SET latin1 DEFAULT NULL, `ref_id` varchar(100) CHARACTER SET latin1 DEFAULT NULL, ";
					$qcreate.="PRIMARY KEY (`id`), KEY `id-rptsendsms` (`id-rptsendsms`), KEY `time_delivery` (`time_delivery`), ";
					$qcreate.="KEY `prvd_status_delivery` (`prvd_status_delivery`), KEY `prvd_status_others` (`prvd_status_others`), ";
					$qcreate.="KEY `prvd_status_delivery_real` (`prvd_status_delivery_real`), KEY `status_delivery` (`status_delivery`), ";
					$qcreate.="KEY `code_sms` (`code_sms`), KEY `prvd_message_id` (`prvd_message_id`), KEY `prvd_message_id_real` (`prvd_message_id_real`), ";
					$qcreate.="KEY `operator_prefix_db` (`operator_prefix_db`), KEY `client_prefix_dbname` (`client_prefix_dbname`), ";
					$qcreate.="KEY `client_code_serv` (`client_code_serv`), KEY `engine_name_recv` (`engine_name_recv`), ";
					$qcreate.="KEY `ip_remote_addr` (`ip_remote_addr`), KEY `ip_remote_host` (`ip_remote_host`), KEY `f7_tid` (`f7_tid`), ";
					$qcreate.="KEY `ref_id` (`ref_id`) ) ";
					$qcreate.="ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPRESSED;";
				
					$createDB	= $this->db->queryDB($qcreate, $conn);
					if($createDB){ $tmpRs[$vopr] = "ok"; }
					else{ $tmpRs[$vopr] = "no"; }
				}// end cekDB
				$this->db->closeDB($conn);
			}
		}// end foreach opr
		foreach($tmpRs as $vRs){
			if($vRs == "no"){ $rs = "failed"; }
		}
		
		return $rs;
	}//end CreateSMSDELIVERY
	
	public function CreateTrigerRECV($fservice, $fbulan, $ftahun){
		$rs		= "ok";
		$tahun	= $ftahun;
		$bulan	= $fbulan;
		$tblName= "";
		$datetabel = $tahun."_".$bulan;
		$tmpRs	= array();
		
		foreach($this->oprtriger as $vopr){
			switch($vopr){
				case "other": $tblName = "i-".$datetabel."-smsrecv"; break;
				default:
					$tblName = "i-".$datetabel."-smsrecv-".$vopr;
			}// end switch
			$conn = $this->db->connectDb($fservice, $GLOBALS["DB_REPORT_CONFIG"]);
			$qset	= "SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED ;";
			$this->db->queryDB($qset, $conn);
			
			$qcreate ="CREATE TRIGGER `".$datetabel."_from_".$vopr."_insert_into_union_all` AFTER INSERT ON `".addslashes($tblName)."` ";
			$qcreate.="FOR EACH ROW INSERT INTO `i-".$datetabel."-union_all` (";
			$qcreate.="`id`, `id-c`, `name-c`, `id-div`, `name-div`, `id-u`, `name-u`, `user_name_tcp`, `id-m`, `id-p-s-cfg`, `name-p-s-cfg`, ";
			$qcreate.="`dest_number`, `shortnumber`, `service_id_prvd`, `model_service`, `name-sender`, `sms_req`, `hide_sms`, ";
			$qcreate.="`subject`, `sms_send`, `code_sms`, `ref_id`, `f7_tid`, `budget_code`, `time_querecv` ,`time_dbrecv`, ";
			$qcreate.="`time_sched`, `operator_prefix_db`, `client_prefix_dbname`, `client_code_serv`, `engine_name_recv`, ";
			$qcreate.="`id-rptrecv` , `counter_sms` ) VALUE ( ";
			$qcreate.="'',NEW.`id-c`, NEW.`name-c`, NEW.`id-div`, NEW.`name-div`, NEW.`id-u`, NEW.`name-u`, NEW.`user_name_tcp`, NEW.`id-m`, NEW.`id-p-s-cfg`,NEW.`name-p-s-cfg`, ";
			$qcreate.="NEW.`dest_number`, NEW.`shortnumber`, NEW.`service_id_prvd`, NEW.`model_service`, NEW.`name-sender`, NEW.`sms_req`, NEW.`hide_sms`, ";
			$qcreate.="NEW.`subject`, NEW.`sms_send`, NEW.`code_sms`, NEW.`ref_id`, NEW.`f7_tid`, NEW.`budget_code`, NEW.`time_querecv` ,NEW.`time_dbrecv`, ";
			$qcreate.="NEW.`time_sched`, NEW.`operator_prefix_db`, NEW.`client_prefix_dbname`, NEW.`client_code_serv`, NEW.`engine_name_recv`, ";
			$qcreate.="NEW.`id` , NEW.`counter_sms`);";
			
			$createDB	= $this->db->queryDB($qcreate, $conn);
			if($createDB){ $tmpRs[$vopr] = "ok"; }
			else{ $tmpRs[$vopr] = "no"; }
			$this->db->closeDB($conn);
		}// end foreach opr
		
		foreach($tmpRs as $vRs){
			if($vRs == "no"){ $rs = "failed"; }
		}
		
		return $rs;
	}// end CreateTrigerRECV
	
	public function CreateTrigerSEND($fservice, $fbulan, $ftahun){
		$rs		= "ok";
		$tahun	= $ftahun;
		$bulan	= $fbulan;
		$tblName= "";
		$tblRecv= "";
		$datetabel = $tahun."_".$bulan;
		$tmpRs	= array();
		
		foreach($this->oprtriger as $vopr){
			switch($vopr){
				case "other":
					$tblRecv = "i-".$datetabel."-smsrecv";
					$tblName = "i-".$datetabel."-smssend";
				break;
				default:
					$tblRecv = "i-".$datetabel."-smsrecv-".$vopr;
					$tblName = "i-".$datetabel."-smssend-".$vopr;
			}// end switch
			$conn = $this->db->connectDb($fservice, $GLOBALS["DB_REPORT_CONFIG"]);
			$qset	= "SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED ;";
			$this->db->queryDB($qset, $conn);
			
			$qcreate ="CREATE TRIGGER `".$datetabel."_update_from_send_to_recv_".$vopr."` BEFORE INSERT ON `".addslashes($tblName)."` ";
			$qcreate.="FOR EACH ROW UPDATE `".addslashes($tblRecv)."` SET ";
			$qcreate.="`prvd_status_send`=NEW.`prvd_status_send`, `prvd_message_id`=NEW.`prvd_message_id`, `status_send`=NEW.`status_send`, ";
			$qcreate.="`time_send`=NEW.`time_send`, `time_sent`=NEW.`time_sent` WHERE `".addslashes($tblRecv)."`.`code_sms`=NEW.`code_sms` ";
			//$qcreate.="AND `".addslashes($tblRecv)."`.`f7_tid`=NEW.`f7_tid` ";
			$qcreate.="AND `".addslashes($tblRecv)."`.`operator_prefix_db`=NEW.`operator_prefix_db`";
			$qcreate.="AND `".addslashes($tblRecv)."`.`ref_id`=NEW.`ref_id`;";
			
			$createDB	= $this->db->queryDB($qcreate, $conn);
			
			$qcreate2 ="CREATE TRIGGER `".$datetabel."_update_from_".$vopr."_to_union_all` AFTER INSERT ON `".addslashes($tblName)."` ";
			$qcreate2.="FOR EACH ROW UPDATE `i-".$datetabel."-union_all` SET ";
			$qcreate2.="`prvd_status_send`=NEW.`prvd_status_send`, `prvd_message_id`=NEW.`prvd_message_id`, `status_send`= NEW.`status_send`, ";
			$qcreate2.="`time_startsend`=NEW.`time_send`, `time_send`=NEW.`time_send`, `time_sent`=NEW.`time_sent`, ";
			$qcreate2.="`time_limitsend`=NEW.`time_limitsend`, `engine_name_send`=NEW.`engine_name_send`, `id-rptsend`=NEW.`id` ";
			$qcreate2.="WHERE `i-".$datetabel."-union_all`.`ref_id`=NEW.`ref_id` AND `i-".$datetabel."-union_all`.`code_sms`=NEW.`code_sms` ";
			$qcreate2.="AND `i-".$datetabel."-union_all`.`operator_prefix_db`=NEW.`operator_prefix_db`;";
			//$qcreate2.="AND `i-".$datetabel."-union_all`.`f7_tid`=NEW.`f7_tid`;";
			
			$createDB	= $this->db->queryDB($qcreate2, $conn);
			
			$this->db->closeDB($conn);
		}// end foreach opr
	}// end CreateTrigerSEND
	
	public function CreateTrigerDELIVERY($fservice, $fbulan, $ftahun){
		$rs		= "ok";
		$tahun	= $ftahun;
		$bulan	= $fbulan;
		$tblName= "";
		$tblRecv= "";
		$datetabel = $tahun."_".$bulan;
		$tmpRs	= array();
		
		foreach($this->oprtriger as $vopr){
			switch($vopr){
				case "other":
					$tblRecv = "i-".$datetabel."-smsrecv";
					$tblName = "i-".$datetabel."-smsdelivery";
				break;
				default:
					$tblRecv = "i-".$datetabel."-smsrecv-".$vopr;
					$tblName = "i-".$datetabel."-smsdelivery-".$vopr;
			}// end switch
			
			$conn = $this->db->connectDb($fservice, $GLOBALS["DB_REPORT_CONFIG"]);
			$qset	= "SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED ;";
			$this->db->queryDB($qset, $conn);
			
			$qcreate ="CREATE TRIGGER `".$datetabel."_update_from_delivery_to_recv_".$vopr."` BEFORE INSERT ON `".addslashes($tblName)."` ";
			$qcreate.="FOR EACH ROW UPDATE `".addslashes($tblRecv)."` SET ";
			$qcreate.="`prvd_status_delivery`=NEW.`prvd_status_delivery`, `prvd_status_delivery_real`=NEW.`prvd_status_delivery_real`, ";
			$qcreate.="`prvd_status_delivery_err_real`=NEW.`prvd_status_delivery_err_real`, `status_delivery`=NEW.`status_delivery`, ";
			$qcreate.="`time_delivery`=NEW.`time_delivery` WHERE `".addslashes($tblRecv)."` .`code_sms` = NEW.`code_sms` ";
			//$qcreate.="AND `".addslashes($tblRecv)."`.`ref_id`=NEW.`ref_id` AND `".addslashes($tblRecv)."`.`f7_tid`=NEW.`f7_tid` ";
			$qcreate.="AND `".addslashes($tblRecv)."`.`ref_id`=NEW.`ref_id` ";
			$qcreate.="AND `".addslashes($tblRecv)."`.`operator_prefix_db`=NEW.`operator_prefix_db`;";
			//$qcreate.="AND (`".addslashes($tblRecv)."`.`status_delivery`='3' OR `".addslashes($tblRecv)."`.`status_delivery`='6' OR ";
			//$qcreate.="`".addslashes($tblRecv)."`.`status_delivery`='1' OR `".addslashes($tblRecv)."`.`status_delivery`='7' OR ";
			//$qcreate.="`".addslashes($tblRecv)."`.`status_delivery`= '' OR `".addslashes($tblRecv)."`.`status_delivery` IS NULL);";
			
			$createDB	= $this->db->queryDB($qcreate, $conn);
			
			$qcreate2 ="CREATE TRIGGER `".$datetabel."_update_from_delivery_".$vopr."_to_union_all` AFTER INSERT ON `".addslashes($tblName)."` ";
			$qcreate2.="FOR EACH ROW UPDATE `i-".$datetabel."-union_all` SET ";
			$qcreate2.="`prvd_status_delivery`=NEW.`prvd_status_delivery`, `prvd_status_delivery_real`=NEW.`prvd_status_delivery_real`, ";
			$qcreate2.="`prvd_status_delivery_err_real`=NEW.`prvd_status_delivery_err_real`, `status_delivery`=NEW.`status_delivery`, ";
			$qcreate2.="`time_delivery`=NEW.`time_delivery`, `id-rptdelivery`=NEW.`id` ";
			$qcreate2.="WHERE `i-".$datetabel."-union_all`.`ref_id`=NEW.`ref_id` AND `i-".$datetabel."-union_all`.`code_sms`=NEW.`code_sms` ";
			$qcreate2.="AND `i-".$datetabel."-union_all`.`operator_prefix_db`=NEW.`operator_prefix_db` ";
			$qcreate2.="AND `i-".$datetabel."-union_all`.`prvd_message_id`=NEW.`prvd_message_id`;";
			
			//$qcreate2.="AND (`i-".$datetabel."-union_all`.`status_delivery`='3' OR `i-".$datetabel."-union_all`.`status_delivery`='6' OR ";
			//$qcreate2.="`i-".$datetabel."-union_all`.`status_delivery`='1' OR `i-".$datetabel."-union_all`.`status_delivery`='7' OR ";
			//$qcreate2.="`i-".$datetabel."-union_all`.`status_delivery`='' OR `i-".$datetabel."-union_all`.`status_delivery` IS NULL);";
			
			$createDB	= $this->db->queryDB($qcreate2, $conn);
		}// end foreach opr
	}// end CreateTrigerDELIVERY
	
	public function CreateSENDINGSMS($fservice, $fbulan, $ftahun){
		$rs		= "ok";
		$tahun	= $ftahun;
		$bulan	= $fbulan;
		$tblName= "";
		$tblRecv= "";
		$datetabel = $tahun."_".$bulan;
		$tmpRs	= array();
		
		foreach($this->opr as $vopr){
			switch($vopr){
				case "other": $tblName = "i-".$datetabel."-sendingsms"; break;
				default:
					$tblName = "i-".$datetabel."-sendingsms-".$vopr;
			}// end switch
			$conn = $this->db->connectDb($fservice, $GLOBALS["DB_HISTORY_CONFIG"]);
			$qset	= "SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED ;";
			$a = $this->db->queryDB($qset, $conn);
			
			$qc		= "SELECT id FROM `".addslashes($tblName)."` limit 1";
			$cekDB	= $this->db->queryDB($qc, $conn);
			if(!$cekDB){
				$qcreate ="CREATE TABLE IF NOT EXISTS `".addslashes($tblName)."` (";
				$qcreate.="`id` bigint(20) NOT NULL AUTO_INCREMENT, `id-smsrecv` mediumint(9) NOT NULL DEFAULT '0', ";
				$qcreate.="`id-p-s-cfg` smallint(6) NOT NULL DEFAULT '0', ";
				$qcreate.="`prvd_status_send` varchar(60) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL, ";
				$qcreate.="`prvd_message_id` varchar(100) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL, ";
				$qcreate.="`code_sms` varchar(50) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL DEFAULT '',";
				$qcreate.="`iod_path` varchar(10000) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL, ";
				$qcreate.="`f7_tid` varchar(100) CHARACTER SET latin1 DEFAULT NULL, `ref_id` varchar(50) CHARACTER SET latin1 DEFAULT NULL, ";
				$qcreate.="`time_log` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, ";
				$qcreate.="PRIMARY KEY (`id`), KEY `id-smsrecv` (`id-smsrecv`), KEY `id-p-s-cfg` (`id-p-s-cfg`), ";
				$qcreate.="KEY `prvd_status_send` (`prvd_status_send`), KEY `prvd_message_id` (`prvd_message_id`), KEY `code_sms` (`code_sms`),";
				$qcreate.="KEY `iod_path` (`iod_path`), KEY `f7_tid` (`f7_tid`), KEY `ref_id` (`ref_id`) ) ";
				$qcreate.="ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_mysql500_ci ROW_FORMAT=COMPRESSED;";
				
				$createDB	= $this->db->queryDB($qcreate, $conn);
			}// end cekDB
		}// end foreach opr
	}// end CreateSENDINGSMS
	
	public function CreateQDLVR ($fservice, $fbulan, $ftahun){
		$rs		= "ok";
		$tblName= "";
		$tahun	= $ftahun;
		$bulan	= $fbulan;
		$tmpRs	= array();
		foreach($this->dlvr as $vtbl){
			switch($vtbl){
				case "other": $tblName = "i-".$tahun."_".$bulan."-delivery"; break;
				default:
					$tblName = "i-".$tahun."_".$bulan."-delivery-".$vtbl;
			}// end switch
			$conn = $this->db->connectDb($fservice, $GLOBALS["DB_REPORT_CONFIG"]);
			if($conn){
				$qset	= "SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED ;";
				$this->db->queryDB($qset, $conn);
				$qc		= "SELECT id FROM `".addslashes($tblName)."` limit 1";
				$cekDB	= $this->db->queryDB($qc, $conn);
				if(!$cekDB){
					$qcreate ="CREATE TABLE IF NOT EXISTS `".addslashes($tblName)."` (";
					$qcreate.="`id` bigint(20) NOT NULL AUTO_INCREMENT, `id-c` mediumint(9) NOT NULL DEFAULT '0', ";
					$qcreate.="`tahun` varchar(4) CHARACTER SET latin1 DEFAULT NULL, `bulan` char(2) CHARACTER SET latin1 DEFAULT NULL, ";
					$qcreate.="`id-smsrecv` bigint(20) NOT NULL DEFAULT '0', `id-usage-sms` bigint(20) NOT NULL DEFAULT '0', ";
					$qcreate.="`id-rptsendsms` bigint(20) NOT NULL DEFAULT '0', `id-other` bigint(20) NOT NULL DEFAULT '0', ";
					$qcreate.="`dbname_other` varchar(50) CHARACTER SET latin1 DEFAULT NULL, `tblname_other` varchar(50) CHARACTER SET latin1 DEFAULT NULL, ";
					$qcreate.="`id-p-s-cfg` int(20) NOT NULL DEFAULT '0', `model_service` varchar(10) CHARACTER SET latin1 DEFAULT NULL, ";
					$qcreate.="`time_send` datetime DEFAULT NULL, `prvd_message_id` varchar(100) CHARACTER SET latin1 DEFAULT NULL, ";
					$qcreate.="`send_port_model` varchar(10) CHARACTER SET latin1 DEFAULT NULL, `code_sms` varchar(40) CHARACTER SET latin1 DEFAULT NULL, ";
					$qcreate.="`prvd_status_delivery` varchar(50) CHARACTER SET latin1 DEFAULT NULL, `prvd_status_other` varchar(50) CHARACTER SET latin1 DEFAULT NULL, ";
					$qcreate.="`prvd_message_id_real` varchar(100) CHARACTER SET latin1 DEFAULT NULL, `prvd_status_delivery_real` varchar(10) CHARACTER SET latin1 DEFAULT NULL, ";
					$qcreate.="`prvd_status_delivery_err_real` varchar(10) CHARACTER SET latin1 DEFAULT NULL, `ip_remote_addr` varchar(50) CHARACTER SET latin1 DEFAULT NULL, ";
					$qcreate.="`ip_remote_host` varchar(50) CHARACTER SET latin1 DEFAULT NULL, `others` varchar(255) CHARACTER SET latin1 DEFAULT NULL, ";
					$qcreate.="`type_delivery` enum('0','1','2') NOT NULL DEFAULT '0', `thread_no` char(255) CHARACTER SET latin1 NOT NULL DEFAULT '1', ";
					$qcreate.="`time_delivery` datetime DEFAULT NULL, `reffnum` varchar(80) CHARACTER SET latin1 DEFAULT NULL, ";
					$qcreate.="`f7_tid` varchar(100) CHARACTER SET latin1 DEFAULT NULL, `sendingParam` text CHARACTER SET utf8, ";
					$qcreate.="`ref_id` varchar(100) CHARACTER SET latin1 DEFAULT NULL, ";
					$qcreate.="`url_client_delivery_report` varchar(255) CHARACTER SET latin1 DEFAULT NULL, `operator_prefix_db` varchar(255) CHARACTER SET latin1 DEFAULT NULL, ";
					$qcreate.="`client_prefix_dbname` varchar(4) CHARACTER SET latin1 DEFAULT NULL, `client_code_serv` char(2) CHARACTER SET latin1 DEFAULT NULL, ";
					$qcreate.="`engine_name_recv` varchar(10) CHARACTER SET latin1 DEFAULT NULL, `engine_name_send` varchar(10) CHARACTER SET latin1 DEFAULT NULL, ";
					$qcreate.="`to_client_parameters` varchar(255) CHARACTER SET latin1 DEFAULT NULL, ";
					$qcreate.="`time_expire` datetime DEFAULT NULL,";
					$qcreate.="PRIMARY KEY (`id`), ";
					$qcreate.="KEY `id-c` (`id-c`), KEY `tahun` (`tahun`), KEY `bulan` (`bulan`), ";
					$qcreate.="KEY `id-smsrecv` (`id-smsrecv`), KEY `id-usage-sms` (`id-usage-sms`), KEY `id-rptsendsms` (`id-rptsendsms`), ";
					$qcreate.="KEY `id-other` (`id-other`), KEY `dbname_other` (`dbname_other`), KEY `tblname_other` (`tblname_other`), ";
					$qcreate.="KEY `id-p-s-cfg` (`id-p-s-cfg`), KEY `model_service` (`model_service`), KEY `time_send` (`time_send`), ";
					$qcreate.="KEY `prvd_message_id` (`prvd_message_id`), KEY `send_port_model` (`send_port_model`), KEY `code_sms` (`code_sms`), ";
					$qcreate.="KEY `prvd_status_delivery` (`prvd_status_delivery`), KEY `prvd_status_other` (`prvd_status_other`), ";
					$qcreate.="KEY `prvd_message_id_real` (`prvd_message_id_real`), KEY `prvd_status_delivery_real` (`prvd_status_delivery_real`), ";
					$qcreate.="KEY `prvd_status_delivery_err_real` (`prvd_status_delivery_err_real`), KEY `ip_remote_addr` (`ip_remote_addr`), ";
					$qcreate.="KEY `ip_remote_host` (`ip_remote_host`), KEY `others` (`others`),  KEY `type_delivery` (`type_delivery`), ";
					$qcreate.="KEY `thread_no` (`thread_no`), KEY `time_delivery` (`time_delivery`),  KEY `reffnum` (`reffnum`),  KEY `ref_id` (`ref_id`), ";
					$qcreate.="KEY `f7_tid` (`f7_tid`), KEY `url_client_delivery_report` (`url_client_delivery_report`),  KEY `operator_prefix_db` (`operator_prefix_db`), ";
					$qcreate.="KEY `client_prefix_dbname` (`client_prefix_dbname`), KEY `client_code_serv` (`client_code_serv`), ";
					$qcreate.="KEY `engine_name_recv` (`engine_name_recv`), KEY `engine_name_send` (`engine_name_send`),  KEY `to_client_parameters` (`to_client_parameters`) ";
					$qcreate.=") ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_mysql500_ci ROW_FORMAT=COMPRESSED;";
					
					$createDB	= $this->db->queryDB($qcreate, $conn);
					if($createDB){ $rs = "ok"; }
				}// end if cekDB
				$this->db->closeDB($conn);
			}// end if conn
		}// end foreach
	}// end CreateQDLVR
}// end class CreateTable
?>