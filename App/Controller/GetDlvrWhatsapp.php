<?php

namespace App\Controller;
use \App\Utils\Loging;
use \App\Utils\GetValue;
use \App\Utils\SendHttp;
use \App\Controller\SendToDB;

class GetDlvrWhatsapp extends \App\Controller\GetFromDB{
	protected $consumer_tag = "consumer";
	protected $logdata;
	protected $getVal;
	protected $insData;
	protected $sendhttp;
	
	public function __construct () {
		$this->logdata	= new \App\Utils\Loging;
		$this->getVal	= new \App\Utils\GetValue;
		$this->insData	= new \App\Controller\SendToDB;
		$this->sendhttp	= new \App\Utils\SendHttp;
	}
	
	public function getDbDlvr ($arrData) {
		$GLOBALS["logname"] = $arrData["logname"];
		$GLOBALS["logproces"] = $GLOBALS["logname"];
		$tblDlvr	= array("thn"=>$arrData["tahun"],"bln"=>$arrData["bulan"],"srvc"=>$arrData['service'],"prefix"=>$arrData['prefix'],"nmtbl"=>"delivery");
		$nmTblDlvr	= $this->getVal->getNameTable($tblDlvr);
		
		$threadno = "";
		foreach($arrData["thread_no"] as $v ){ $threadno.= "'".$v."',";}
		$threadno = substr($threadno, 0, -1);
		
		$q = "SELECT * FROM `".$nmTblDlvr."` where ( RIGHT(`id`,1) IN (".$threadno.")) AND `client_prefix_dbname`='".$arrData['client']."' ";
		$q.= "AND `time_expire` >= NOW() ORDER BY `id` ASC LIMIT 100";
		$cQdlvr = array(
			"q" => $q,
			"dbname" => $GLOBALS["DB_CONFIG_TRX"],
			"confdb" => $GLOBALS["DB_TRX_CONFIG"]
		);


		//$tmplog = array(

		//	'tblDlvr' => $tblDlvr,

		//	'nmTblDlvr' => $nmTblDlvr,

		//	'arrData' => $arrData,

		//	'cQdlvr' => $cQdlvr

		//);

		//$this->logdata->write(__FUNCTION__, '##DIGEBUKIN## --> '.json_encode($tmplog), $GLOBALS["logproces"]);
		$this->setData($cQdlvr);
		$gdata = $this->get_data_db();
		if($gdata != "failed" && count($gdata) > 0){
			foreach($gdata as $v){
				$tFromOpr = "q-deliveryfromoperator-".strtolower($arrData["prefix"]);
				$sFromOpr = "SELECT * FROM `".$tFromOpr."` where `prvd_message_id`='".$v["prvd_message_id"]."'";
				$cFromOpr = array(
					"q"	=> $sFromOpr,
					"dbname" => $GLOBALS["DB_CONFIG_TRX"],
					"confdb" => $GLOBALS["DB_TRX_CONFIG"]
				);
				$this->setData($cFromOpr);
				$gstatus = $this->get_data_db();
				if($gstatus != "failed" && count($gstatus) > 0){
					$arrParam = array(
						"dt" => $arrData,
						"dlvr" => $v,
						"sts" => $gstatus,
						"tbldlvr" => $nmTblDlvr,
						"oprdlvr" => $tFromOpr
					);
					$this->genDlvr($arrParam);
				}// end if count gstatus
			}// end foreach gdata
		}// end if count gdata
		sleep(1);
	}// end getDbDlvr
	
	public function genDlvr ($v) {
		$tblSmsDlvr = array("thn"=>$v["dlvr"]["tahun"],"bln"=>$v["dlvr"]["bulan"],"srvc"=>$v["dt"]['service'],"prefix"=>$v["dt"]['prefix'],"nmtbl"=>"smsdelivery");
		$nmTblSmsDlvr = $this->getVal->getNameTable($tblSmsDlvr);
		$urldlvr = $v["dlvr"]["url_client_delivery_report"];
		$nmClient = $v["dlvr"]["client_prefix_dbname"];
		
		foreach($v["sts"] as $vdlvr){
			$stsdlvr = $this->getVal->getWhatsappSts($vdlvr["prvd_status_delivery"]);
			if($stsdlvr != "1"){
				$uptSmsDlvr	= array(
					"arrdata"	=> array("prvd_status_delivery" => $stsdlvr, "time_delivery" => $vdlvr["time_delivery"]),
					"dtwhere"	=> array("id" => $v["dlvr"]['id']),
					"tablename"	=> $v["tbldlvr"],
					"dbname"	=> "cidx",
					"confdb"	=> $GLOBALS["DB_REPORT_CONFIG"]
				);
				$this->setData($uptSmsDlvr);
				$this->updateData();
			}
			$arrSmsDlvr = array(
				"time_delivery"=>$vdlvr["time_delivery"],
				"prvd_status_delivery"=>$vdlvr["prvd_status_delivery"],
				"prvd_status_others"=>$vdlvr["prvd_status_other"],
				"prvd_message_id_real"=>$vdlvr["prvd_message_id_real"],
				"prvd_status_delivery_real"=>$vdlvr["prvd_status_delivery_real"],
				"prvd_status_delivery_err_real"=>$vdlvr["prvd_status_delivery_err_real"],
				"status_delivery"=>$stsdlvr,
				"code_sms"=>$v["dlvr"]['code_sms'],
				"f7_tid"=>$v["dlvr"]["f7_tid"],
				"prvd_message_id"=>$v["dlvr"]['prvd_message_id'],
				"ip_remote_addr"=>$vdlvr["ip_remote_addr"],
				"ip_remote_host"=>$vdlvr["ip_remote_host"],
				"operator_prefix_db"=>$v["dlvr"]["operator_prefix_db"],
				"client_prefix_dbname"=>$v["dlvr"]["client_prefix_dbname"],
				"client_code_serv"=>$v["dlvr"]['client_code_serv'],
				"engine_name_recv"=>$v["dlvr"]['engine_name_recv'],
				"ref_id"=>$v["dlvr"]['ref_id']
			);
			$inSmsDlvr	= array(
				"arrdata"	=> $arrSmsDlvr,
				"tablename"	=> $nmTblSmsDlvr,
				"dbname"	=> $v["dlvr"]["client_prefix_dbname"],
				"confdb"	=> $GLOBALS["DB_REPORT_CONFIG"]
			);
			$this->insData->setData($inSmsDlvr);
			$this->insData->insertData();
			
			//==== start delete from qdelivery
			if(strtolower($vdlvr["prvd_status_delivery"]) == "read"){
				$sDelDlvr = "DELETE FROM `".$v["tbldlvr"]."` where `id`='".$v["dlvr"]["id"]."'";
				$delDlvr = array(
					"q"	=> $sDelDlvr,
					"dbname" => $GLOBALS["DB_CONFIG_TRX"],
					"confdb" => $GLOBALS["DB_TRX_CONFIG"]
				);
				$this->insData->setData($delDlvr);
				$this->insData->deleteData();
			}
			//==== end delete from qdelivery
			
			//==== start delete from qdeliveryfromoperator
			$sDelFromOpr = "DELETE FROM `".$v["oprdlvr"]."` where `id`='".$vdlvr["id"]."'";
			$delFromOpr = array(
				"q"	=> $sDelFromOpr,
				"dbname" => $GLOBALS["DB_CONFIG_TRX"],
				"confdb" => $GLOBALS["DB_TRX_CONFIG"]
			);
			$this->insData->setData($delFromOpr);
			$this->insData->deleteData();
			//==== end delete from qdeliveryfromoperator
			
			$arrStsDlvr = array(
				"ref_id" => $v["dlvr"]['ref_id'],
				"code_sms" => $v["dlvr"]['code_sms'],
				"type_status" => "dlvr",
				"status" => $stsdlvr,
				"channel" => $v["dlvr"]["operator_prefix_db"],
				"time_dlvr" => strtotime($vdlvr["time_delivery"])
			);
			$router = "x.stsclient";
			$severity = $router.".".$nmClient;
			$dtDlvr = json_encode($arrStsDlvr);
			$arrDlvr = array("url"=>$urldlvr,"dtSts"=>$dtDlvr);
			$jsondata = $this->parsVal->CreateJson($arrDlvr);
			$arrInMq = array(
				"router"	=> $router,
				"severity"	=> $severity,
				"jsonmq"	=> $jsondata,
				"confmq"	=> $GLOBALS["CONF_MQ_SMS"]
			);
			$this->setData($arrInMq);
			$this->insertMq();
		}// end foreach dlvr
	}// end getDbDlvr
}// end GetDlvrEmail
?>
