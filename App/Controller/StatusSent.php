<?php

namespace App\Controller;
use \App\Utils\Loging;
use \App\Utils\SendHttp;
use PhpAmqpLib\Connection\AMQPConnection;

class StatusSent {
	protected $consumer_tag = "consumer";
        protected $logdata;
	protected $sendFunc;
	
	public function __construct () {
                $this->logdata  = new \App\Utils\Loging;
		$this->sendFunc	= new \App\Utils\SendHttp;
	}

	public function shutdown($ch, $conn) {
		$ch->close();
		$conn->close();
	}

	public function sentStatus ($arrData) {
		$GLOBALS["logname"] = $arrData["logname"];
		$conn = new AMQPConnection($arrData["confmq"]["host"], $arrData["confmq"]["port"], $arrData["confmq"]["user"], $arrData["confmq"]["pass"], $arrData["confmq"]["vhost"]);
		if($conn){
			$ch = $conn->channel();
			$ch->queue_declare($arrData["queue"], false, true, false, false);
			$ch->exchange_declare($arrData["exchange"], 'topic', false, true, false);
			$ch->queue_bind($arrData["queue"], $arrData["exchange"]);
			$ch->basic_consume($arrData["queue"], $this->consumer_tag, false, false, false, false, array($this, $arrData["funcdata"]));
			register_shutdown_function(array($this, 'shutdown'), $ch, $conn);
			
			while (count($ch->callbacks)) {
				$ch->wait();
			}
		}
	}// end sentStatus
	
	public function processStsSent ($msg) {
		$data = json_decode($msg->body);
		$check = array_filter(get_object_vars($data));
		if($check > 0){
			//$urlSent = $check["url"];
			$urlSent = ($check["url"]=="")?"http://172.27.0.86:8081/json/callback":$check["url"];
			$dtSent = $check["dtSts"];
			$tmplog = "Callback Status Sent : url ".$urlSent." ".$dtSent."\n";
			$this->logdata->write(__FUNCTION__, $tmplog, $GLOBALS["logname"]);
			$hitSent = $this->sendFunc->sendBackData($dtSent, $urlSent);
			$this->logdata->write(__FUNCTION__, "Resp : ".$hitSent["response"], $GLOBALS["logname"]);
			$rsSent	= strtolower($hitSent["response"]);
			if(substr_count($rsSent, "error") > 0){
				// Reject and requeue message to RabbitMQ
				$msg->delivery_info['channel']->basic_reject($msg->delivery_info['delivery_tag'], true);
			}else{
				// Recv delete count
				$msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
			}
			// quit
			//$msg->delivery_info['channel']->basic_cancel($msg->delivery_info['consumer_tag']);
		}
	}// end processStsSent
	
	public function processStsDlvr ($msg) {
		$data = json_decode($msg->body);
		$check = array_filter(get_object_vars($data));
		if($check > 0){
			//$urlDlvr = $check["url"]
			$urlDlvr = ($check["url"]=="")?"http://172.27.0.86:8081/json/callback":$check["url"];
			$dtDlvr = $check["dtSts"];
			$tmplog = "Callback Status Dlvr : url ".$urlDlvr." ".$dtDlvr."\n";
			$this->logdata->write(__FUNCTION__, $tmplog, $GLOBALS["logname"]);
			$hitSent = $this->sendFunc->sendBackData($dtDlvr, $urlDlvr);
			$this->logdata->write(__FUNCTION__, "Resp : ".$hitSent["response"], $GLOBALS["logname"]);
			$rsSent	= strtolower($hitSent["response"]);
			if(substr_count($rsSent, "error") > 0){
				// Reject and requeue message to RabbitMQ
				$msg->delivery_info['channel']->basic_reject($msg->delivery_info['delivery_tag'], true);
			}else{
				// Recv delete count
				$msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
			}
			// quit
			//$msg->delivery_info['channel']->basic_cancel($msg->delivery_info['consumer_tag']);
		}
	}// end processStsDlvr
}
?>
