<?php

namespace App\Controller;
use \App\Utils\Loging;
use \App\Utils\ParsData;
use \App\Utils\SendHttp;
use PhpAmqpLib\Connection\AMQPConnection;

class SendDlvr extends \App\Controller\SendDlvrFunc {
	protected $consumer_tag = "consumer";
	protected $logdata;
	protected $parsVal;
	protected $sendProvider;
	
	public function __construct () {
		$this->logdata	= new \App\Utils\Loging;
		$this->parsVal	= new \App\Utils\ParsData;
		$this->sendProvider	= new \App\Utils\SendHttp;
	}

	public function sendMqDlvr ($arrData) {
		$GLOBALS["logproces"] = $arrData["logname"];
		$conn = new AMQPConnection($arrData["confmq"]["host"], $arrData["confmq"]["port"], $arrData["confmq"]["user"], $arrData["confmq"]["pass"], $arrData["confmq"]["vhost"]);
		if($conn){
			$ch = $conn->channel();
			$ch->queue_declare($arrData["queue"], false, true, false, false);
			$ch->exchange_declare($arrData["exchange"], 'topic', false, true, false);
			$ch->queue_bind($arrData["queue"], $arrData["exchange"]);
			$ch->basic_consume($arrData["queue"], $this->consumer_tag, false, false, false, false, array($this, $arrData["funcdata"]));
			register_shutdown_function(array($this, 'shutdown'), $ch, $conn);
			
			while (count($ch->callbacks)) {
				$ch->wait();
			}
		}
	}// end sendMqData
	
	public function processDlvr ($msg) {
		$data	= json_decode($msg->body);
		$arrData=$this->parsVal->getDataArrDlvr($data);
		$this->setDataSend($arrData);
		$this->sendToClient();
		
		// Recv delete count
		$msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
		// quit
		//$msg->delivery_info['channel']->basic_cancel($msg->delivery_info['consumer_tag']);
	}// end processSms
	
	public function shutdown($ch, $conn) {
		$ch->close();
		$conn->close();
	}
}
?>