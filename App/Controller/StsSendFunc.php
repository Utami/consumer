<?php

namespace App\Controller;
use \App\Utils\Loging;
use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Message\AMQPMessage;

class StsSendFunc extends \App\Utils\Loging {
		
	public function insert_to_mq ($arrData) {
		$dataMq = $arrData["datamq"];
		$GLOBALS["logname"] = $arrData["logname"];
		$conn	= new AMQPConnection($dataMq["confmq"]["host"], $dataMq["confmq"]["port"], $dataMq["confmq"]["user"], $dataMq["confmq"]["pass"], $dataMq["confmq"]["vhost"]);
		if($conn){
			$channel= $conn->channel();
			$channel->basic_qos(0, 1, false);
			$dlvrmode =  array('delivery_mode' => 2);
			$msgmq	= new AMQPMessage($dataMq["jsonmq"], $dlvrmode);
			$channel->basic_publish($msgmq, $dataMq["router"], $dataMq["severity"]);
			
			$channel->close();
			$conn->close();
		}else{ print_r("error"); }
	}// end insert_to_mq
}
?>