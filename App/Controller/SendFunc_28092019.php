<?php
namespace App\Controller;
class SendFunc extends \App\Controller\SendToDB {
	protected $dataSend;
	
	public function setDataSend($dt){ $this->dataSend = $dt; }// end setData
	public function getDataSend(){ return $this->dataSend; }// end getData
	
	public function sendToOpr () {
		/*print_r($this->getDataSend());
		exit;*/
		$arrData= $this->getDataSend();
		$iThn	= substr($arrData['time_dbrecv'],0,4);
		$iBln	= substr($arrData['time_dbrecv'],5,2);
		$tgl	= array("thn"=>$iThn,"bln"=>$iBln);
		$nmClient = $arrData['client_prefix_dbname'];
		$prefix	= $arrData['operator_prefix_db'];
		
		$fsleep = $this->getVal->getSleep($prefix, $arrData['engine_name_recv']);
		usleep(intval($fsleep));
		$dtParam = preg_replace_callback('!s:(\d+):"(.*?)";!', function($m) { return 's:'.strlen($m[2]).':"'.$m[2].'";'; }, $arrData["sendingParam"]);
		$sendParam	= unserialize($dtParam);
		
		$fileName	= $sendParam["file_name"];
		$dlrmask	= $sendParam["dlrmask"];
		$dlrurl		= $sendParam["dlrurl"];
		$ipNo		= $sendParam["send_ip_no"];
		$port		= $sendParam["send_port"];
		$paramVal['code_sms']	= $arrData['code_sms'];
		$paramVal['method']		= $sendParam["send_method"];
		$paramVal['portversion']= $sendParam["send_port_vers"];
		$paramVal['portmodel']	= strtolower($sendParam["send_port_model"]);
		$paramVal['prvd_service_id'] = $sendParam["service_id_prvd"];
		if ($paramVal['portmodel'] == "gsm"){ $doSendViaGsm = "Y"; }else{ $doSendViaGsm = "N"; }
		$paramVal['to']	= $arrData['dest_number'];
		$paramVal['sms']= $arrData['sms_send'];
		$paramVal['date_now']	= date('YmdHis');
		$c	= $this->getVal->GetMyMicrotime6();
		$paramVal['date_micr_now']		= $paramVal['date_now']."$c";
		$paramVal['send_mesg_format']	= $sendParam["send_mesg_format"];
		$paramVal['user']				= $sendParam["send_username"];
		$paramVal['pass']				= $sendParam["send_password"];
		$paramVal['from']				= $arrData['sender_name'];
		if ($paramVal['from'] == "") { $paramVal['from']	= $this->senderTagDefault; }
		$paramVal['type_delivery']	= $sendParam["type_delivery"];
		$paramVal['shortnumber']	= $sendParam["shortnumber"];
		$paramVal['message_send']	= $sendParam["message_send"];
		$paramVal['message_statusdelivery']	= $sendParam["message_statusdelivery"];
		$paramVal['moremessage_send']		= $sendParam["moremessage_send"];
		$paramVal['moremessage_statusdelivery']	= $sendParam["moremessage_statusdelivery"];
		$paramVal['getresp_statussend']			= $sendParam["getresp_statussend"];
		$paramVal['getresp_statusdelivery']		= $sendParam["getresp_statusdelivery"];
		$paramVal['time_dbrecv']				= $arrData['time_dbrecv'];
		
		$f7_tid=($arrData['f7_tid'] == '0' OR  $arrData['f7_tid'] == '') ? $this->getVal->GetF7TrxID($arrData['dest_number'],$arrData['client_code_serv'],"8") : $arrData['f7_tid'];
		$tempTid= "";
		for ($lenTid=0; $lenTid < (29 - strlen($f7_tid)); $lenTid++) { $tempTid .=  "0"; }
		$f7_tid	= substr($tempTid.$f7_tid,-29);
		
		$f7_sendId	= $this->getVal->GetF7TrxID($arrData['dest_number'],$arrData['client_code_serv']);
		$paramVal['tid']	= $f7_sendId;
		$paramVal['f7_tid']	= $f7_tid;
		
		$paramQuery=$sendParam["parameter"];
		$iod_path = $paramQuery;
		$dlrurl = str_replace("@@t@",$paramVal['date_now'],$dlrurl);
		$dlrurl = str_replace("@@tm@",$paramVal['date_micr_now'],$dlrurl);
		$dlrurl = str_replace("@@f7_tid@",$paramVal['f7_tid'],$dlrurl);
		$dlrurl = str_replace("@@tid@",$paramVal['tid'],$dlrurl);
		$parts	= parse_url($dlrurl);
		
		if (isset($parts['query'])) {

			parse_str($parts['query'], $partsquery);
			if (isset($partsquery['_tid'])) $paramVal['_tid'] = $partsquery['_tid'];
		}
		
		$arrTDS	= array(" ","-",":");
		$tds	= str_replace($arrTDS,"",$arrData['time_dbrecv']);
		$iod_path	= str_replace("@@tds@",$tds,"$iod_path");
		
		// Transaction Date from operator
		$iod_path = str_replace("@@td@",$paramVal['time_dbrecv'],"$iod_path");
		// Service ID from operator
		$iod_path = str_replace("@@sid@",$paramVal['prvd_service_id'],"$iod_path");
		
		// Transaction ID from operator, for this case, push no need transaction from operator
		$iod_path = str_replace("@@tid@",$paramVal['tid'],"$iod_path");
		$iod_path = str_replace("@@f7_tid@",$paramVal['f7_tid'],"$iod_path");
		
		// Time as Transaction ID
		$iod_path = str_replace("@@t@",$paramVal['date_now'],"$iod_path");
		// Time in microtime as Transaction ID
		$iod_path = str_replace("@@tm@",$paramVal['date_micr_now'],"$iod_path");
		// user
		$iod_path = str_replace("@@u@",$paramVal['user'],"$iod_path");
		// password
		$iod_path = str_replace("@@p@",$paramVal['pass'],"$iod_path");
		// sender/from
		$iod_path = str_replace("@@s@",$paramVal['from'],"$iod_path");
		
		// destination/to
		$iod_path = str_replace("@@d@",$paramVal['to'],"$iod_path");
		$iod_path = str_replace("@@dlrmask@",$dlrmask,"$iod_path");
		$iod_path = str_replace("@@dlrurl@",urlencode($dlrurl),"$iod_path");
		
		$paramsendmformat=$paramVal['send_mesg_format'];
		switch($paramsendmformat){
		  case "nothing": $paramVal['sms']=$paramVal['sms']; $paramVal['to']=$paramVal['to']; break;
		  case "htmlentities": $paramVal['sms']=htmlentities($paramVal['sms']); $paramVal['to']=htmlentities($paramVal['to']); break;
		  default:
		  	$paramVal['sms'] = urlencode($paramVal['sms']);
			$paramVal['to'] = urlencode($paramVal['to']);
		}// end switch paramsendmformat
		$iod_path = str_replace("@@m@",$paramVal['sms'],"$iod_path");
		$timeToOpr	= date("Y-m-d H:i:s");
		
		if ($doSendViaGsm == "Y") { /*blm dibkin funcnya */ }
		else{
			$htipe="http";
			switch($paramVal['portmodel']){
				case "http": $htipe="http"; break;
				case "https": $htipe="https"; break;
				case "smpp": $htipe="smpp"; break;
			}// end switch portmodel
			$maxtime = 30;
			$keepAlive=TRUE;
			if(strtolower($prefix) == "isat"){
				$maxtime = 2;
				$keepAlive=FALSE;
			}
			//$ret = array("error" => 0, "trx_date" =>20190617114824, "full_iod"=> "http://10.8.3.231:2001/bank/push_bulk.jsp?appsid=mitra_prem&pwd=m1tR4A2&sid=mitrasprintsprint&msisdn=6282122902978&sms=haloo+testing&trx_id=S11297814490423397723", "response" => "0_11297814490423397723");
			$ret = $this->sendProvider->sendToProvider($paramVal,$fileName,$iod_path,$ipNo,$port,$htipe,$maxtime,$keepAlive); //provider curl
			if($prefix == "tsel" && substr_count(strtolower($ret['response']), "connection") == 0 ){
				$ret['response'] = $ret['response']."_".$paramVal['tid'];
			}
		}// end else $doSendViaGsm <> "Y"
		$timeRecvOpr	= date("Y-m-d H:i:s");
		//log from operator
		$tmplog		= $arrData['dest_number']." Method Send :".$paramVal['method'];
		$this->logdata->write(__FUNCTION__, $tmplog, $GLOBALS["logproces"]);
		$tmplog		= $arrData['dest_number']." From Opr Error :".$ret['error'];
		$this->logdata->write(__FUNCTION__, $tmplog, $GLOBALS["logproces"]);
		$tmplog		= $arrData['dest_number']." From Opr Trx_date :".$ret['trx_date'];
		$this->logdata->write(__FUNCTION__, $tmplog, $GLOBALS["logproces"]);
		$tmplog		= $arrData['dest_number']." From Opr Full_iod :".$ret['full_iod'];
		$this->logdata->write(__FUNCTION__, $tmplog, $GLOBALS["logproces"]);
		$tmplog		= $arrData['dest_number']." From Opr Response Pars:".$ret['response'];
		$this->logdata->write(__FUNCTION__, $tmplog, $GLOBALS["logproces"]);
		
		$response	= $this->getVal->GetValSendResponse($ret,$paramVal);
		//log to db
		$tmplog		= $arrData['dest_number']." ToDb StatusSend :".$response['status_send'];
		$this->logdata->write(__FUNCTION__, $tmplog, $GLOBALS["logproces"]);
		$tmplog		= $arrData['dest_number']." ToDb Errmsg :".$response['errmsg'];
		$this->logdata->write(__FUNCTION__, $tmplog, $GLOBALS["logproces"]);
		$tmplog		= $arrData['dest_number']." ToDb Status :".$response['status'];
		$this->logdata->write(__FUNCTION__, $tmplog, $GLOBALS["logproces"]);
		$tmplog		= $arrData['dest_number']." ToDb Code :".$response['code'];
		$this->logdata->write(__FUNCTION__, $tmplog, $GLOBALS["logproces"]);
		
		if ($response['status_send'] == "2" && $prefix == "tsel"){ $response['code']=$ret['response']; }
		$sts = "2";
		if ($response['status_send'] == 2){ $sts="0"; }else{ $sts="1"; }
		$reff_expld	= explode("_", $arrData['ref_id']);
		// insert smssend
		$iSmsSend	= array(
						"code_sms"=>$arrData['code_sms'],"prvd_status_send"=>$response['status'],"prvd_message_id"=>$response['code'],"f7_tid"=>$f7_tid,
						"status_send"=>$sts,"time_send"=>$timeToOpr,"operator_prefix_db"=>$prefix,"id-p-s-cfg"=>$arrData["id_p_s_cfg"],
						"client_prefix_dbname"=>$nmClient,"client_code_serv"=>$arrData['client_code_serv'],"engine_name_recv"=>$arrData['engine_name_recv'],
						"ref_id"=>$reff_expld[0],"operator_prefix_db"=>$arrData['operator_prefix_db'],"time_sent"=>$timeRecvOpr
					);
		$tblSmsSend	= array("thn"=>$tgl["thn"],"bln"=>$tgl["bln"],"srvc"=>$arrData['engine_name_recv'],"prefix"=>$prefix,"nmtbl"=>"smssend");
		$nmTblSmsSend	= $this->getVal->getNameTable($tblSmsSend);
		$insSmsSend	= array(
						"arrdata"	=> $iSmsSend,
						"tablename"	=> $nmTblSmsSend,
						"dbname"	=> $nmClient,
						"confdb"	=> $GLOBALS["DB_REPORT_CONFIG"]
					);
		$this->setData($insSmsSend);
		$this->insertData();
		// end insert smssend
		
		// insert history
		$tblHistory		= array("thn"=>$tgl["thn"],"bln"=>$tgl["bln"],"srvc"=>$arrData['engine_name_recv'],"prefix"=>$prefix,"nmtbl"=>"sendingsms");
		$nmTblHistory	= $this->getVal->getNameTable($tblHistory);
		$full_iod		= (empty($ret['full_iod']) || $ret['full_iod']=='') ? "Not Set" : $ret['full_iod'];
		$reff_expld		= explode("_", $arrData['ref_id']);
		$iHistory		= array(
			"id-p-s-cfg"=>$arrData['id_p_s_cfg'],"prvd_status_send"=>$response['status'],"prvd_message_id"=>$response['code'],
			"iod_path"=>$full_iod,"code_sms"=>$arrData['code_sms'],"ref_id"=>$reff_expld[0],"time_log"=>$timeToOpr,"f7_tid"=>$f7_tid
		);
		$insHistory	= array(
						"arrdata"	=> $iHistory,
						"tablename"	=> $nmTblHistory,
						"dbname"	=> $nmClient."_history",
						"confdb"	=> $GLOBALS["DB_REPORT_CONFIG"]
					);
		$this->setData($insHistory);
		$this->insertData();
		// end insert history
		
		$arrStsDlvr = array();
		if ($response['status_send'] != "2" ){ // sms send to operator
			// insert qdelivery
			$texpired = 1440;
			if($sendParam["expire_minute"] != "" || $sendParam["expire_minute"] != 0){
				$texpired = intval($sendParam["expire_minute"]);
			}
			$addexpired = "+".$texpired." minute";
			$time_expired = date('Y-m-d H:i:s',date(strtotime($addexpired, strtotime($timeToOpr))));
			$iQdlvr	= array(
							"id-c"=>$arrData['id_c'],"tahun"=>$tgl["thn"],"bulan"=>$tgl["bln"],"model_service"=>$arrData['model_service'],
							"time_send"=>$timeToOpr,"time_expire"=>$time_expired,"prvd_message_id"=>$response['code'],"send_port_model"=>$paramVal['portmodel'],
							"code_sms"=>$arrData['code_sms'],"time_delivery"=>$timeToOpr,"type_delivery"=>$paramVal['type_delivery'],"f7_tid"=>$f7_tid,"ref_id"=>$reff_expld[0],
							"sendingParam"=>$arrData['sendingParam'],"id-p-s-cfg"=>$arrData["id_p_s_cfg"],"operator_prefix_db"=>$prefix,
							"client_prefix_dbname"=>$nmClient,"client_code_serv"=>$arrData['client_code_serv'],"engine_name_recv"=>$arrData['engine_name_recv'],
							"url_client_delivery_report"=>$arrData['url_callback']
						);
			$tblDlvr	= array("thn"=>$tgl["thn"],"bln"=>$tgl["bln"],"srvc"=>$arrData['engine_name_recv'],"prefix"=>$prefix,"nmtbl"=>"delivery");
			$nmTblDlvr	= $this->getVal->getNameTable($tblDlvr);
			$inQdlvr	= array(
								"arrdata"	=> $iQdlvr,
								"tablename"	=> $nmTblDlvr,
								"dbname"	=> $GLOBALS["DB_CONFIG_TRX"],
								"confdb"	=> $GLOBALS["DB_TRX_CONFIG"]
							);
			$this->setData($inQdlvr);
			$this->insertData();
			// end insert qdelivery
		}else{
			// insert smsdelivery
			$arrSmsDlvr=array(
				"time_delivery"=>$timeToOpr,
				"prvd_status_delivery"=>"SPRINT UNSENT",
				"prvd_status_others"=>"",
				"prvd_message_id_real"=>"",
				"prvd_status_delivery_real"=>"",
				"prvd_status_delivery_err_real"=>"",
				"status_delivery"=>"5",
				"code_sms"=>$arrData['code_sms'],
				"f7_tid"=>$f7_tid,
				"ref_id"=>$reff_expld[0],
				"prvd_message_id"=>$response['code'],
				"ip_remote_addr"=>"",
				"ip_remote_host"=>"",
				"operator_prefix_db"=>$prefix,
				"client_prefix_dbname"=>$nmClient,
				"client_code_serv"=>$arrData['client_code_serv'],
				"engine_name_recv"=>$arrData['engine_name_recv']
			);
			$tblSmsDlvr		= array("thn"=>$tgl["thn"],"bln"=>$tgl["bln"],"srvc"=>$arrData['engine_name_recv'],"prefix"=>$prefix,"nmtbl"=>"smsdelivery");
			$nmTblSmsDlvr	= $this->getVal->getNameTable($tblSmsDlvr);
			$inSmsDlvr		= array(
				"arrdata"	=> $arrSmsDlvr,
				"tablename"	=> $nmTblSmsDlvr,
				"dbname"	=> $nmClient,
				"confdb"	=> $GLOBALS["DB_REPORT_CONFIG"]
			);
			$arrStsDlvr		= array(
				"ref_id" => $arrData['ref_id'],
				"code_sms" => $arrData['code_sms'],
				"type_status" => "dlvr",
				"status" => "5",
				"channel" => $arrData["channel"],
				"time_dlvr" => strtotime($timeRecvOpr)
			);
			$this->setData($inSmsDlvr);
			$this->insertData();
			// end insert smsdelivery
		}
		//setcallback
		$urlCallBack	= $arrData['url_callback'];
		
		// insert MQ Status Sent To Client
		$arrStsSent = array(
			"ref_id" => $arrData['ref_id'],
			"code_sms" => $arrData['code_sms'],
			"type_status" => "send",
			//"status" => "1",
			"status" => $sts,
			"channel" => $arrData["channel"],
			"time_send" => strtotime($timeToOpr),
			"time_prov_recv"=> strtotime($timeRecvOpr)
		);
		$router = "x.stssend";
		$severity = $router.".".$nmClient;
		$dtSent = json_encode($arrStsSent);
		$arrSent = array("url"=>$urlCallBack,"dtSts"=>$dtSent);
		$jsondata = $this->parsVal->CreateJson($arrSent);
		$arrInMq = array(
			"router"	=> $router,
			"severity"	=> $severity,
			"jsonmq"	=> $jsondata,
			"confmq"	=> $GLOBALS["CONF_MQ_SMS"]
		);
		$this->setData($arrInMq);
		$this->insertMq();
		// end insert MQ Status Sent To Client
		// insert MQ Status Dlvr To Client
		if(count($arrStsDlvr) > 0){
			$router		= "x.stsclient";
			$severity	= $router.".".$nmClient;
			$dtDlvr		= json_encode($arrStsDlvr);
			$arrDlvr	= array("url"=>$urlCallBack,"dtSts"=>$dtDlvr);
			$jsondata	= $this->parsVal->CreateJson($arrDlvr);
			$arrInMq	= array(
				"router"	=> $router,
				"severity"	=> $severity,
				"jsonmq"	=> $jsondata,
				"confmq"	=> $GLOBALS["CONF_MQ_SMS"]
			);
			$this->setData($arrInMq);
			$this->insertMq();
		}
		// end insert MQ Status Dlvr To Client
	}// end sendToOpr
	
	public function sendToEmail () {
		$arrData		= $this->getDataSend();
		$dtParam		= preg_replace_callback('!s:(\d+):"(.*?)";!', function($m) { return 's:'.strlen($m[2]).':"'.$m[2].'";'; }, $arrData["sendingParam"]);
		$sendParam		= unserialize($dtParam);
		$from_email		= "";
		$from_subject	= $arrData["subject"];
		if(strtolower($arrData['sms_send']) != "file"){
			$from_body		= '<html><meta charset="UTF-8"><meta content="width=device-width, initial-scale=1" name="viewport"><meta name="x-apple-disable-message-reformatting"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta content="telephone=no" name="format-detection"><title></title><!--[if (mso 16)]><style type="text/css">a {text-decoration: none;}</style><![endif]--><!--[if gte mso 9]><style>sup { font-size: 100% !important; }</style><![endif]--><!--[if !mso]><!-- --><link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i" rel="stylesheet"><!--<![endif]-->';
			$bodyemail		= urldecode($arrData["msgdetail"]);
			$bodyemail		= str_replace("&sol;", "/", $bodyemail);
			$from_body		.= $bodyemail.'</html>';
		}else{
			$from_body		= '<html><meta charset="UTF-8"><meta content="width=device-width, initial-scale=1" name="viewport"><meta name="x-apple-disable-message-reformatting"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta content="telephone=no" name="format-detection"><title></title><!--[if (mso 16)]><style type="text/css">a {text-decoration: none;}</style><![endif]--><!--[if gte mso 9]><style>sup { font-size: 100% !important; }</style><![endif]--><!--[if !mso]><!-- --><link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i" rel="stylesheet"><!--<![endif]-->'.$arrData['msgdetail'].'</body></html>';
		}
		
		$sendto			= $arrData["sendto"];
		$attach			= $arrData["attachment"];
		
		$smtp_user	= $sendParam["send_username"];
		$smtp_pass	= $sendParam["send_password"];
		$host = explode(":;:", $sendParam["send_ip_no"]);
		$smtp_host	= $host[0];
		$from_name	= $host[2];
		$smtp_ssl	= $sendParam["send_port_vers"];
		$smtp_port	= $sendParam["send_port"];
		
		$f7_tid=($arrData['f7_tid'] == '0' OR  $arrData['f7_tid'] == '') ? $this->getVal->GetF7TrxID($arrData['dest_number'],$arrData['client_code_serv'],"8") : $arrData['f7_tid'];
		$tempTid= "";
		for ($lenTid=0; $lenTid < (29 - strlen($f7_tid)); $lenTid++) { $tempTid .=  "0"; }
		$f7_tid	= substr($tempTid.$f7_tid,-29);
		
		$sts		= "0";
		$stsDlvr	= "2";
		$timeToOpr	= date("Y-m-d H:i:s");
		$timeprvd	= str_replace(":", "", str_replace("-", "", $timeToOpr));
		$prvd		= $sendto.str_replace(" ", "", $timeprvd);
		// send email
		if(!empty($smtp_user) && !empty($smtp_pass)){
			
			if($sendParam["send_method"] == "infobip"){
				$contentHeader = $this->parsVal->getHeader($smtp_user, $smtp_pass);
				$tmplog = "Header : ";
				foreach($contentHeader as $v){ $tmplog.= $v.","; }
				$tmplog = substr($tmplog, 0, -1);
				$this->logdata->write(__FUNCTION__, $tmplog, $GLOBALS["logproces"]);
				$arrEmail = array(
					"from" => $from_name." <".$host[1].">",
					"to" => $sendto,
					"subject" => $from_subject,
					"text" => "",
					"html" => $from_body,
					"intermediateReport" => "true",
					"notifyUrl" => "http://103.28.57.88:10010",
					"notifyContentType" => "application/json",
					"track" => "true",
					"trackingUrl" => "http://103.28.57.88:10010/tracking.php",
					"bulkId" => 'cusotmBulkId'
				);
				$contentBody = $this->parsVal->getBody($arrEmail);
				$this->logdata->write(__FUNCTION__, $from_name." <".$host[1].">", $GLOBALS["logproces"]);
				$hitSendEmail = $this->sendProvider->sendEmail($contentHeader, $contentBody, $smtp_host);
				$tmplog = "Response : ".$hitSendEmail;
				$this->logdata->write(__FUNCTION__, $tmplog, $GLOBALS["logproces"]);
				if(!empty($hitSendEmail)){
					$resp = $this->getVal->getEmailResponse($hitSendEmail);
					$sts = $resp["sts"];
					$prvd= $resp["msgid"];
				}
			}
		}
		$timeRecvOpr= date("Y-m-d H:i:s");
		// end send email
		//log send data
		$tmplog		= "sendto :".$sendto.", Status Send :".$sts." prvd :".$prvd;
		$this->logdata->write(__FUNCTION__, $tmplog, $GLOBALS["logproces"]);
		$tmplog		= "sendto :".$sendto.", Time Send Start :".$timeToOpr;
		$this->logdata->write(__FUNCTION__, $tmplog, $GLOBALS["logproces"]);
		$tmplog		= "sendto :".$sendto.", Time Send End :".$timeRecvOpr;
		$this->logdata->write(__FUNCTION__, $tmplog, $GLOBALS["logproces"]);
		
		$nmClient	= $arrData['client_prefix_dbname'];
		$prefix		= $arrData['operator_prefix_db'];
		$iThn		= substr($arrData['time_dbrecv'],0,4);
		$iBln		= substr($arrData['time_dbrecv'],5,2);
		$tgl		= array("thn"=>$iThn,"bln"=>$iBln);
		$reff_expld	= explode("_", $arrData['ref_id']);
		
		// insert smssend
		$iSmsSend	= array(
			"code_sms"=>$arrData['code_sms'],"prvd_status_send"=>$sts,"prvd_message_id"=>$prvd,"f7_tid"=>$f7_tid,
			"status_send"=>$sts,"time_send"=>$timeToOpr,"operator_prefix_db"=>$prefix,"id-p-s-cfg"=>$arrData["id_p_s_cfg"],
			"client_prefix_dbname"=>$nmClient,"client_code_serv"=>$arrData['client_code_serv'],"engine_name_recv"=>$arrData['engine_name_recv'],
			"ref_id"=>$reff_expld[0],"operator_prefix_db"=>$arrData['operator_prefix_db'],"time_sent"=>$timeRecvOpr
		);
		$tblSmsSend		= array("thn"=>$tgl["thn"],"bln"=>$tgl["bln"],"srvc"=>$arrData['engine_name_recv'],"prefix"=>"email","nmtbl"=>"smssend");
		$nmTblSmsSend	= $this->getVal->getNameTable($tblSmsSend);
		$insSmsSend		= array(
			"arrdata"	=> $iSmsSend, "tablename"	=> $nmTblSmsSend,
			"dbname"	=> $nmClient, "confdb"	=> $GLOBALS["DB_REPORT_CONFIG"]
		);
		$this->setData($insSmsSend);
		$this->insertData();
		// end insert smssend
		
		// insert history
		$tblHistory		= array("thn"=>$tgl["thn"],"bln"=>$tgl["bln"],"srvc"=>$arrData['engine_name_recv'],"prefix"=>"email","nmtbl"=>"sendingsms");
		$nmTblHistory	= $this->getVal->getNameTable($tblHistory);
		$full_iod		= "sendto:".$sendto.":;:subject".$from_subject.":;:smtp_user".$smtp_user;
		$iHistory		= array(
			"id-p-s-cfg"=>$arrData['id_p_s_cfg'],"prvd_status_send"=>$sts,"prvd_message_id"=>$prvd,
			"iod_path"=>$full_iod,"code_sms"=>$arrData['code_sms'],"ref_id"=>$reff_expld[0],"time_log"=>$timeToOpr,"f7_tid"=>$f7_tid
		);
		$insHistory	= array(
			"arrdata"	=> $iHistory,
			"tablename"	=> $nmTblHistory,
			"dbname"	=> $nmClient."_history",
			"confdb"	=> $GLOBALS["DB_REPORT_CONFIG"]
		);
		$this->setData($insHistory);
		$this->insertData();
		// end insert history
		$dtParam = preg_replace_callback('!s:(\d+):"(.*?)";!', function($m) { return 's:'.strlen($m[2]).':"'.$m[2].'";'; }, $arrData["sendingParam"]);
		$sendParam	= unserialize($dtParam);
		$arrStsDlvr = array();
		if ($sts == "1" ){ // email send to operator
			// insert qdelivery
			$texpired = 1440;
			if(isset($sendParam["expire_minute"])){
				if($sendParam["expire_minute"] != "" || $sendParam["expire_minute"] != 0){
					$texpired = intval($sendParam["expire_minute"]);
				}
			}
			$addexpired = "+".$texpired." minute";
			$time_expired = date('Y-m-d H:i:s',date(strtotime($addexpired, strtotime($timeToOpr))));
			$iQdlvr	= array(
				"id-c"=>$arrData['id_c'],"tahun"=>$tgl["thn"],"bulan"=>$tgl["bln"],"model_service"=>"http",
				"time_send"=>$timeToOpr,"time_expire"=>$time_expired,"prvd_message_id"=>$prvd,"send_port_model"=>"http",
				"code_sms"=>$arrData['code_sms'],"time_delivery"=>$timeToOpr,"type_delivery"=>"1","f7_tid"=>$f7_tid,"ref_id"=>$reff_expld[0],
				"sendingParam"=>$arrData['sendingParam'],"id-p-s-cfg"=>$arrData["id_p_s_cfg"],"operator_prefix_db"=>$prefix,
				"client_prefix_dbname"=>$nmClient,"client_code_serv"=>$arrData['client_code_serv'],"engine_name_recv"=>$arrData['engine_name_recv'],
				"url_client_delivery_report"=>$arrData['url_callback']
			);
			$tblDlvr	= array("thn"=>$tgl["thn"],"bln"=>$tgl["bln"],"srvc"=>$arrData['engine_name_recv'],"prefix"=>$prefix,"nmtbl"=>"delivery");
			$nmTblDlvr	= $this->getVal->getNameTable($tblDlvr);
			$inQdlvr	= array(
				"arrdata"	=> $iQdlvr,
				"tablename"	=> $nmTblDlvr,
				"dbname"	=> $GLOBALS["DB_CONFIG_TRX"],
				"confdb"	=> $GLOBALS["DB_TRX_CONFIG"]
			);
			$this->setData($inQdlvr);
			$this->insertData();
			// end insert qdelivery
		}else{
			// insert smsdelivery
			$arrSmsDlvr=array(
				"time_delivery"=>$timeToOpr,
				"prvd_status_delivery"=>"SPRINT UNSENT",
				"prvd_status_others"=>"",
				"prvd_message_id_real"=>"",
				"prvd_status_delivery_real"=>"",
				"prvd_status_delivery_err_real"=>"",
				"status_delivery"=>"5",
				"code_sms"=>$arrData['code_sms'],
				"f7_tid"=>$f7_tid,
				"ref_id"=>$reff_expld[0],
				"prvd_message_id"=>$prvd,
				"ip_remote_addr"=>"",
				"ip_remote_host"=>"",
				"operator_prefix_db"=>$prefix,
				"client_prefix_dbname"=>$nmClient,
				"client_code_serv"=>$arrData['client_code_serv'],
				"engine_name_recv"=>$arrData['engine_name_recv']
			);
			$tblSmsDlvr		= array("thn"=>$tgl["thn"],"bln"=>$tgl["bln"],"srvc"=>$arrData['engine_name_recv'],"prefix"=>$prefix,"nmtbl"=>"smsdelivery");
			$nmTblSmsDlvr	= $this->getVal->getNameTable($tblSmsDlvr);
			$inSmsDlvr		= array(
				"arrdata"	=> $arrSmsDlvr,
				"tablename"	=> $nmTblSmsDlvr,
				"dbname"	=> $nmClient,
				"confdb"	=> $GLOBALS["DB_REPORT_CONFIG"]
			);
			$arrStsDlvr		= array(
				"ref_id" => $arrData['ref_id'],
				"code_sms" => $arrData['code_sms'],
				"type_status" => "dlvr",
				"status" => "5",
				"channel" => $arrData["channel"],
				"time_dlvr" => strtotime($timeRecvOpr)
			);
			$this->setData($inSmsDlvr);
			$this->insertData();
			// end insert smsdelivery
		}
		//setcallback
		$urlCallBack	= $arrData['url_callback'];
		// insert MQ Status Sent To Client
		$arrStsSent = array(
			"ref_id" => $arrData['ref_id'],
			"code_sms" => $arrData['code_sms'],
			"type_status" => "send",
			"status" => $sts,
			"channel" => $arrData["channel"],
			"time_send" => strtotime($timeToOpr),
			"time_prov_recv"=> strtotime($timeRecvOpr)
		);
		$router = "x.stssend";
		$severity = $router.".".$nmClient;
		$dtSent = json_encode($arrStsSent);
		$arrSent = array("url"=>$urlCallBack,"dtSts"=>$dtSent);
		$jsondata = $this->parsVal->CreateJson($arrSent);
		$arrInMq = array(
			"router"	=> $router,
			"severity"	=> $severity,
			"jsonmq"	=> $jsondata,
			"confmq"	=> $GLOBALS["CONF_MQ_SMS"]
		);
		$this->setData($arrInMq);
		$this->insertMq();
		// end insert MQ Status Sent To Client
		
		if(count($arrStsDlvr) > 0){
			$router		= "x.stsclient";
			$severity	= $router.".".$nmClient;
			$dtDlvr		= json_encode($arrStsDlvr);
			$arrDlvr	= array("url"=>$urlCallBack,"dtSts"=>$dtDlvr);
			$jsondata	= $this->parsVal->CreateJson($arrDlvr);
			$arrInMq	= array(
				"router"	=> $router,
				"severity"	=> $severity,
				"jsonmq"	=> $jsondata,
				"confmq"	=> $GLOBALS["CONF_MQ_SMS"]
			);
			$this->setData($arrInMq);
			$this->insertMq();
		}
	}// end sendToEmail
	
	public function sendToTelegram () {
		$arrData= $this->getDataSend();
		$message= $arrData["msg"];
		$token	= $arrData["sender"];
		$media	= $arrData["media"];
		$chatid	= $arrData["chatid"];
		$jenis	= $arrData["jenis"];
		$location	= $arrData["location"];
		// 1 success
		$timeToOpr	= date("Y-m-d H:i:s");
		// send telegram
		if($jenis == "location"){
			$getParam	= $this->sendTelegram->exePush($message, $token, $media, $chatid, "message", $location);
			$sendMsg	= $this->sendProvider->httpTelegram($getParam);
			
			$getParam	= $this->sendTelegram->exePush($message, $token, $media, $chatid, $jenis, $location);
			$sendLoc	= $this->sendProvider->httpTelegram($getParam);
		}else{
			$getParam	= $this->sendTelegram->exePush($message, $token, $media, $chatid, $jenis, $location);
			$send		= $this->sendProvider->httpTelegram($getParam);
		}
		$timeRecvOpr= date("Y-m-d H:i:s");
		// end send telegram
		$timeprvd	= str_replace(":", "", str_replace("-", "", $timeRecvOpr));
		$prvd		= $arrData['chatid'].str_replace(" ", "", $timeprvd);
		$sts		= "0";
		$stsDlvr	= "2";
		if($send["response"]["ok"] == "1"){ $sts = "1"; $stsDlvr = "1"; }
		//log send data
		$tmplog		= "chatid :".$arrData['chatid'].", Status Send :".$sts." prvd :".$prvd;
		$this->logdata->write(__FUNCTION__, $tmplog, $GLOBALS["logproces"]);
		$tmplog		= "chatid :".$arrData['chatid'].", Time Send Start :".$timeToOpr;
		$this->logdata->write(__FUNCTION__, $tmplog, $GLOBALS["logproces"]);
		$tmplog		= "chatid :".$arrData['chatid'].", Time Send End :".$timeRecvOpr;
		$this->logdata->write(__FUNCTION__, $tmplog, $GLOBALS["logproces"]);
		
		$nmClient	= $arrData['client_prefix_dbname'];
		$prefix		= $arrData['operator_prefix_db'];
		$iThn		= substr($arrData['time_dbrecv'],0,4);
		$iBln		= substr($arrData['time_dbrecv'],5,2);
		$tgl		= array("thn"=>$iThn,"bln"=>$iBln);
		$reff_expld	= explode("_", $arrData['ref_id']);
		
		// insert smssend
		$iSmsSend	= array(
			"code_sms"=>$arrData['code_sms'],"prvd_status_send"=>$sts,"prvd_message_id"=>$prvd,"f7_tid"=>$arrData['f7_tid'],
			"status_send"=>$sts,"time_send"=>$timeToOpr,"operator_prefix_db"=>$prefix,"id-p-s-cfg"=>$arrData["id_p_s_cfg"],
			"client_prefix_dbname"=>$nmClient,"client_code_serv"=>$arrData['client_code_serv'],"engine_name_recv"=>$arrData['engine_name_recv'],
			"ref_id"=>$reff_expld[0],"operator_prefix_db"=>$arrData['operator_prefix_db'],"time_sent"=>$timeRecvOpr
		);
		$tblSmsSend		= array("thn"=>$tgl["thn"],"bln"=>$tgl["bln"],"srvc"=>$arrData['engine_name_recv'],"prefix"=>"socmed","nmtbl"=>"smssend");
		$nmTblSmsSend	= $this->getVal->getNameTable($tblSmsSend);
		$insSmsSend		= array(
							"arrdata"	=> $iSmsSend,
							"tablename"	=> $nmTblSmsSend,
							"dbname"	=> $nmClient,
							"confdb"	=> $GLOBALS["DB_REPORT_CONFIG"]
						);
		$this->setData($insSmsSend);
		$this->insertData();
		// end insert smssend
		
		// insert history
		$tblHistory		= array("thn"=>$tgl["thn"],"bln"=>$tgl["bln"],"srvc"=>$arrData['engine_name_recv'],"prefix"=>"socmed","nmtbl"=>"sendingsms");
		$nmTblHistory	= $this->getVal->getNameTable($tblHistory);
		$full_iod		= "message:".$message.":;:token:".$token.":;:media".$media.":;:chatid".$chatid.":;:jenis".$jenis.":;:location".$location;
		$iHistory		= array(
			"id-p-s-cfg"=>$arrData['id_p_s_cfg'],"prvd_status_send"=>$sts,"prvd_message_id"=>$prvd,
			"iod_path"=>$full_iod,"code_sms"=>$arrData['code_sms'],"ref_id"=>$reff_expld[0],"time_log"=>$timeToOpr,"f7_tid"=>$arrData['f7_tid']
		);
		$insHistory	= array(
						"arrdata"	=> $iHistory,
						"tablename"	=> $nmTblHistory,
						"dbname"	=> $nmClient."_history",
						"confdb"	=> $GLOBALS["DB_REPORT_CONFIG"]
					);
		$this->setData($insHistory);
		$this->insertData();
		// end insert history
		
		// insert smsdelivery
		$tblSmsDlvr		= array("thn"=>$tgl["thn"],"bln"=>$tgl["bln"],"srvc"=>$arrData['engine_name_recv'],"prefix"=>"socmed","nmtbl"=>"smsdelivery");
		$nmTblSmsDlvr	= $this->getVal->getNameTable($tblSmsDlvr);
		$arrSmsDlvr=array(
			"time_delivery"=>$timeRecvOpr,
			"ref_id"=>$reff_expld[0],
			"prvd_status_delivery"=>$prvd,
			"prvd_status_others"=>"",
			"prvd_message_id_real"=>"",
			"prvd_status_delivery_real"=>"",
			"prvd_status_delivery_err_real"=>"",
			"status_delivery"=>$stsDlvr,
			"code_sms"=>$arrData['code_sms'],
			"f7_tid"=>$arrData['f7_tid'],
			"ref_id"=>$reff_expld[0],
			"prvd_message_id"=>$prvd,
			"ip_remote_addr"=>"",
			"ip_remote_host"=>"",
			"client_prefix_dbname"=>$nmClient,
			"client_code_serv"=>$arrData['client_code_serv'],
			"engine_name_recv"=>$arrData['engine_name_recv'],
			"operator_prefix_db"=>$arrData['operator_prefix_db']
		);
		$inSmsDlvr	= array(
			"arrdata"	=> $arrSmsDlvr,
			"tablename"	=> $nmTblSmsDlvr,
			"dbname"	=> $nmClient,
			"confdb"	=> $GLOBALS["DB_REPORT_CONFIG"]
		);
		$this->setData($inSmsDlvr);
		$this->insertData();
		// end insert smsdelivery
		
		//setcallback
		$urlCallBack	= $arrData['url_callback'];
		// insert MQ Status Sent To Client
		$arrStsSent = array(
			"ref_id" => $arrData['ref_id'],
			"code_sms" => $arrData['code_sms'],
			"type_status" => "send",
			"status" => $sts,
			"channel" => $arrData["channel"],
			"time_send" => strtotime($timeToOpr),
			"time_prov_recv"=> strtotime($timeRecvOpr)
		);
		$router = "x.stssend";
		$severity = $router.".".$nmClient;
		$dtSent = json_encode($arrStsSent);
		$arrSent = array("url"=>$urlCallBack,"dtSts"=>$dtSent);
		$jsondata = $this->parsVal->CreateJson($arrSent);
		$arrInMq = array(
			"router"	=> $router,
			"severity"	=> $severity,
			"jsonmq"	=> $jsondata,
			"confmq"	=> $GLOBALS["CONF_MQ_SMS"]
		);
		$this->setData($arrInMq);
		$this->insertMq();
		// end insert MQ Status Sent To Client
		
		// insert MQ Status Delivery To Client
		$arrStsDlvr		= array(
				"ref_id" => $arrData['ref_id'],
				"code_sms" => $arrData['code_sms'],
				"type_status" => "dlvr",
				"status" => $stsDlvr,
				"channel" => $arrData["channel"],
				"time_dlvr" => strtotime($timeRecvOpr)
			);
		$router		= "x.stsclient";
		$severity	= $router.".".$nmClient;
		$dtDlvr		= json_encode($arrStsDlvr);
		$arrDlvr	= array("url"=>$urlCallBack,"dtSts"=>$dtDlvr);
		$jsondata	= $this->parsVal->CreateJson($arrDlvr);
		$arrInMq	= array(
						"router"	=> $router,
						"severity"	=> $severity,
						"jsonmq"	=> $jsondata,
						"confmq"	=> $GLOBALS["CONF_MQ_SMS"]
					);
		$this->setData($arrInMq);
		$this->insertMq();
	}// end sendToTelegram
	
	public function sendToLine () {
		$arrData= $this->getDataSend();
		$token	= $arrData["sender"];
		$message= $arrData["msg"];
		$media	= $arrData["media"];
		$sendto	= $arrData["secret_token"];
		$chatid	= $arrData["chatid"];
		$jenis	= $arrData["jenis"];
		$location	= $arrData["location"];
		$sts	= "1";
		$stsDlvr	= "1";
		// send line
		$timeToOpr	= date("Y-m-d H:i:s");
		if($jenis == "phototext"){
			$getParam	= $this->sendLine->exePush($message, $token, $sendto, $media, $chatid, "photo", $location);
			$sendPhoto	= $this->sendProvider->httpLine($getParam);
			$getParam	= $this->sendLine->exePush($message, $token, $sendto, $media, $chatid, $jenis, $location);
			$send	= $this->sendProvider->httpLine($getParam);
		}else{
			$getParam	= $this->sendLine->exePush($message, $token, $sendto, $media, $chatid, $jenis, $location);
			$send		= $this->sendProvider->httpLine($getParam);
		}
		$timeRecvOpr= date("Y-m-d H:i:s");
		$sendRS		= json_decode($send["response"], true);
		$cRS		= count($sendRS);
		// send line
		$timeprvd	= str_replace(":", "", str_replace("-", "", $timeRecvOpr));
		$prvd		= $arrData['chatid'].str_replace(" ", "", $timeprvd);
		if($cRS > 0){ $sts	= "0"; $stsDlvr= "2"; }
		
		//log send data
		$tmplog		= "chatid :".$arrData['chatid'].", Status Send :".$sts." prvd :".$prvd;
		$this->logdata->write(__FUNCTION__, $tmplog, $GLOBALS["logproces"]);
		$tmplog		= "chatid :".$arrData['chatid'].", Time Send Start :".$timeToOpr;
		$this->logdata->write(__FUNCTION__, $tmplog, $GLOBALS["logproces"]);
		$tmplog		= "chatid :".$arrData['chatid'].", Time Send End :".$timeRecvOpr;
		$this->logdata->write(__FUNCTION__, $tmplog, $GLOBALS["logproces"]);
		
		$nmClient	= $arrData['client_prefix_dbname'];
		$prefix		= $arrData['operator_prefix_db'];
		$iThn		= substr($arrData['time_dbrecv'],0,4);
		$iBln		= substr($arrData['time_dbrecv'],5,2);
		$tgl		= array("thn"=>$iThn,"bln"=>$iBln);
		$reff_expld	= explode("_", $arrData['ref_id']);
		
		// insert smssend
		$iSmsSend	= array(
			"code_sms"=>$arrData['code_sms'],"prvd_status_send"=>$sts,"prvd_message_id"=>$prvd,"f7_tid"=>$arrData['f7_tid'],
			"status_send"=>$sts,"time_send"=>$timeToOpr,"operator_prefix_db"=>$prefix,"id-p-s-cfg"=>$arrData["id_p_s_cfg"],
			"client_prefix_dbname"=>$nmClient,"client_code_serv"=>$arrData['client_code_serv'],"engine_name_recv"=>$arrData['engine_name_recv'],
			"ref_id"=>$reff_expld[0],"operator_prefix_db"=>$arrData['operator_prefix_db'],"time_sent"=>$timeRecvOpr
		);
		$tblSmsSend		= array("thn"=>$tgl["thn"],"bln"=>$tgl["bln"],"srvc"=>$arrData['engine_name_recv'],"prefix"=>"socmed","nmtbl"=>"smssend");
		$nmTblSmsSend	= $this->getVal->getNameTable($tblSmsSend);
		$insSmsSend		= array(
							"arrdata"	=> $iSmsSend,
							"tablename"	=> $nmTblSmsSend,
							"dbname"	=> $nmClient,
							"confdb"	=> $GLOBALS["DB_REPORT_CONFIG"]
						);
		$this->setData($insSmsSend);
		$this->insertData();
		// end insert smssend
		
		// insert history
		$tblHistory		= array("thn"=>$tgl["thn"],"bln"=>$tgl["bln"],"srvc"=>$arrData['engine_name_recv'],"prefix"=>"socmed","nmtbl"=>"sendingsms");
		$nmTblHistory	= $this->getVal->getNameTable($tblHistory);
		$full_iod		= "message:".$message.":;:token:".$token.":;:media".$media.":;:chatid".$chatid.":;:jenis".$jenis.":;:location".$location;
		$iHistory		= array(
			"id-p-s-cfg"=>$arrData['id_p_s_cfg'],"prvd_status_send"=>$sts,"prvd_message_id"=>$prvd,
			"iod_path"=>$full_iod,"code_sms"=>$arrData['code_sms'],"ref_id"=>$reff_expld[0],"time_log"=>$timeToOpr,"f7_tid"=>$arrData['f7_tid']
		);
		$insHistory	= array(
						"arrdata"	=> $iHistory,
						"tablename"	=> $nmTblHistory,
						"dbname"	=> $nmClient."_history",
						"confdb"	=> $GLOBALS["DB_REPORT_CONFIG"]
					);
		$this->setData($insHistory);
		$this->insertData();
		// end insert history
		
		// insert smsdelivery
		$tblSmsDlvr		= array("thn"=>$tgl["thn"],"bln"=>$tgl["bln"],"srvc"=>$arrData['engine_name_recv'],"prefix"=>"socmed","nmtbl"=>"smsdelivery");
		$nmTblSmsDlvr	= $this->getVal->getNameTable($tblSmsDlvr);
		$arrSmsDlvr=array(
			"time_delivery"=>$timeRecvOpr,
			"ref_id"=>$reff_expld[0],
			"prvd_status_delivery"=>$prvd,
			"prvd_status_others"=>"",
			"prvd_message_id_real"=>"",
			"prvd_status_delivery_real"=>"",
			"prvd_status_delivery_err_real"=>"",
			"status_delivery"=>$stsDlvr,
			"code_sms"=>$arrData['code_sms'],
			"f7_tid"=>$arrData['f7_tid'],
			"ref_id"=>$reff_expld[0],
			"prvd_message_id"=>$prvd,
			"ip_remote_addr"=>"",
			"ip_remote_host"=>"",
			"client_prefix_dbname"=>$nmClient,
			"client_code_serv"=>$arrData['client_code_serv'],
			"engine_name_recv"=>$arrData['engine_name_recv'],
			"operator_prefix_db"=>$arrData['operator_prefix_db']
		);
		$inSmsDlvr	= array(
			"arrdata"	=> $arrSmsDlvr,
			"tablename"	=> $nmTblSmsDlvr,
			"dbname"	=> $nmClient,
			"confdb"	=> $GLOBALS["DB_REPORT_CONFIG"]
		);
		$this->setData($inSmsDlvr);
		$this->insertData();
		// end insert smsdelivery
		
		//setcallback
		$urlCallBack	= $arrData['url_callback'];
		$arrCallBack	= array(
			 "ref_id"	=> $arrData['ref_id'],
			 "code_sms"	=> $arrData['code_sms'],
			 "type_status" => "send",
			 "status"	=> $sts,
			 "channel"	=> $arrData["channel"],
			 "time_send"=> strtotime($timeToOpr),
			 "time_prov_recv"=> strtotime($timeRecvOpr)
		);
		$dtCallBack		= json_encode($arrCallBack);
		$hitCallBack	= $this->sendProvider->sendBackData($dtCallBack, $urlCallBack);
		$tmplog		= "ref_id :".$arrData['ref_id'].", Callback : url ".$urlCallBack." ".$dtCallBack."\n";
		$this->logdata->write(__FUNCTION__, $tmplog, $GLOBALS["logproces"]);
		
		// insert MQ Status Delivery To Client
		$arrStsDlvr		= array(
				"ref_id" => $arrData['ref_id'],
				"code_sms" => $arrData['code_sms'],
				"type_status" => "dlvr",
				"status" => $stsDlvr,
				"channel" => $arrData["channel"],
				"time_dlvr" => strtotime($timeRecvOpr)
			);
		$router		= "x.stsclient";
		$severity	= $router.".".$nmClient;
		$dtDlvr		= json_encode($arrStsDlvr);
		$arrDlvr	= array("url"=>$urlCallBack,"dtStsDlvr"=>$dtDlvr);
		$jsondata	= $this->parsVal->CreateJson($arrDlvr);
		$arrInMq	= array(
						"router"	=> $router,
						"severity"	=> $severity,
						"jsonmq"	=> $jsondata,
						"confmq"	=> $GLOBALS["CONF_MQ_SMS"]
					);
		$this->setData($arrInMq);
		$this->insertMq();
	}// end sendToLine
	
	public function sendToFacebook () {
		$arrData = $this->getDataSend();
		$message = $arrData["msg"];
		$token = $arrData["sender"];
		$media = $arrData["media"];
		$chatid = $arrData["chatid"];
		$jenis = $arrData["jenis"];
		$location = $arrData["location"];
		
		// send facebook
		$timeToOpr	= date("Y-m-d H:i:s");
		if($jenis == "photo" || $jenis == "video"){
			$getParam	= $this->sendFb->exePush($message, $token, $media, $chatid, "photoOne", $location);
			$sendOne	= $this->sendProvider->httpFacebook($getParam);
			$getParam	= $this->sendFb->exePush($message, $token, $media, $chatid, "photoOne", $location, $sendOne["response"]["attachment_id"]);
			$sendOne	= $this->sendProvider->httpFacebook($getParam);
		}else{
			$getParam	= $this->sendFb->exePush($message, $token, $media, $chatid, $jenis, $location);
			$send		= $this->sendProvider->httpFacebook($getParam);
		}
		$timeRecvOpr	= date("Y-m-d H:i:s");
		$cRS	= count($send);
		// send facebook
		$timeprvd	= str_replace(":", "", str_replace("-", "", $timeRecvOpr));
		$prvd		= $arrData['chatid'].str_replace(" ", "", $timeprvd);
		$sts		= "0";
		$stsDlvr	= "2";
		if($cRS > 0){ $sts = "1"; $stsDlvr = "1"; }
		
		//log send data
		$tmplog		= "chatid :".$arrData['chatid'].", Status Send :".$sts." prvd :".$prvd;
		$this->logdata->write(__FUNCTION__, $tmplog, $GLOBALS["logproces"]);
		$tmplog		= "chatid :".$arrData['chatid'].", Time Send Start :".$timeToOpr;
		$this->logdata->write(__FUNCTION__, $tmplog, $GLOBALS["logproces"]);
		$tmplog		= "chatid :".$arrData['chatid'].", Time Send End :".$timeRecvOpr;
		$this->logdata->write(__FUNCTION__, $tmplog, $GLOBALS["logproces"]);
		
		$nmClient	= $arrData['client_prefix_dbname'];
		$prefix		= $arrData['operator_prefix_db'];
		$iThn		= substr($arrData['time_dbrecv'],0,4);
		$iBln		= substr($arrData['time_dbrecv'],5,2);
		$tgl		= array("thn"=>$iThn,"bln"=>$iBln);
		$reff_expld	= explode("_", $arrData['ref_id']);
		
		// insert smssend
		$iSmsSend	= array(
			"code_sms"=>$arrData['code_sms'],"prvd_status_send"=>$sts,"prvd_message_id"=>$prvd,"f7_tid"=>$arrData['f7_tid'],
			"status_send"=>$sts,"time_send"=>$timeToOpr,"operator_prefix_db"=>$prefix,"id-p-s-cfg"=>$arrData["id_p_s_cfg"],
			"client_prefix_dbname"=>$nmClient,"client_code_serv"=>$arrData['client_code_serv'],"engine_name_recv"=>$arrData['engine_name_recv'],
			"ref_id"=>$reff_expld[0],"operator_prefix_db"=>$arrData['operator_prefix_db'],"time_sent"=>$timeRecvOpr
		);
		$tblSmsSend		= array("thn"=>$tgl["thn"],"bln"=>$tgl["bln"],"srvc"=>$arrData['engine_name_recv'],"prefix"=>"socmed","nmtbl"=>"smssend");
		$nmTblSmsSend	= $this->getVal->getNameTable($tblSmsSend);
		$insSmsSend		= array(
							"arrdata"	=> $iSmsSend,
							"tablename"	=> $nmTblSmsSend,
							"dbname"	=> $nmClient,
							"confdb"	=> $GLOBALS["DB_REPORT_CONFIG"]
						);
		$this->setData($insSmsSend);
		$this->insertData();
		// end insert smssend
		
		// insert history
		$tblHistory		= array("thn"=>$tgl["thn"],"bln"=>$tgl["bln"],"srvc"=>$arrData['engine_name_recv'],"prefix"=>"socmed","nmtbl"=>"sendingsms");
		$nmTblHistory	= $this->getVal->getNameTable($tblHistory);
		$full_iod		= "message:".$message.":;:token:".$token.":;:media".$media.":;:chatid".$chatid.":;:jenis".$jenis.":;:location".$location;
		$iHistory		= array(
			"id-p-s-cfg"=>$arrData['id_p_s_cfg'],"prvd_status_send"=>$sts,"prvd_message_id"=>$prvd,
			"iod_path"=>$full_iod,"code_sms"=>$arrData['code_sms'],"ref_id"=>$reff_expld[0],"time_log"=>$timeToOpr,"f7_tid"=>$arrData['f7_tid']
		);
		$insHistory	= array(
						"arrdata"	=> $iHistory,
						"tablename"	=> $nmTblHistory,
						"dbname"	=> $nmClient."_history",
						"confdb"	=> $GLOBALS["DB_REPORT_CONFIG"]
					);
		$this->setData($insHistory);
		$this->insertData();
		// end insert history
		
		// insert smsdelivery
		$tblSmsDlvr		= array("thn"=>$tgl["thn"],"bln"=>$tgl["bln"],"srvc"=>$arrData['engine_name_recv'],"prefix"=>"socmed","nmtbl"=>"smsdelivery");
		$nmTblSmsDlvr	= $this->getVal->getNameTable($tblSmsDlvr);
		$arrSmsDlvr=array(
			"time_delivery"=>$timeRecvOpr,
			"ref_id"=>$reff_expld[0],
			"prvd_status_delivery"=>$prvd,
			"prvd_status_others"=>"",
			"prvd_message_id_real"=>"",
			"prvd_status_delivery_real"=>"",
			"prvd_status_delivery_err_real"=>"",
			"status_delivery"=>$stsDlvr,
			"code_sms"=>$arrData['code_sms'],
			"f7_tid"=>$arrData['f7_tid'],
			"ref_id"=>$reff_expld[0],
			"prvd_message_id"=>$prvd,
			"ip_remote_addr"=>"",
			"ip_remote_host"=>"",
			"client_prefix_dbname"=>$nmClient,
			"client_code_serv"=>$arrData['client_code_serv'],
			"engine_name_recv"=>$arrData['engine_name_recv'],
			"operator_prefix_db"=>$arrData['operator_prefix_db']
		);
		$inSmsDlvr	= array(
			"arrdata"	=> $arrSmsDlvr,
			"tablename"	=> $nmTblSmsDlvr,
			"dbname"	=> $nmClient,
			"confdb"	=> $GLOBALS["DB_REPORT_CONFIG"]
		);
		$this->setData($inSmsDlvr);
		$this->insertData();
		// end insert smsdelivery
		
		//setcallback
		$urlCallBack	= $arrData['url_callback'];
		// insert MQ Status Sent To Client
		$arrStsSent = array(
			"ref_id" => $arrData['ref_id'],
			"code_sms" => $arrData['code_sms'],
			"type_status" => "send",
			"status" => $sts,
			"channel" => $arrData["channel"],
			"time_send" => strtotime($timeToOpr),
			"time_prov_recv"=> strtotime($timeRecvOpr)
		);
		$router = "x.stssend";
		$severity = $router.".".$nmClient;
		$dtSent = json_encode($arrStsSent);
		$arrSent = array("url"=>$urlCallBack,"dtSts"=>$dtSent);
		$jsondata = $this->parsVal->CreateJson($arrSent);
		$arrInMq = array(
			"router"	=> $router,
			"severity"	=> $severity,
			"jsonmq"	=> $jsondata,
			"confmq"	=> $GLOBALS["CONF_MQ_SMS"]
		);
		$this->setData($arrInMq);
		$this->insertMq();
		// end insert MQ Status Sent To Client
		
		// insert MQ Status Delivery To Client
		$arrStsDlvr		= array(
				"ref_id" => $arrData['ref_id'],
				"code_sms" => $arrData['code_sms'],
				"type_status" => "dlvr",
				"status" => $stsDlvr,
				"channel" => $arrData["channel"],
				"time_dlvr" => strtotime($timeRecvOpr)
			);
		$router		= "x.stsclient";
		$severity	= $router.".".$nmClient;
		$dtDlvr		= json_encode($arrStsDlvr);
		$arrDlvr	= array("url"=>$urlCallBack,"dtStsDlvr"=>$dtDlvr);
		$jsondata	= $this->parsVal->CreateJson($arrDlvr);
		$arrInMq	= array(
						"router"	=> $router,
						"severity"	=> $severity,
						"jsonmq"	=> $jsondata,
						"confmq"	=> $GLOBALS["CONF_MQ_SMS"]
					);
		$this->setData($arrInMq);
		$this->insertMq();
	}// end sendToFacebook
	
	public function sendToWhatsapp () {
		$arrData = $this->getDataSend();
		$bearer = $arrData["sender"];
		$txtmsg = explode(";:;", $arrData["msg"]);
		//1 = reguler
		//2 = with header
		$msgwa = $txtmsg[1];
		$prms = array();
		if(strval($txtmsg[0]) == 1){
			$msgwa = $txtmsg[1];
		}else{
			$cmsg = count($txtmsg);
			for($i=2;$i<$cmsg;$i++){
				array_push($prms, $txtmsg[$i]);
			}
		}
		
		$params = $this->parsVal->getParamWA($prms);
		if ($arrData["chatid"]['0'] == "0"){
			$arrData["chatid"] = "62" . substr($arrData["chatid"],1,strlen($arrData["chatid"]));
		}
		$msisdn = "+".$arrData["chatid"];
		$checkWA = $this->sendProvider->verifyContact($bearer, array($msisdn));
		$this->logdata->write(__FUNCTION__, "Check WA : ".json_encode($checkWA), $GLOBALS["logproces"]);
		
		$waid = $checkWA["contacts"][0]["wa_id"];
		$stsid = $checkWA["contacts"][0]["status"];
		
		$timeToOpr	= date("Y-m-d H:i:s");
		$nmClient	= $arrData['client_prefix_dbname'];
		$prefix		= $arrData['operator_prefix_db'];
		$iThn		= substr($arrData['time_dbrecv'],0,4);
		$iBln		= substr($arrData['time_dbrecv'],5,2);
		$tgl		= array("thn"=>$iThn,"bln"=>$iBln);
		$reff_expld	= explode("_", $arrData['ref_id']);
		$timeRecvOpr= date("Y-m-d H:i:s");
		$timeprvd	= str_replace(":", "", str_replace("-", "", $timeRecvOpr));
		$prvd		= $arrData['chatid'].str_replace(" ", "", $timeprvd);
		$stsent = 1;
		if($stsid == "valid"){
			$dtParam		= preg_replace_callback('!s:(\d+):"(.*?)";!', function($m) { return 's:'.strlen($m[2]).':"'.$m[2].'";'; }, $arrData["sendingParam"]);
			$sendParam		= unserialize($dtParam);
			$expl_nmspace = explode(":;:", $sendParam["send_ip_no"]);
			$dtsend = array(
				"recipient_type" => "individual",
				"to" => $waid,
				"ttl" => "P1D",
				"type" => "hsm",
				"hsm" => array(
					"namespace" => strtolower($expl_nmspace[1]),
					"element_name" => $msgwa, // kode template
					"language" => array("policy" => "deterministic", 'code' => 'id'),
					"localizable_params" => $params
				)
			);
			$arrSend = array("bearer" => $bearer, "dtsend" => $dtsend);
			$this->logdata->write(__FUNCTION__, "Send WA : ".json_encode($arrSend), $GLOBALS["logproces"]);
			$send = $this->sendProvider->sendWhatsapp($arrSend);
			$this->logdata->write(__FUNCTION__, "Resp WA : ".json_encode($send), $GLOBALS["logproces"]);
			$timeRecvOpr= date("Y-m-d H:i:s");
			
			if(isset($send["error"]) == "invalid_client"){ $stsent = 0; }
			else{ $prvd = $send["messages"][0]["id"]; }
		}else{ $stsent = 0; }
		
		// insert smssend
			$iSmsSend	= array(
				"code_sms"=>$arrData['code_sms'],"prvd_status_send"=>$stsent,"prvd_message_id"=>$prvd,"f7_tid"=>$arrData['f7_tid'],
				"status_send"=>$stsent,"time_send"=>$timeToOpr,"operator_prefix_db"=>$prefix,"id-p-s-cfg"=>$arrData["id_p_s_cfg"],
				"client_prefix_dbname"=>$nmClient,"client_code_serv"=>$arrData['client_code_serv'],"engine_name_recv"=>$arrData['engine_name_recv'],
				"ref_id"=>$reff_expld[0],"operator_prefix_db"=>$arrData['operator_prefix_db'],"time_sent"=>$timeRecvOpr
			);
			$tblSmsSend		= array("thn"=>$tgl["thn"],"bln"=>$tgl["bln"],"srvc"=>$arrData['engine_name_recv'],"prefix"=>"socmed","nmtbl"=>"smssend");
			$nmTblSmsSend	= $this->getVal->getNameTable($tblSmsSend);
			$insSmsSend		= array(
				"arrdata"	=> $iSmsSend,
				"tablename"	=> $nmTblSmsSend,
				"dbname"	=> $nmClient,
				"confdb"	=> $GLOBALS["DB_REPORT_CONFIG"]
			);
			$this->setData($insSmsSend);
			$this->insertData();
			// end insert smssend
		$arrStsDlvr = array();
		if (strval($stsent == "1") ){ // whatsapp send to operator
			// insert qdelivery
			$texpired = 1440;
			if(isset($sendParam["expire_minute"])){
				if($sendParam["expire_minute"] != "" || $sendParam["expire_minute"] != 0){
					$texpired = intval($sendParam["expire_minute"]);
				}
			}
			$addexpired = "+".$texpired." minute";
			$time_expired = date('Y-m-d H:i:s',date(strtotime($addexpired, strtotime($timeToOpr))));
			$iQdlvr	= array(
				"id-c"=>$arrData['id_c'],"tahun"=>$tgl["thn"],"bulan"=>$tgl["bln"],"model_service"=>"http",
				"time_send"=>$timeToOpr,"time_expire"=>$time_expired,"prvd_message_id"=>$prvd,"send_port_model"=>"http",
				"code_sms"=>$arrData['code_sms'],"time_delivery"=>$timeToOpr,"type_delivery"=>"1","f7_tid"=>$arrData['f7_tid'],"ref_id"=>$reff_expld[0],
				"sendingParam"=>$arrData['sendingParam'],"id-p-s-cfg"=>$arrData["id_p_s_cfg"],"operator_prefix_db"=>$prefix,
				"client_prefix_dbname"=>$nmClient,"client_code_serv"=>$arrData['client_code_serv'],"engine_name_recv"=>$arrData['engine_name_recv'],
				"url_client_delivery_report"=>$arrData['url_callback']
			);
			$tblDlvr	= array("thn"=>$tgl["thn"],"bln"=>$tgl["bln"],"srvc"=>$arrData['engine_name_recv'],"prefix"=>"socmed","nmtbl"=>"delivery");
			$nmTblDlvr	= $this->getVal->getNameTable($tblDlvr);
			$inQdlvr	= array(
				"arrdata"	=> $iQdlvr,
				"tablename"	=> $nmTblDlvr,
				"dbname"	=> $GLOBALS["DB_CONFIG_TRX"],
				"confdb"	=> $GLOBALS["DB_TRX_CONFIG"]
			);
			$this->setData($inQdlvr);
			$this->insertData();
			// end insert qdelivery
		}else{
			// insert smsdelivery
			$arrSmsDlvr=array(
				"time_delivery"=>$timeToOpr,
				"prvd_status_delivery"=>"SPRINT UNSENT",
				"prvd_status_others"=>"",
				"prvd_message_id_real"=>"",
				"prvd_status_delivery_real"=>"",
				"prvd_status_delivery_err_real"=>"",
				"status_delivery"=>"5",
				"code_sms"=>$arrData['code_sms'],
				"f7_tid"=>$arrData['f7_tid'],
				"ref_id"=>$reff_expld[0],
				"prvd_message_id"=>$response['code'],
				"ip_remote_addr"=>"",
				"ip_remote_host"=>"",
				"operator_prefix_db"=>$arrData['operator_prefix_db'],
				"client_prefix_dbname"=>$nmClient,
				"client_code_serv"=>$arrData['client_code_serv'],
				"engine_name_recv"=>$arrData['engine_name_recv']
			);
			$tblSmsDlvr		= array("thn"=>$tgl["thn"],"bln"=>$tgl["bln"],"srvc"=>$arrData['engine_name_recv'],"prefix"=>"socmed","nmtbl"=>"smsdelivery");
			$nmTblSmsDlvr	= $this->getVal->getNameTable($tblSmsDlvr);
			$inSmsDlvr	= array(
				"arrdata"	=> $arrSmsDlvr,
				"tablename"	=> $nmTblSmsDlvr,
				"dbname"	=> $nmClient,
				"confdb"	=> $GLOBALS["DB_REPORT_CONFIG"]
			);
			$this->setData($inSmsDlvr);
			$this->insertData();
			$arrStsDlvr		= array(
				"ref_id" => $arrData['ref_id'],
				"code_sms" => $arrData['code_sms'],
				"type_status" => "dlvr",
				"status" => "5",
				"channel" => $arrData["channel"],
				"time_dlvr" => strtotime($timeRecvOpr)
			);
			// end insert smsdelivery
		}
		// insert history
		$tblHistory		= array("thn"=>$tgl["thn"],"bln"=>$tgl["bln"],"srvc"=>$arrData['engine_name_recv'],"prefix"=>"socmed","nmtbl"=>"sendingsms");
		$nmTblHistory	= $this->getVal->getNameTable($tblHistory);
		$iHistory		= array(
			"id-p-s-cfg"=>$arrData['id_p_s_cfg'],"prvd_status_send"=>$stsent,"prvd_message_id"=>$prvd,
			"iod_path"=>json_encode($arrSend),"code_sms"=>$arrData['code_sms'],"ref_id"=>$reff_expld[0],"time_log"=>$timeToOpr,"f7_tid"=>$arrData['f7_tid']
		);
		$insHistory	= array(
			"arrdata"	=> $iHistory,
			"tablename"	=> $nmTblHistory,
			"dbname"	=> $nmClient."_history",
			"confdb"	=> $GLOBALS["DB_REPORT_CONFIG"]
		);
		$this->setData($insHistory);
		$this->insertData();
		// end insert history
		
		//setcallback
		$urlCallBack	= str_replace("\\", "", $arrData['url_callback']);
		// insert MQ Status Sent To Client
		$arrStsSent = array(
			"ref_id" => $arrData['ref_id'],
			"code_sms" => $arrData['code_sms'],
			"type_status" => "send",
			"status" => $stsent,
			"channel" => $arrData["channel"],
			"time_send" => strtotime($timeToOpr),
			"time_prov_recv"=> strtotime($timeRecvOpr)
		);
		$router = "x.stssend";
		$severity = $router.".".$nmClient;
		$dtSent = json_encode($arrStsSent);
		$arrSent = array("url"=>$urlCallBack,"dtSts"=>$dtSent);
		$jsondata = $this->parsVal->CreateJson($arrSent);
		$arrInMq = array(
			"router"	=> $router,
			"severity"	=> $severity,
			"jsonmq"	=> $jsondata,
			"confmq"	=> $GLOBALS["CONF_MQ_SMS"]
		);
		$this->setData($arrInMq);
		$this->insertMq();
		// end insert MQ Status Sent To Client
		// insert MQ Status Dlvr To Client
		if(count($arrStsDlvr) > 0){
			$router		= "x.stsclient";
			$severity	= $router.".".$nmClient;
			$dtDlvr		= json_encode($arrStsDlvr);
			$arrDlvr	= array("url"=>$urlCallBack,"dtSts"=>$dtDlvr);
			$jsondata	= $this->parsVal->CreateJson($arrDlvr);
			$arrInMq	= array(
				"router"	=> $router,
				"severity"	=> $severity,
				"jsonmq"	=> $jsondata,
				"confmq"	=> $GLOBALS["CONF_MQ_SMS"]
			);
			$this->setData($arrInMq);
			$this->insertMq();
		}
		// end insert MQ Status Dlvr To Client
	}// end sendToWhatsapp
}// end class SendFunc
?>
