<?php

namespace App\Utils;

class FuncTelegram
{
	var $message;
	var $token;
	var $media;
	var $chatid;
	var $jenissend;
	var $longitude;
	var $latitude;
	var $qurlocation;
	var $address;
	var $data;
	var $perintah;
	var $urlpush = 'https://api.telegram.org/bot';
	
	public function exePush($msg, $tkn, $mda, $cid, $jns, $qlc)
	{
		$this->message		= $msg;
		$this->token		= $tkn;
		$this->media		= $mda;
		$this->chatid		= $cid;
		$this->jenissend	= $jns;
		$this->qurlocation	= $qlc;
		
		$this->data = array('chat_id' => $this->chatid);
		
		switch($this->jenissend)
		{
			case "message":
				$this->data["text"]			= $this->message;
				$this->data["parse_mode"]	= "HTML";
				$this->data["reply_markup"]	= NULL;
				
				$this->perintah				= "sendMessage";
			break;
			
			case "photo":
				$this->data["photo"]		= $this->media;
				$this->data["caption"]		= $this->message;
				$this->data["reply_markup"]	= NULL;
				
				$this->perintah				= "sendPhoto";
			break;
			
			case "video":
				$this->data["video"]		= $this->media;
				$this->data["caption"]		= $this->message;
				$this->data["reply_markup"]	= NULL;
				
				$this->perintah				= "sendVideo";
			break;
			
			case "document":
				$this->data["document"]		= $this->media;
				$this->data["caption"]		= $this->message;
				$this->data["reply_markup"]	= NULL;
				
				$this->perintah				= "sendDocument";
			break;
			
			case "location":
				$location				= $this->getLocation($this->qurlocation);
				$this->data["latitude"]	= $this->getLatitude();
				$this->data["longitude"]= $this->getLongitude();
				
				$this->perintah			= "sendLocation";
			break;
			
			default:
				$this->data["chat_id"]	= "Request Empty!";
		}// end switch
		
		$rs = array("paramData" => $this->data, "paramCommand" => $this->perintah, "paramToken" => $this->token);
		
		return $rs;
	}// end exePush
	
	public function getLocation($dataLoc) {
		$key 		= "AIzaSyDKDPrIj3_dyFf9_Es5JUK4Kw_P0hEnBk8";
		$queryclean = str_replace (" ", "+", $dataLoc);
		
		/* URL QUERY LOCATION */
		$url		= "https://maps.googleapis.com/maps/api/place/textsearch/json?query=".$queryclean."&key=".$key;
		$result		= file_get_contents($url);
		$j_result	= json_decode($result, true);
		
		for($i = 0; $i < count($j_result["results"]); $i++){
			$this->latitude		= $j_result["results"][$i]["geometry"]["location"]["lat"];
			$this->longitude	= $j_result["results"][$i]["geometry"]["location"]["lng"];
			$loc_name			= $j_result["results"][$i]["name"];
			$loc_alamat			= $j_result["results"][$i]["formatted_address"];
			$this->address		= $loc_name." \n".$loc_alamat;
			
			if($i == 2){ $i = count($j_result["results"]); }
		}// end for
	}
	
	public function getLongitude() { return $this->longitude; }
	public function getLatitude() { return $this->latitude; }
	public function getAddress() { return $this->address; }
	
	private function setRespons($result)
	{
		$o	= $result["ok"];
		
		if($o == "1")
		{
			return "success";
		}
		else
		{
			return "failed";
		}
	}
}

?>