<?php
namespace App\Utils;

class FuncLine
{
	var $message;
	var $token;
	var $tknsecret;
	var $media;
	var $chatid;
	var $jenissend;
	var $longitude;
	var $latitude;
	var $qurlocation;
	var $data;
	
	public function pushMessage($message) 
    {
		$response = FuncLine::exec_url('https://api.line.me/v2/bot/message/push',$this->token,json_encode($message));
		return $response;
    }
	
	public function exec_url($fullurl,$channelAccessToken,$message){
		$header = array(
            "Content-Type: application/json",
            'Authorization: Bearer '.$channelAccessToken,
        );
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_VERBOSE, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_POST,           1 );
		curl_setopt($ch, CURLOPT_POSTFIELDS,     $message); 
		curl_setopt($ch, CURLOPT_FAILONERROR, 0);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt($ch, CURLOPT_URL, $fullurl);
		
		$returned =  curl_exec($ch);
	
		return($returned);
	}
	
	public function exePush($msg, $tkn, $sct, $mda, $cid, $jns, $qlc)
	{
		$this->message		= $msg;
		$this->token		= $tkn;
		$this->tknsecret	= $sct;
		$this->media		= $mda;
		$this->chatid		= $cid;
		$this->jenissend	= $jns;
		$this->qurlocation	= $qlc;
		
		$this->data = array('to' => $this->chatid);
		
		switch($this->jenissend)
		{
			case "message":
				/*$this->data["message"] = array(
												array('type' => 'text','text' => $this->message)
											);*/
				$tmp = array(
									'messages' => array(
														array('type' => 'text','text' => $this->message)
													)
								);
				$this->data	= array_merge($this->data, $tmp);
			break;
			case "photo":
				/*$this->data["message"] = array(
												array(
													'type' => 'image',
													'originalContentUrl' => $this->media,
													'previewImageUrl'		=> $this->media
												)
											);*/
				$tmp = array(
									'messages' => array(
														array(
															'type' => 'image',
															'originalContentUrl' => $this->media,
															'previewImageUrl'		=> $this->media
														)
													)
								);
				$this->data	= array_merge($this->data, $tmp);
			break;
			case "photouri":
				/*$this->data["message"] = array(
												array(
													'type'	=> 'template',
													'altText'	=> 'this is image_carousel',
													'template'	=> array(
														'type'		=> 'image_carousel',
														'columns'	=> array(
															array(
																'imageUrl'	=> $this->media,
																'action'	=> array(
																	'type'	=> 'uri',
																	'label'	=> 'View Details',
																	'uri'	=> $this->message
																)// end array action
															)
														)// end array columns
													)// end array template
												)
											);*/
				$tmp = array(
									'messages' => array(
														array(
															'type'	=> 'template',
															'altText'	=> 'this is image_carousel',
															'template'	=> array(
																'type'		=> 'image_carousel',
																'columns'	=> array(
																	array(
																		'imageUrl'	=> $this->media,
																		'action'	=> array(
																			'type'	=> 'uri',
																			'label'	=> 'View Details',
																			'uri'	=> $this->message
																		)
																	)
																)
															)
														)
													)
								);
				$this->data	= array_merge($this->data, $tmp);
			break;
			case "video":
				/*$this->data["message"] = array(
												array(
													'type'					=> 'video',
													'originalContentUrl'	=> $this->media,
													'previewImageUrl'		=> $this->media
												)
											);*/
				$tmp = array(
									'messages' => array(
														array(
															'type' => 'video',
															'originalContentUrl' => $this->media,
															'previewImageUrl'		=> $this->media
														)
													)
								);
				$this->data	= array_merge($this->data, $tmp);
			break;
			case "location":
				$location				= $this->getLocation($this->qurlocation);
				
				/*$this->data["message"]	= array(
												array(
													'type'		=> 'location',
													'title'		=> $this->message,
													'address'	=> substr($this->getAddress(),0,100),
													'latitude'	=> $this->getLatitude(),
													'longitude'	=> $this->getLongitude()
												)
											);*/
				
				$tmp = array(
									'messages' => array(
														array(
															'type'		=> 'location',
															'title'		=> $this->message,
															'address'	=> substr($this->getAddress(),0,100),
															'latitude'	=> $this->getLatitude(),
															'longitude'	=> $this->getLongitude()
														)
													)
								);
				$this->data	= array_merge($this->data, $tmp);
			break;
			default:
				$this->data["chat_id"]	= "Request Empty!";
		}// end switch
		
		
		
		$rs = array("paramData" => $this->data, "paramToken" => $this->token);
		
		return $rs;
	}// end exePush
	
	public function getLocation($dataLoc) {
		$key 		= "AIzaSyDKDPrIj3_dyFf9_Es5JUK4Kw_P0hEnBk8";
		$queryclean = str_replace (" ", "+", $dataLoc);
		
		/* URL QUERY LOCATION */
		$url		= "https://maps.googleapis.com/maps/api/place/textsearch/json?query=".$queryclean."&key=".$key;
		$result		= file_get_contents($url);
		$j_result	= json_decode($result, true);
		
		for($i = 0; $i < count($j_result["results"]); $i++){
			$this->latitude		= $j_result["results"][$i]["geometry"]["location"]["lat"];
			$this->longitude	= $j_result["results"][$i]["geometry"]["location"]["lng"];
			$loc_name			= $j_result["results"][$i]["name"];
			$loc_alamat			= $j_result["results"][$i]["formatted_address"];
			$this->address		= $loc_name." \n".$loc_alamat;
			
			if($i == 2){ $i = count($j_result["results"]); }
		}// end for
	}
	
	public function getLongitude() { return $this->longitude; }
	public function getLatitude() { return $this->latitude; }
	public function getAddress() { return $this->address; }
	
	
	private function setRespons($result)
	{
		//die(print_r($result));
		$o	= $result["message"];
		
		if(!empty($o))
		{
			return "failed";
		}
		else
		{
			return "success";
		}
	}
	
}

?>