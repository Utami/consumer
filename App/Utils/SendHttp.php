<?php
namespace App\Utils;

class SendHttp{
	public static function chunkedresult($data) {
		$cont_type = "";
		$http_status_string = "";$http_status="";
		$temp = explode("\r\n\r\n",$data);
		$countPars = count($temp);
		$doChunk = "0";
		if ($countPars > 1) {
			$ret['header'] = trim($temp[0]);
			if ((preg_match('/<html>(.*)/i',trim($ret['header']),$match)) AND (preg_match('/<body>(.*)/i',trim($ret['header']),$match))){
				$ret['body'] = $data;
			}// end if preg_match
			else {
				$doChunk = "1";
				$ret['body']   = str_replace($ret['header'],"",$data);
				$statusTemp = "";
				if (preg_match('/^HTTP\/1\.[0-1] ([0-9][0-9][0-9] .*)\\r\\n/', $ret['header'], $match)) {
					$statusTemp = $match[1];
				}elseif (preg_match('/^HTTP\/1\.[0-1] ([0-9][0-9][0-9] .*)/', $ret['header'], $match)) {
					$statusTemp = $match[1];
				}// end if preg_match
			}// end if else preg_match
			$status = explode(" ",trim($statusTemp));
			if (isset($status[0])){ $http_status = $status[0]; }else{ $http_status = ""; }
			if (isset($match[0])){ $http_status_string = trim($match[0]); }else{ $http_status_string = ""; }
			if (preg_match('/Content\\-Type:\\s+([a-zA-Z\/]*)\\r\\n/',$ret['header'],$match)) { $cont_type = $match[1]; }elseif (preg_match('/Content\\-Type:\\s+([a-zA-Z\/]*)/',$ret['header'],$match)) { $cont_type = $match[1]; }// end if preg_match
			if ((strlen($cont_type) != (strlen(str_replace("xml","",$cont_type)))) AND ($cont_type != "")) { $cont_type = "xml"; }
		}// end if countPars
		if ($doChunk == "0") {
			$body = $data;
			if (substr(trim($data),0,5) == "<?xml") { $cont_type = "xml"; }
			if (preg_match('/^<html>(.*)<\/html>$/i',trim($data),$match)) {
				if (strlen(trim("<html>".$match[1]."</html>")) == strlen(trim($data)) ) {
					if (preg_match('/<body>(.*)<\/body>/i',trim($data),$match)){ $body = $match[1]; }
				}// end if strlen
			}// end if preg_match
			$ret['body'] = trim($body);
			$ret['header'] = "";
		}// end if doChunk
		$ret['http_status'] = $http_status;
		$ret['http_status_string'] = $http_status_string;
		if ($cont_type == ""){ $cont_type = "text/html"; }
		$ret['cont_type'] = $cont_type;

		return $ret;
	}// end chunkedresult
	
	public static function sendBackData($arrData, $urldata){
		$maxtime="30"; $maxsize="4096";$maxConnect='15';$maxtryconencttime='';
		$data_string = $arrData; //json_encode($arrData);
		$ch = curl_init();
		$keepAlive=TRUE;
		
		if($keepAlive){
			$GLOBALS['chCurl']=$ch;
			$header[]="Cache-Control: max-age=0";
			$header[]="Connection: keep-alive";
			$header[]="Keep-Alive: 300";
			$header[]="Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7";
			curl_setopt($ch, CURLOPT_MAXCONNECTS, $maxConnect);  // limiting connections
			curl_setopt($ch, CURLOPT_FORBID_REUSE, true);
		}
		if($maxtryconencttime == ''){
			switch($maxtime){
				case ($maxtime > '30'): $maxtryconencttime='30'; break;
				case ($maxtime > '25' && $maxtime <= '30'): $maxtryconencttime='25'; break;
				case ($maxtime > '20' && $maxtime <= '25'): $maxtryconencttime='20'; break;
				case ($maxtime > '15' && $maxtime <= '20'): $maxtryconencttime='15'; break;
				case ($maxtime > '10' && $maxtime <= '15'): $maxtryconencttime='10'; break;
				case ($maxtime > '5' && $maxtime <= '10'): $maxtryconencttime='5'; break;
				default:
					$maxtryconencttime='2';
			}// end switch
		}
		$url = $urldata;
		curl_setopt($ch, CURLOPT_URL,$url); // set url to post to
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Content-Length: ' . strlen($data_string)));
		curl_setopt($ch, CURLOPT_HEADER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, $maxtime); // times out after $maxtime
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $maxtryconencttime); // times out after $maxtryconencttime
		$result = curl_exec($ch);//print_r($rs);
		$cont_type="";$response="";
		if(curl_errno($ch) == "0"){
		  $ret['error']=0;
		  $temp=SendHttp::chunkedresult($result);
		  /*if(!((preg_match('/2[0-9][0-9]/',$temp['http_status'],$match)) OR ($temp['http_status'] == ""))){
		    $response=$temp['http_status_string'];
		  }else{
		    $response=$temp['body'];
			$cont_type=$temp['cont_type'];
		  }*/
		  $response=$temp['body'];
		}else{
		  $ret['error']=1;
		  $ret['full_iod']=$url;
		  $response="Error/Connection Failed";
		}
		$ret['full_iod']=$url;
		$ret['trx_date']=date('YmdHis');
		$ret['response']=trim($response);
		$ret['cont_type']=$cont_type;
		if(!($keepAlive)){ curl_close($ch); }
		return $ret;
	}// end sendBackData
	
	public static function sendToProvider($paramMethod,$path,$param,$ipno,$portno,$hitipe,$maxtime='',$maxsize='',&$ch='',$maxConnect='15' ,$maxtryconencttime='',$keepAlive=TRUE){

		if ($maxtime == ""){ $maxtime="30"; }
		if ($maxsize == ""){ $maxsize="4096"; }
		
		if (strtolower($paramMethod['method']) == "post"){ $path="$path"; }
		elseif (strtolower($paramMethod['method']) == "get"){ $path="$path?$param"; }
		else{
		  $ret['error']=TRUE;
		  $ret['response']="Method Not Detected";
		  return $ret;
		}
		if(isset($GLOBALS['chCurl'])){ $ch=$GLOBALS['chCurl']; }
		if($ch == ''){ $ch = curl_init(); }
		
		if($keepAlive){
		  $GLOBALS['chCurl']=$ch;
		  $header[]="Cache-Control: max-age=0";
		  $header[]="Connection: keep-alive";
		  $header[]="Keep-Alive: 300";
		  $header[]="Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7";
		  curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		  curl_setopt($ch, CURLOPT_MAXCONNECTS, $maxConnect);  // limiting connections
		  curl_setopt($ch, CURLOPT_FORBID_REUSE, true);
		}
		if(strtolower($paramMethod['method']) == "post"){
		  curl_setopt($ch, CURLOPT_POST, 1); // add POST fields
		  curl_setopt($ch, CURLOPT_POSTFIELDS, $param); // add POST fields
		}
		if(strtolower($hitipe == "https")){ $iod_path="https://$ipno:$portno/$path"; }
		else{ $iod_path = "http://$ipno:$portno/$path"; }
		
		if($maxtryconencttime == ''){
		  if($maxtime > '30'){ $maxtryconencttime='30'; }
		  elseif(($maxtime > '25') AND ($maxtime <= '30')){ $maxtryconencttime='25'; }
		  elseif(($maxtime > '20') AND ($maxtime <= '25')){ $maxtryconencttime='20'; }
		  elseif(($maxtime > '15') AND ($maxtime <= '20')){ $maxtryconencttime='15'; }
		  elseif(($maxtime > '10') AND ($maxtime <= '15')){ $maxtryconencttime='10'; }
		  elseif(($maxtime > '5') AND ($maxtime <= '10')){ $maxtryconencttime='5'; }
		  else{ $maxtryconencttime='2'; }
		}
		
		curl_setopt($ch, CURLOPT_URL,$iod_path); // set url to post to
		curl_setopt($ch, CURLOPT_TIMEOUT, $maxtime); // times out after $maxtime
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $maxtryconencttime); // times out after $maxtryconencttime
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); // return into a variable
		curl_setopt($ch, CURLOPT_HEADER, true);
		
		if(isset($paramMethod['https'])){
		  if(!(isset($paramMethod['sslkey']))){
		    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE); // https layer
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); // https layer
		  }
		}
		$result=curl_exec($ch); // run the whole process
		$cont_type="";$response="";
		if(curl_errno($ch) == "0"){
		  $ret['error']=0;
		  $temp = SendHttp::chunkedresult($result);
		  if(!((preg_match('/2[0-9][0-9]/',$temp['http_status'],$match)) OR ($temp['http_status'] == ""))){
		    $response=$temp['http_status_string'];
		  }else{
		    $response=$temp['body'];
			$cont_type=$temp['cont_type'];
		  }
		}else{
		  $ret['error']=1;
		  $response="Error/Connection Failed";
		}
		$ret['full_iod']=$iod_path;
		$ret['trx_date']=date('YmdHis');
		$ret['response']=trim($response);
		$ret['cont_type']=$cont_type;
		if(!($keepAlive)){ curl_close($ch); }
		return $ret;
	}// end sendToProvider
	
	public static function httpTelegram($arrData){
		$maxtime="30"; $maxsize="4096";$maxConnect='15';$maxtryconencttime='';
		$url = 'https://api.telegram.org/bot'.$arrData["paramToken"].'/'.$arrData["paramCommand"];
		$ch = curl_init();
		$keepAlive=TRUE;
		
		if($keepAlive){
			$GLOBALS['chCurl']=$ch;
			$header[]="Cache-Control: max-age=0";
			$header[]="Connection: keep-alive";
			$header[]="Keep-Alive: 300";
			$header[]="Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7";
			curl_setopt($ch, CURLOPT_MAXCONNECTS, $maxConnect);  // limiting connections
			curl_setopt($ch, CURLOPT_FORBID_REUSE, true);
		}
		if($maxtryconencttime == ''){
			switch($maxtime){
				case ($maxtime > '30'): $maxtryconencttime='30'; break;
				case ($maxtime > '25' && $maxtime <= '30'): $maxtryconencttime='25'; break;
				case ($maxtime > '20' && $maxtime <= '25'): $maxtryconencttime='20'; break;
				case ($maxtime > '15' && $maxtime <= '20'): $maxtryconencttime='15'; break;
				case ($maxtime > '10' && $maxtime <= '15'): $maxtryconencttime='10'; break;
				case ($maxtime > '5' && $maxtime <= '10'): $maxtryconencttime='5'; break;
				default:
					$maxtryconencttime='2';
			}// end switch
		}
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, count($arrData["paramData"]));
		curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($arrData["paramData"]));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, $maxtime); // times out after $maxtime
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $maxtryconencttime); // times out after $maxtryconencttime
		
		$result = curl_exec($ch);
		$cont_type="";$response="";
		if(curl_errno($ch) == "0"){
		  $ret['error']=0;
		  $temp=SendHttp::chunkedresult($result);
		  if(!((preg_match('/2[0-9][0-9]/',$temp['http_status'],$match)) OR ($temp['http_status'] == ""))){
		    $response=$temp['http_status_string'];
		  }else{
		    $response=$temp['body'];
			$cont_type=$temp['cont_type'];
		  }
		  //$response=$temp['body'];
		}else{
		  $ret['error']=1;
		  $ret['full_iod']=$url;
		  $response="Error/Connection Failed";
		}
		$ret['full_iod']=$url;
		$ret['trx_date']=date('YmdHis');
		$ret['response']=json_decode($response,true);
		$ret['cont_type']=$cont_type;
		if(!($keepAlive)){ curl_close($ch); }

		return $ret;
	}// end httpTelegram
	
	public static function httpLine($arrData){
		$maxtime="30"; $maxsize="4096";$maxConnect='15';$maxtryconencttime='';
		$url	= "https://api.line.me/v2/bot/message/push";
		$msg	= json_encode($arrData["paramData"], true);
		$header = array(
            "Content-Type: application/json",
            'Authorization: Bearer '.$arrData["paramToken"],
        );
		$ch = curl_init();
		$keepAlive=TRUE;
		
		if($keepAlive){
			$GLOBALS['chCurl']=$ch;
			$header[]="Cache-Control: max-age=0";
			$header[]="Connection: keep-alive";
			$header[]="Keep-Alive: 300";
			$header[]="Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7";
			curl_setopt($ch, CURLOPT_MAXCONNECTS, $maxConnect);  // limiting connections
			curl_setopt($ch, CURLOPT_FORBID_REUSE, true);
		}
		if($maxtryconencttime == ''){
			switch($maxtime){
				case ($maxtime > '30'): $maxtryconencttime='30'; break;
				case ($maxtime > '25' && $maxtime <= '30'): $maxtryconencttime='25'; break;
				case ($maxtime > '20' && $maxtime <= '25'): $maxtryconencttime='20'; break;
				case ($maxtime > '15' && $maxtime <= '20'): $maxtryconencttime='15'; break;
				case ($maxtime > '10' && $maxtime <= '15'): $maxtryconencttime='10'; break;
				case ($maxtime > '5' && $maxtime <= '10'): $maxtryconencttime='5'; break;
				default:
					$maxtryconencttime='2';
			}// end switch
		}
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_VERBOSE, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_POST,           1 );
		curl_setopt($ch, CURLOPT_POSTFIELDS,     $msg); 
		curl_setopt($ch, CURLOPT_FAILONERROR, 0);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_TIMEOUT, $maxtime); // times out after $maxtime
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $maxtryconencttime); // times out after $maxtryconencttime
		
		$result = curl_exec($ch);
		
		$cont_type="";$response="";
		if(curl_errno($ch) == "0"){
		  $ret['error']=0;
		  $temp=SendHttp::chunkedresult($result);
		  if(!((preg_match('/2[0-9][0-9]/',$temp['http_status'],$match)) OR ($temp['http_status'] == ""))){
		    $response=$temp['http_status_string'];
		  }else{
		    $response=$temp['body'];
			$cont_type=$temp['cont_type'];
		  }
		}else{
		  $ret['error']=1;
		  $ret['full_iod']=$url;
		  $response="Error/Connection Failed";
		}
		$ret['full_iod']=$url;
		$ret['trx_date']=date('YmdHis');
		$ret['response']=$response;
		$ret['cont_type']=$cont_type;
		if(!($keepAlive)){ curl_close($ch); }
		return $ret;
	}// end httpLine
	
	public static function httpFacebook($arrData){
		$maxtime="30"; $maxsize="4096";$maxConnect='15';$maxtryconencttime='';
		$url = "https://graph.facebook.com/v2.6/me/messages?access_token=".$arrData["paramToken"];
		if($arrData["paramFlag"] != 0){
			$url = "https://graph.facebook.com/v2.6/me/message_attachments?access_token=".$arrData["paramToken"];
		}
		$msg	= json_encode($arrData["paramData"], true);
		$ch = curl_init();
		$keepAlive=TRUE;
		
		if($keepAlive){
			$GLOBALS['chCurl']=$ch;
			$header[]="Cache-Control: max-age=0";
			$header[]="Connection: keep-alive";
			$header[]="Keep-Alive: 300";
			$header[]="Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7";
			curl_setopt($ch, CURLOPT_MAXCONNECTS, $maxConnect);  // limiting connections
			curl_setopt($ch, CURLOPT_FORBID_REUSE, true);
		}
		if($maxtryconencttime == ''){
			switch($maxtime){
				case ($maxtime > '30'): $maxtryconencttime='30'; break;
				case ($maxtime > '25' && $maxtime <= '30'): $maxtryconencttime='25'; break;
				case ($maxtime > '20' && $maxtime <= '25'): $maxtryconencttime='20'; break;
				case ($maxtime > '15' && $maxtime <= '20'): $maxtryconencttime='15'; break;
				case ($maxtime > '10' && $maxtime <= '15'): $maxtryconencttime='10'; break;
				case ($maxtime > '5' && $maxtime <= '10'): $maxtryconencttime='5'; break;
				default:
					$maxtryconencttime='2';
			}// end switch
		}
		
		curl_setopt($ch, CURLOPT_URL,$url); // set url to post to
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $msg);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		
		$result = curl_exec($ch);
		$cont_type="";$response="";
		if(curl_errno($ch) == "0"){
		  $ret['error']=0;
		  $temp=SendHttp::chunkedresult($result);
		  if(!((preg_match('/2[0-9][0-9]/',$temp['http_status'],$match)) OR ($temp['http_status'] == ""))){
		    $response=$temp['http_status_string'];
		  }else{
		    $response=$temp['body'];
			$cont_type=$temp['cont_type'];
		  }
		  //$response=$temp['body'];
		}else{
		  $ret['error']=1;
		  $ret['full_iod']=$url;
		  $response="Error/Connection Failed";
		}
		$ret['full_iod']=$url;
		$ret['trx_date']=date('YmdHis');
		$ret['response']=json_decode($response,true);
		$ret['cont_type']=$cont_type;
		if(!($keepAlive)){ curl_close($ch); }
		return $ret;
	}// end httpFacebook
	
	public static function sendEmail($header, $body, $url){
		$context = stream_context_create(array(
			'http' => array(
				'method' => 'POST',
				'header' => $header,
				'content' => $body
			)
		));
		$hit = @file_get_contents($url, false, $context);
		$rs = json_decode($hit, true);
		
		return $hit;
	}// end sendEmail
	
	public static function verifyContact($bearer, $contact){
		$dt = array('blocking' => 'wait', 'contacts' => $contact);
		$header = array("Content-Type: application/json", "Authorization: Bearer $bearer");
		
		$ch = curl_init("https://whatsapp.kata.ai/v1/contacts");
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($dt));
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		
		$rs	= curl_exec($ch);
		curl_close($ch);
		
		return json_decode($rs, true);
	}// end sendEmail
	
	public static function sendWhatsapp($arrData){
		$header = array(
			"Content-Type: application/json",
			"Authorization: Bearer ".$arrData["bearer"]
		);
		$ch = curl_init("https://whatsapp.kata.ai/v1/messages");
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($arrData["dtsend"]));
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		
		$result	= curl_exec($ch);
		curl_close($ch);
		$rs = json_decode($result, true);
		
		return $rs;
	}// end sendWhatsapp
}
?>
