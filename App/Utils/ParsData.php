<?php
namespace App\Utils;

class ParsData{
	//======================SMS=======================
	public static function parsSmsMq($arrData) {
		$sendParam=unserialize($arrData["sending_param"]);
		$rs = array(
					"id_p_s_cfg" => $arrData["id-p-s-cfg"],
					"id_c" => $arrData["division_id"],
					"dest_number" => $arrData["msisdn"],
					"sender_name" => $arrData["sender"],
					"sms_send" => $arrData["message"],
					"model_service" => $arrData["modelservice"],
					"time_req" => $arrData["dateNow"],
					"time_dbrecv" => $arrData["recvDate"],
					"time_startsend" => $arrData["dateNow"],
					"time_send" => $arrData["recvDate"],
					"time_sent" => $arrData["recvDate"],
					"retry_tosend" => "1",
					"replyCode" => $arrData["code_sms"],
					"id_hss_d" => "0",
					"senderName" => $arrData["sender"],
					"sms" => $arrData["message"],
					"modelService" => $arrData["modelservice"],
					"type_send" => $arrData["sending_type"],
					"sendingParam" => $arrData["sending_param"],
					"url_client_status_sent" => $sendParam["url_client_status_sent"],
					"url_client_delivery_report" => $sendParam["url_client_delivery_report"],
					"operator_prefix_db"=>$arrData["provider"],
					"client_prefix_dbname"=>$arrData["client_name"],
					"client_code_serv"=>$arrData["client_id"],
					"engine_name_recv"=>$arrData["servicetype"],
					"severity"=>$arrData["severity"],
					"i_tcp_user"=>$arrData["username"],
					"i_tcp_pass"=>"",
					"time_limitsend"=>"",
					"to_client_parameters"=>"",
					"f7_tid"=>$arrData["ref_id"]
				);
		return $rs;
	}// end parsSmsMq
	
	public static function getDataSms($data){
		$check=array_filter(get_object_vars($data));
		$arrData="err";
		if(!empty($check)){
			$channel=(isset($check["channel"])) ? $check["channel"] : "";
			$id_p_s_cfg=(isset($check["id_p_s_cfg"])) ? $check["id_p_s_cfg"] : "";
			$id_c=(isset($check["id_c"])) ? $check["id_c"] : "";
			$dest_number=(isset($check["dest_number"])) ? $check["dest_number"] : "";
			$sender_name=(isset($check["sender_name"])) ? $check["sender_name"] : "";
			$sms_send=(isset($check["sms_send"])) ? $check["sms_send"] : "";
			$model_service=(isset($check["model_service"])) ? $check["model_service"] : "";
			$time_req=(isset($check["time_req"])) ? $check["time_req"] : "";
			$time_dbrecv=(isset($check["time_dbrecv"])) ? $check["time_dbrecv"] : "";
			$replyCode=(isset($check["replyCode"])) ? $check["replyCode"] : "";
			$time_startsend=(isset($check["time_startsend"])) ? $check["time_startsend"] : "";
			$time_send=(isset($check["time_send"])) ? $check["time_send"] : "";
			$time_sent=(isset($check["time_sent"])) ? $check["time_sent"] : "";
			$retry_tosend=(isset($check["retry_tosend"])) ? $check["retry_tosend"] : "";
			$id_hss_d=(isset($check["id_hss_d"])) ? $check["id_hss_d"] : "";
			$type_send=(isset($check["type_send"])) ? $check["type_send"] : "";
			$sendingParam=(isset($check["sendingParam"])) ? $check["sendingParam"] : "";
			$url_client_delivery_report=(isset($check["url_client_delivery_report"])) ? $check["url_client_delivery_report"] : "";
			$url_client_status_sent=(isset($check["url_client_status_sent"])) ? $check["url_client_status_sent"] : "";
			$client_code_serv=(isset($check["client_code_serv"])) ? $check["client_code_serv"] : "";
			$operator_prefix_db=(isset($check["operator_prefix_db"])) ? $check["operator_prefix_db"] : "";
			$engine_name_recv=(isset($check["engine_name_recv"])) ? $check["engine_name_recv"] : "";
			$client_prefix_dbname=(isset($check["client_prefix_dbname"])) ? $check["client_prefix_dbname"] : "";
			$severity=(isset($check["severity"])) ? $check["severity"] : "";
			$i_tcp_user=(isset($check["i_tcp_user"])) ? $check["i_tcp_user"] : "";
			$i_tcp_pass=(isset($check["i_tcp_pass"])) ? $check["i_tcp_pass"] : "";
			$f7_tid=(isset($check["f7_tid"])) ? $check["f7_tid"] : "";
			$url_callback=(isset($check["url_callback"])) ? $check["url_callback"] : "";
			
			
			$arrData=array(
				"channel"=>$data->channel,
				"id_p_s_cfg"=>$data->id_p_s_cfg,
				"id_c" => $data->id_c,
				"dest_number" => $data->dest_number,
				"sender_name" => $data->sender_name,
				"sms_send" => $data->sms_send,
				"model_service" => $data->model_service,
				"time_req" => $data->time_req,
				"time_dbrecv" => $data->time_dbrecv,
				"code_sms" => $data->replyCode,
				"time_startsend" => $data->time_startsend,
				"time_send" => $data->time_send,
				"time_sent" => $data->time_sent,
				"retry_tosend" => $data->retry_tosend,
				"id_hss_d" => $data->id_hss_d,
				"type_send" => $data->type_send,
				"sendingParam" => $data->sendingParam,
				"url_client_status_sent" => $data->url_client_status_sent,
				"url_client_delivery_report" => $data->url_client_delivery_report,
				"client_prefix_dbname" => $data->client_prefix_dbname,
				"operator_prefix_db" => $data->operator_prefix_db,
				"client_code_serv" => $data->client_code_serv,
				"engine_name_recv" => $data->engine_name_recv,
				"severity" => $data->severity,
				"i_tcp_user"=>$data->i_tcp_user,
				"i_tcp_pass"=>$data->i_tcp_pass,
				"f7_tid"=>$data->f7_tid,
				"ref_id"=>$data->ref_id,
				"url_callback"=>$data->url_callback
			);
		}
		
		return $arrData;
	}// end getDataSms
	
	public static function parsSmsDB($arrData) {
		$sendParam=unserialize($arrData["sending_param"]);
		$rs = array(
				"id-c"=>$arrData["division_id"],"name-c"=>$arrData["division_name"],"id-u"=>"0","name-u"=>"","user_name_tcp"=>$arrData["username"],
				"id-m"=>"0","id-st"	=>$arrData["id-st"],"name-st"=>$arrData["name-st"],"id-c-ch"=>"0","name-c-ch"=>"",
				"id-p-s-cfg"=>$arrData['id-p-s-cfg'],"name-p-s-cfg"=>$sendParam[1],"id-hss"=>"0","name-hss"=>"","id-hss-d"=>"0",
				"name-hss-d"=>"","id-hss-m"=>"0","dest_number"=>$arrData["msisdn"],"smscno"=>"","shortnumber"=>$arrData['sender'],
				"trx_id_get"=>"","service_id_prvd"=>$sendParam['service_id_prvd'],"model_service" => $arrData["modelservice"],
				"ip_http_forwarded"=>"","ip_remote_addr"=>$arrData["ip_remote"],"name-sender"=>$arrData["sender"],
				"sms_req"=>"","hide_sms"=>"0","subject"=>$arrData["subject"],"sms_send"=>$arrData["message"],"code_sms"=>$arrData["code_sms"],
				"budget_code"=>"","unique_code"=>"","live_code"=>"0","status_send"=>"2","status_delivery"=>"3","retry_tosend"=>"1",
				"time_querecv"=>$arrData["dateNow"],"time_dbrecv"=>$arrData["recvDate"],"time_sched"=>$arrData["recvDate"],"time_startsend"=>$arrData["dateNow"],
				"time_send"=>$arrData["recvDate"],"time_sent"=>$arrData["recvDate"],"time_delivery"=>$arrData["recvDate"],"operator_prefix_db"=>$arrData["provider"],
				"client_prefix_dbname"=>$arrData["client_name"],"client_code_serv"=>$arrData["client_id"],"engine_name_recv"=>$arrData["servicetype"],
				"f7_tid"=>$arrData["ref_id"]
			);
		
		return $rs;
	}// end parsSmsDB
	
	//======================End SMS=======================
	
	//======================Email=======================
	
	public static function parsEmailMq($arrData) {
		$sendParam=unserialize($arrData["sending_param"]);
		$rs = array(
					"id_p_s_cfg" => $arrData["id-p-s-cfg"],
					"id_c" => $arrData["division_id"],
					"sendto" => $arrData["email"],
					"attachment" => $arrData["attachment"],
					"subject" => $arrData["subject"],
					"sms_send" => $arrData["message"],
					"time_req" => $arrData["dateNow"],
					"time_dbrecv" => $arrData["recvDate"],
					"time_startsend" => $arrData["dateNow"],
					"time_send" => $arrData["recvDate"],
					"time_sent" => $arrData["recvDate"],
					"retry_tosend" => "1",
					"replyCode" => $arrData["code_sms"],
					"id_hss_d" => "0",
					"modelService" => $arrData["modelservice"],
					"type_send" => $arrData["sending_type"],
					"sendingParam" => $arrData["sending_param"],
					"url_client_status_sent" => $sendParam["url_client_status_sent"],
					"url_client_delivery_report" => $sendParam["url_client_delivery_report"],
					"operator_prefix_db"=>$arrData["provider"],
					"client_prefix_dbname"=>$arrData["client_name"],
					"client_code_serv"=>$arrData["client_id"],
					"engine_name_recv"=>$arrData["servicetype"],
					"severity"=>$arrData["severity"],
					"i_tcp_user"=>$arrData["username"],
					"i_tcp_pass"=>"",
					"time_limitsend"=>"",
					"to_client_parameters"=>"",
					"f7_tid"=>$arrData["ref_id"],
					"url_callback"=>$data->url_callback
				);
		return $rs;
	}// end parsEmailMq
	
	public static function getDataEmail($data){
		$check=array_filter(get_object_vars($data));
		$arrData="err";
		if(!empty($check)){
			$channel=(isset($check["channel"])) ? $check["channel"] : "";
			$id_p_s_cfg=(isset($check["id_p_s_cfg"])) ? $check["id_p_s_cfg"] : "";
			$id_c=(isset($check["id_c"])) ? $check["id_c"] : "";
			$sendto=(isset($check["sendto"])) ? $check["sendto"] : "";
			$attachment=(isset($check["attachment"])) ? $check["attachment"] : "";
			$subject=(isset($check["subject"])) ? $check["subject"] : "";
			$sms_send=(isset($check["sms_send"])) ? $check["sms_send"] : "";
			$msgdetail	= (isset($check["msgdetail"])) ? $check["msgdetail"] : "";
			$time_req=(isset($check["time_req"])) ? $check["time_req"] : "";
			$time_dbrecv=(isset($check["time_dbrecv"])) ? $check["time_dbrecv"] : "";
			$replyCode=(isset($check["replyCode"])) ? $check["replyCode"] : "";
			$time_startsend=(isset($check["time_startsend"])) ? $check["time_startsend"] : "";
			$time_send=(isset($check["time_send"])) ? $check["time_send"] : "";
			$time_sent=(isset($check["time_sent"])) ? $check["time_sent"] : "";
			$retry_tosend=(isset($check["retry_tosend"])) ? $check["retry_tosend"] : "";
			$id_hss_d=(isset($check["id_hss_d"])) ? $check["id_hss_d"] : "";
			$type_send=(isset($check["type_send"])) ? $check["type_send"] : "";
			$sendingParam=(isset($check["sendingParam"])) ? $check["sendingParam"] : "";
			$url_client_delivery_report=(isset($check["url_client_delivery_report"])) ? $check["url_client_delivery_report"] : "";
			$url_client_status_sent=(isset($check["url_client_status_sent"])) ? $check["url_client_status_sent"] : "";
			$client_code_serv=(isset($check["client_code_serv"])) ? $check["client_code_serv"] : "";
			$operator_prefix_db=(isset($check["operator_prefix_db"])) ? $check["operator_prefix_db"] : "";
			$engine_name_recv=(isset($check["engine_name_recv"])) ? $check["engine_name_recv"] : "";
			$client_prefix_dbname=(isset($check["client_prefix_dbname"])) ? $check["client_prefix_dbname"] : "";
			$severity=(isset($check["severity"])) ? $check["severity"] : "";
			$i_tcp_user=(isset($check["i_tcp_user"])) ? $check["i_tcp_user"] : "";
			$i_tcp_pass=(isset($check["i_tcp_pass"])) ? $check["i_tcp_pass"] : "";
			$f7_tid=(isset($check["f7_tid"])) ? $check["f7_tid"] : "";
			$ref_id=(isset($check["ref_id"])) ? $check["ref_id"] : "";
			$url_callback=(isset($check["url_callback"])) ? $check["url_callback"] : "";
			
			$arrData=array(
				"channel"=>$channel,
				"id_p_s_cfg"=>$id_p_s_cfg,
				"id_c" => $id_c,
				"sendto" => $sendto,
				"attachment" => $attachment,
				"subject" => $subject,
				"sms_send" => $sms_send,
				"msgdetail" => $msgdetail,
				"time_req" => $time_req,
				"time_dbrecv" => $time_dbrecv,
				"code_sms" => $replyCode,
				"time_startsend" => $time_startsend,
				"time_send" => $time_send,
				"time_sent" => $time_sent,
				"retry_tosend" => $retry_tosend,
				"id_hss_d" => $id_hss_d,
				"type_send" => $type_send,
				"sendingParam" => $sendingParam,
				"url_client_status_sent" => $url_client_status_sent,
				"url_client_delivery_report" => $url_client_delivery_report,
				"client_prefix_dbname" => $client_prefix_dbname,
				"operator_prefix_db" => $operator_prefix_db,
				"client_code_serv" => $client_code_serv,
				"engine_name_recv" => $engine_name_recv,
				"severity" => $severity,
				"i_tcp_user"=>$i_tcp_user,
				"i_tcp_pass"=>$i_tcp_pass,
				"f7_tid"=>$f7_tid,
				"ref_id"=>$ref_id,
				"url_callback"=>$url_callback
			);
		}
		
		return $arrData;
	}// end getDataEmail
	
	//======================End Email=======================
	
	//======================Socmed=======================
	
	public static function parsSocmedMq($arrData) {
		$sendParam=unserialize($arrData["sending_param"]);
		$rs = array(
					"id_p_s_cfg" => $arrData["id-p-s-cfg"],
					"id_c" => $arrData["division_id"],
					"token" => $arrData["token"],
					"secret_token" => $arrData["secret_token"],
					"message_type" => $arrData["message_type"],
					"location" => $arrData["location"],
					"subject" => $arrData["subject"],
					"message" => $arrData["message"],
					"time_req" => $arrData["dateNow"],
					"time_dbrecv" => $arrData["recvDate"],
					"time_startsend" => $arrData["dateNow"],
					"time_send" => $arrData["recvDate"],
					"time_sent" => $arrData["recvDate"],
					"retry_tosend" => "1",
					"replyCode" => $arrData["code_sms"],
					"id_hss_d" => "0",
					"modelService" => $arrData["modelservice"],
					"type_send" => $arrData["sending_type"],
					"sendingParam" => $arrData["sending_param"],
					"url_client_status_sent" => $sendParam["url_client_status_sent"],
					"url_client_delivery_report" => $sendParam["url_client_delivery_report"],
					"operator_prefix_db"=>$arrData["provider"],
					"client_prefix_dbname"=>$arrData["client_name"],
					"client_code_serv"=>$arrData["client_id"],
					"engine_name_recv"=>$arrData["servicetype"],
					"severity"=>$arrData["severity"],
					"i_tcp_user"=>$arrData["username"],
					"i_tcp_pass"=>"",
					"time_limitsend"=>"",
					"to_client_parameters"=>"",
					"f7_tid"=>$arrData["ref_id"]
				);
		return $rs;
	}// end parsSocmedMq
	
	public static function getDataArrSocmed($data){
		$check=array_filter(get_object_vars($data));
		$arrData="err";
		if(!empty($check)){
			$channel= (isset($check["channel"])) ? $check["channel"] : "";
			$id_c	=(isset($check["id_c"])) ? $check["id_c"] : "";
			$sender	= (isset($check["token"])) ? $check["token"] : "";
			$msg	= (isset($check["message"])) ? $check["message"] : "";
			$chatid	= (isset($check["chatid"])) ? $check["chatid"] : "";
			$secret_token	= (isset($check["secret_token"])) ? $check["secret_token"] : "";
			$media	= (isset($check["media"])) ? $check["media"] : "";
			$jenis	= (isset($check["social_type"])) ? $check["social_type"] : "";
			$location	= (isset($check["location"])) ? $check["location"] : "";
			$client_code_serv=(isset($check["client_code_serv"])) ? $check["client_code_serv"] : "";
			$operator_prefix_db = (isset($check["operator_prefix_db"])) ? $check["operator_prefix_db"] : "";
			$engine_name_recv=(isset($check["engine_name_recv"])) ? $check["engine_name_recv"] : "";
			$client_prefix_dbname = (isset($check["client_prefix_dbname"])) ? $check["client_prefix_dbname"] : "";
			$message_type	= (isset($check["message_type"])) ? $check["message_type"] : "";
			$f7_tid=(isset($check["f7_tid"])) ? $check["f7_tid"] : "";
			$replyCode=(isset($check["replyCode"])) ? $check["replyCode"] : "";
			$id_p_s_cfg=(isset($check["id_p_s_cfg"])) ? $check["id_p_s_cfg"] : "";
			$time_req=(isset($check["time_req"])) ? $check["time_req"] : "";
			$time_startsend=(isset($check["time_startsend"])) ? $check["time_startsend"] : "";
			$time_send=(isset($check["time_send"])) ? $check["time_send"] : "";
			$time_sent=(isset($check["time_sent"])) ? $check["time_sent"] : "";
			$time_dbrecv=(isset($check["time_dbrecv"])) ? $check["time_dbrecv"] : "";
			$ref_id=(isset($check["ref_id"])) ? $check["ref_id"] : "";
			$url_callback=(isset($check["url_callback"])) ? $check["url_callback"] : "";
			$sending_param=(isset($check["sendingParam"])) ? $check["sendingParam"] : "";
			
			$arrData=array(
				"channel"=>$channel,
				"id_p_s_cfg"=>$id_p_s_cfg,
				"id_c" => $id_c,
				"sender"=>$sender,
				"msg"=>$msg,
				"secret_token"=>$secret_token,
				"chatid"=>$chatid,
				"media"=>$media,
				"jenis"=>$jenis,
				"location"=>$location,
				"operator_prefix_db"=>$operator_prefix_db,
				"client_code_serv"=>$client_code_serv,
				"client_prefix_dbname"=>$client_prefix_dbname,
				"engine_name_recv"=>$engine_name_recv,
				"sendingParam" => $sending_param,
				"time_req" => $time_req,
				"time_startsend" => $time_startsend,
				"time_send" => $time_send,
				"time_sent" => $time_sent,
				"time_dbrecv" => $time_dbrecv,
				"code_sms" => $replyCode,
				"f7_tid"=>$f7_tid,
				"ref_id"=>$ref_id,
				"url_callback"=>$url_callback
			);
		}// end
		//ParsData
		return $arrData;
	}// end getDataArrSocmed
	
	public static function getParamWA($prms){
		$rs = array();
		foreach ($prms as $k => $v) {
			$tempkey['default'] = $v;
			array_push($rs, $tempkey);
		}
		
		return $rs;
	}// end getParamWA
	
	//======================End Socmed=======================
	
	//======================Status Delivery==================
	public static function getDataArrDlvr($data){
		$check=array_filter(get_object_vars($data));
		$arrData="err";
		if(!empty($check)){
			$url	= (isset($check["url"])) ? $check["url"] : "";
			$dtsts	= (isset($check["dtStsDlvr"])) ? $check["dtStsDlvr"] : "";
			$arrData=array(
				"url"	=> $url,
				"dtsts"	=> $dtsts
			);
		}
		return $arrData;
	}// end getDataArrDlvr
	//======================End Status Delivery==============
	public static function CreateJson($arrJson){
		$jencode=json_encode($arrJson);
		return $jencode;
	}
	
	public static function getHeader($username, $password){
		$auth = base64_encode("$username:$password");
		$rs = array(
			"Authorization:Basic $auth",
			"Content-Type: multipart/form-data ; boundary=" . $GLOBALS["MULTIPART_BOUNDARY"]
		);
		
		return $rs;
	}// end getHeader
	
	public static function getBodyPart($FORM_FIELD, $value){
		$rs = "";
		if ($FORM_FIELD === 'attachment') {
			$rs = 'Content-Disposition: form-data; name="'.$FORM_FIELD.'"; filename="'.basename($value).'"' . $GLOBALS["EOL"];
			$rs .= 'Content-Type: '.mime_content_type($value) . $GLOBALS["EOL"];
			$rs .= 'Content-Transfer-Encoding: binary' . $GLOBALS["EOL"];
			$rs .= $GLOBALS["EOL"] . file_get_contents($value) .$GLOBALS["EOL"];
		}else{
			$rs = 'Content-Disposition: form-data; name="' . $FORM_FIELD . '"' . $GLOBALS["EOL"];
			$rs .= $GLOBALS["EOL"] . $value . $GLOBALS["EOL"];
		}
		
		return $rs;
	}// end getBodyPart
	
	public static function getBody($arrEmail){
		$rs = "";
		foreach ($arrEmail as $k => $v) {
			//$rs.= '--' . $GLOBALS["MULTIPART_BOUNDARY"] . $GLOBALS["EOL"];
			//$rs.= 'Content-Disposition: form-data; name="' . $k . '"' . $GLOBALS["EOL"];
			//$rs.= $GLOBALS["EOL"] . $v . $GLOBALS["EOL"];
			$v = is_array($v) ? $v : array($v);
			foreach ($v as $val) {
				$rs .= '--' . $GLOBALS["MULTIPART_BOUNDARY"] . $GLOBALS["EOL"] . ParsData::getBodyPart($k, $val);
			}
		}
		$rs.= '--' . $GLOBALS["MULTIPART_BOUNDARY"] . '--';
		
		return $rs;
	}// end getBody
}
?>