<?php

namespace App\Utils;

class FuncFacebook
{
	
	var $message;
	var $token;
	var $media;
	var $mediaid;
	var $valmedia;
	var $chatid;
	var $jenissend;
	var $longitude;
	var $latitude;
	var $qurlocation;
	var $address;
	var $data;
	var $flag		= 0;
	var $urlpush	= 'https://graph.facebook.com/v2.6/me/messages?access_token=';
	var $urlmedia	= 'https://graph.facebook.com/v2.6/me/message_attachments?access_token=';
	
	public function exePush($msg, $tkn, $mda, $cid, $jns, $qlc, $mediaid="")
	{
		$this->message		= $msg;
		$this->token		= $tkn;
		$this->media		= $mda;
		$this->chatid		= $cid;
		$this->jenissend	= $jns;
		$this->qurlocation	= $qlc;
		
		$this->data = array("recipient" => array("id" => $this->chatid));
		
		switch($this->jenissend)
		{
			case "message":
				$this->data["message"] = array("text" => $this->message);
			break;
			
			case "photoOne":
				$this->flag	= 1;
				$this->data["message"] = array(
											"attachment"	=> array(
												"type"		=> "image",
												"payload"	=> array(
													"is_reusable"	=> true,
													"url"			=> $this->media
												)
											)
										);
			break;
			
			case "photo":case "video":
				$this->data["message"] = array(
												"attachment" => array(
													"type"		=> "template",
													"payload"	=> array(
														"template_type"	=> "media",
														"elements"		=> array(
															"media_type"	=> "image",
															"attachment_id"	=> $mediaid
														)// end elements
													)// end payload
												)// end attachment
											);
			break;
		}// end switch
		$rs = array("paramData" => $this->data, "paramToken" => $this->token, "paramFlag" => $this->flag);
		
		return $rs;
	}//end exePush
	
	private function setRespons($result)
	{
		$r	= json_decode($result, true);
		$o	= $r["recipient_id"];
		
		if(!empty($o))
		{
			return "success";
		}
		else
		{
			return "failed";
		}
	}
}


?>