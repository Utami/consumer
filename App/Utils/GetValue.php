<?php

namespace App\Utils;

class GetValue{
	
	public static function getNameTable($arrData) {
		$serviceClient=$arrData["srvc"];
		$prefix=$arrData["prefix"];
		switch($serviceClient){
			case "otp": $rs="i-".$arrData["thn"]."_".$arrData["bln"]."-".$arrData["nmtbl"]."-prior"; break;
			default:
				switch(strtolower($prefix)){
					case "tsel":
					case "isat":
					case "xl":
					case "email":
					case "socmed":
						$rs="i-".$arrData["thn"]."_".$arrData["bln"]."-".$arrData["nmtbl"]."-".strtolower($prefix);
					break;
					default:
						$rs="i-".$arrData["thn"]."_".$arrData["bln"]."-".$arrData["nmtbl"];
				}
		}// end switch
		
		return $rs;
	}// end getNameTable
	
	public static function GetMyMicrotime6() {
		$c="1000000";
		while (strlen($c) > 6) {
			$b=explode(" ",microtime());
			$c=round($b['0']*1000000);
		}
		switch(strlen($c)){
			case 0: $c="000000"; break;
			case 1: $c="00000"."$c"; break;
			case 2: $c="0000"."$c"; break;
			case 3: $c="000"."$c"; break;
			case 4: $c="00"."$c"; break;
			case 5: $c="0"."$c"; break;
		}

		return $c;
	}// End GetMyMicrotime6
	
	public function GetMyMicrotime4() {
		$c = "10000";
		while (strlen($c) > 4) {
			$b=explode(" ",microtime());
			$c=round($b['0']*10000);
		}
		switch(strlen($c)){
			case 0: $c="0000"; break;
			case 1: $c="000"."$c"; break;
			case 2: $c="00"."$c"; break;
			case 3: $c="0"."$c"; break;
		}
		return $c;
	}// end GetMyMicrotime4
	
	public static function array_random_assoc($arr, $num = 1) {
		$keys=array_keys($arr);
		shuffle($keys);
		$r=array();
		for ($i = 0; $i < $num; $i++) {
			$r[$i] = $arr[$keys[$i]];
		}
		return $r;
	}// End array_random_assoc
	public static function random_by_addition($a,$c = 0) {
		if ($c == 0) {
			srand((float)microtime()*1000000);
			$c=rand();
		}
		$jml = (int)$a + (int)$c;
		$jml = "".$jml;
		while (strlen($jml) > 1 ) {
			$jumlah = 0;
			for ($i = 0;$i < strlen($jml);$i++ ) {
				$jumlah = $jumlah + $jml[$i];
			}
			$jml = "".$jumlah;
		}
		return $jml;
	}// End random_by_addition
	
	public static function RandNumber() {
		$rv=array('1','2','3','4','5','6','7','8','9','0');
		$rv=GetValue::array_random_assoc($rv, count($rv));
		$length = rand(1,count($rv)-1);
		for($i = 0;$i <= $length;$i++ ){
			$randArr[$i] = GetValue::random_by_addition($rv[$i]);
		}
		$rv=array_merge($rv ,$randArr );
		$rv=GetValue::array_random_assoc($rv, count($rv));
		return $rv[0];
	}// End RandNumber
	
	public static function GetF7TrxID($msisdn,$servid='',$mode='2') {
		$unix_id="";
		$dateHIS =  (date('H') * 3600) + (date('i') * 60) + date('s');
        $temp = "";
        for ($i=0; $i < (5 - strlen($dateHIS)); $i++) { $temp .=  "0"; }
		
		$dateDHIS =  (((date('d') - 1) * 1000 ) + 86400 ) + $dateHIS;
        $temp2 = "";
        for ($i=0; $i < (6 - strlen($dateDHIS)); $i++) { $temp2 .=  "0"; }
		
		$hp = substr($msisdn,-4);
        $hp2 = substr($msisdn,-6);
        if (strlen($servid) > 2) $servid = substr($servid,0,2);
        if (strlen($servid) == 1) $servid = "0".$servid;
		$lsid=strlen($servid);
		switch($lsid){
			case ($lsid > 2): $servid = substr($servid,0,2); break;
			case ($lsid == 1):  $servid = "0".$servid; break;
			case ($lsid == ""):  $servid = '99'; break;
		}
		switch($mode){
			case "1":
				$unix_id = $temp2.$dateDHIS.GetValue::GetMyMicrotime6().GetValue::RandNumber().GetValue::RandNumber().GetValue::RandNumber().GetValue::RandNumber();
			break;
			case "2":
				$unix_id = $servid.$hp.$temp2.$dateDHIS.GetValue::GetMyMicrotime4().GetValue::RandNumber().GetValue::RandNumber().GetValue::RandNumber().GetValue::RandNumber();
			break;
			case "3":
				$unix_id = $hp2.$temp.$dateHIS.GetValue::GetMyMicrotime4().GetValue::RandNumber();
			break;
			case "5";
				$unix_id = $servid.$hp.$temp2.GetValue::$dateDHIS.GetValue::RandNumber().GetValue::RandNumber();
			break;
			case "7"://33 Digit
				$unix_id = date('Y').date('m').date('d').$servid.$hp2.$temp.$dateHIS.GetValue::GetMyMicrotime6().GetValue::RandNumber();
				$unix_id.=GetValue::RandNumber().GetValue::RandNumber().GetValue::RandNumber().GetValue::RandNumber().GetValue::RandNumber();
			break;
			case "8":// 29 digit
				$unix_id = date('Y').date('m').date('d').$servid.$hp2.$temp.$dateHIS.GetValue::GetMyMicrotime4();
				$unix_id.=GetValue::RandNumber().GetValue::RandNumber().GetValue::RandNumber().GetValue::RandNumber();
			break;
			default:
				$unix_id = $servid.$hp2.$temp.$dateHIS.GetValue::GetMyMicrotime4().GetValue::RandNumber().GetValue::RandNumber().GetValue::RandNumber();
		}
		return $unix_id;
	}// end GetF7TrxID
	
	
	public static function x6splitContBaris2data(&$ret,&$kolom, $contdata,$contbaris){
		$ret ="";$kolom = "0";$data="";$kc = "0";$endMinSpasi = 0;
		for ($i=0;$i<$contbaris;$i++){
			if (($contbaris <= 1) && (!(is_array($contdata)))){
				$contdatabaris		= trim($contdata);
				$contdatabaris_len	= strlen($contdata);
			}else{
				$contdatabaris		= trim($contdata[$i]);
				$contdatabaris_len	= strlen($contdata[$i]);
			}
			$char='';
			$in_string=false;
			$k="0";
			for ($j=0;$j<=$contdatabaris_len;$j++){
				$char = substr($contdatabaris,$j,1);
				if ($j == "0") {
					$lj = 0;$startCek = FALSE;
					for ($li = 0; $li <=$contdatabaris_len ; $li++) {
						$char2 = substr($contdatabaris,$li,1);
						if ($char2 != '"') {
							if (($char2 == " ") && (!($startCek))) { continue; }else{ break; }
						}else{ $startCek = TRUE; }
						$lj++;
					}// end for contdatabaris_len
					if (($lj % 2 ) == "1" ) {
						$in_string = true;
						$start_char = $char;
					}else{ $in_string = false; }
				}
				if (($char == ",")&& (!($in_string)) ){
					$ret[$i][$k]	= trim(substr($contdatabaris , 0, $j));
					$data		= $ret[$i][$k];
					$contdatabaris	= trim(substr($contdatabaris , $j+1));
					$contdatabaris_len= strlen($contdatabaris);
					$j = -1;
					$k++;
				}
				if (($char == ",") && ($in_string)){
					$lj = 1;$startCek = FALSE;$endMinSpasi = 0;
					for ($li = $j-1; $li >= 0  ; $li--) {
						$char2 = substr($contdatabaris,$li,1);
						if ($char2 != '"') {
							if (($char2 == " ") && (!($startCek))) {
								$endMinSpasi++;
								continue;
							}else{ break; }
						}else{ $startCek = TRUE; }
						$lj++;
					}// end for $li
					if (($lj % 2) == "0") { $end_string = true; }
					else{ $end_string = false; }
					if ($end_string){
						$ret[$i][$k]	= substr($contdatabaris , 1, $j-2-$endMinSpasi);
						$ret[$i][$k]	= str_replace("\"\"","\"",$ret[$i][$k]);
						$data		= $ret[$i][$k];
						$contdatabaris	= trim(substr($contdatabaris , $j+1));
						$contdatabaris_len= strlen($contdatabaris);
						$j = -1;
						$in_string = false;
						$k++;
					}
				}
				if (($j ==  $contdatabaris_len) && (!($in_string))) {
					$ret[$i][$k]	= trim(substr($contdatabaris , 0, $j));
					$data		= $ret[$i][$k];
					$contdatabaris_len= -1;
					$k++;
				}
				if (($j ==  $contdatabaris_len)&& ($in_string)) {
					$ret[$i][$k]	= substr($contdatabaris , 1, $j-2-$endMinSpasi);
					$ret[$i][$k]	= str_replace("\"\"","\"",$ret[$i][$k]);
					$data		= $ret[$i][$k];
					$contdatabaris_len= -1;
					$k++;
				}
				$kc = $k - 1;
				if ($data == '""'){ $ret[$i][$kc] = ""; }
			}// end for contdatabaris_len
		}// end for contbaris
		if ((count($ret[0]) <= 1) AND ($ret[0][0] == "")){ $kolom = "0"; }else{ $kolom = $k; }
	}// end x6splitContBaris2data
	
	public static function xml_reader($data){
		$values = array();
		$index = array();
		$array = array();
		$parser = xml_parser_create();
		xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
		xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
		xml_parse_into_struct($parser, $data, $values, $index);
		xml_parser_free($parser);
		$i = 0;
		if (!(isset($values[$i]['tag']))){ $values[$i]['tag'] = ""; }
		if (!(isset($values[$i]['attributes']))){ $values[$i]['attributes'] = ""; }
		$name = $values[$i]['tag'];
		$array[$name]['@'] = $values[$i]['attributes'];
		$array[$name]['#'] = GetValue::get_xml_array($values, $i);
		return $array;
	}// end xml_reader
	
	public static function get_xml_array($values, &$i){
	  $child = array();
	  if (isset($values[$i]['value'])) if ($values[$i]['value']) array_push($child, $values[$i]['value']);
	  else $values[$i]['value'] = "";
	  while (++$i < count($values))
	  {
		switch ($values[$i]['type'])
		{
		  case 'cdata':
			array_push($child, $values[$i]['value']);
		  break;
	
		  case 'complete':
			if (!(isset($values[$i]['tag']))) $values[$i]['tag'] = "";
		if (!(isset($values[$i]['tag']))) $values[$i]['tag'] = "";
			$name = $values[$i]['tag'];
		if (isset($child[$name])) {
				$size = sizeof($child[$name]);
		}
		else $size = "0";
			if (!(isset($values[$i]['attributes']))) $values[$i]['attributes'] = "";
		if (!(isset($values[$i]['value']))) $values[$i]['value'] = "";
			$child[$name][$size]['#'] = $values[$i]['value'];
			if($values[$i]['attributes'])
			{
				$child[$name][$size]['@'] = $values[$i]['attributes'];
			}
		
		  break;
	
		  case 'open':
			$name = $values[$i]['tag'];
			$size = sizeof($child[$name]);
		if ((isset($values[$i]['attributes']))) {
				if($values[$i]['attributes'])
			{
				$child[$name][$size]['@'] = $values[$i]['attributes'];
				$child[$name][$size]['#'] = GetValue::get_xml_array($values, $i);
				}
		}
			else
			{
			  $child[$name][$size]['#'] = GetValue::get_xml_array($values, $i);
			}
		  break;
	
		  case 'close':
			return $child;
		  break;
		}
	  }
	  return $child;
	}// End get_xml_array
	
	public static function GetValSendResponse($data,$param){
		$ret = array('status_send' => '', 'errmsg' => '', 'status' => '', 'code'   => '');
		$resp=trim($data["response"]);
		if ((strlen($resp) > 2) && ((strlen(str_replace("timeout","",strtolower($resp))) != strlen($resp)) || (strlen(str_replace("time out","",strtolower($resp))) != strlen($resp)) || (strlen(str_replace("connection fail","",strtolower($resp))) != strlen($resp)) || (strlen(str_replace("refuse","",strtolower($resp))) != strlen($resp)) || (strlen(str_replace("conenction false","",strtolower($resp))) != strlen($resp)) || (strlen(str_replace("service unavailable","",strtolower($resp))) != strlen($resp)) || (strlen(str_replace("access denied","",strtolower($resp))) != strlen($resp)) || (strlen(str_replace("Invalid User Or Password","",strtolower($resp))) != strlen($resp))) ){
			$ret['status_send'] = "2";
			$ret['status'] = "00";
			$ret['code']   = "$resp";
			//return $ret;
		}// end if cek error
		else{
		
		if ($param['portmodel'] == "smpp") {
			if ($resp == "0: Accepted for delivery") {
				$ret['status'] = "1";
				if (isset($param['_tid'])) $ret['code'] = $param['_tid'];
				else $ret['code']   = $param['date_micr_now'];				
				$ret['status_send'] = "1";
			}else{
				$ret['status'] = "00";
				$ret['code']   = "$resp";
				$ret['status_send'] = "0";
			}
			return $ret;
		}else{
			GetValue::x6splitContBaris2data($dataCheck,$jml,$param['getresp_statussend'],1);
			
			if ($jml <= "0") {
				$ret['status_send'] = "0";
				$ret['status'] = "00";
				$ret['code']   = "Query Provider Not Set";
				return $ret;
			}else{
				GetValue::x6splitContBaris2data($dataComp,$jml2,$param['message_send'],"1");
				if ($jml2 <= "0") {
					$ret['status_send'] = "0";
					$ret['status'] = "00";
					$ret['code']   = "Query Provider Not Set";
					return $ret;
				}// end if $jml2 x6splitContBaris2data 2
				$contType = $dataComp[0][0];
				if ($contType == "xml") {
					if (strlen(str_replace("<?xml","",$resp)) == strlen($resp)) {
						$ret['status'] = "00";
						$ret['code']   = "$resp";
						$ret['status_send'] = "0";
					}else{
						$xmldata =GetValue::xml_reader($resp);
						if (isset($xmldata[$dataComp[0][1]]['#'][$dataComp[0][2]]['0']['#'])) {
							$ret['status']=$xmldata[$dataComp[0][1]]['#'][$dataComp[0][2]]['0']['#'];
						}else{ $ret['status'] = ""; }
						if (isset($xmldata[$dataComp[0][1]]['#'][$dataComp[0][3]]['0']['#'])) {
							$ret['code']=$xmldata[$dataComp[0][1]]['#'][$dataComp[0][3]]['0']['#'];
						}else{ $ret['code'] = ""; }
						$ret['errmsg'] = "";
						if (!(isset($dataComp[0][4]))){ $dataComp[0][4] = ""; }
						if ($dataComp[0][4] != ""){ $ret['errmsg']=$xmldata[$dataComp[0][1]]['#'][$dataComp[0][4]]['0']['#']; }
					}
				}// end if xml
				else{
					$dataComp[0][1] = stripcslashes($dataComp[0][1]);
					if ($dataComp[0][1] != "") {
						$temp = explode($dataComp[0][1],$resp);
						if (count($temp) == "1") {
							$ret['status'] = "00";
							$ret['status_send'] = "0";
							$ret['code']   = $resp;
							return $ret;
						}else{
							$ret['status'] = $temp[$dataComp[0][2]];
							$ret['code']   = $temp[$dataComp[0][3]];
							$ret['errmsg'] = "";
							if (!(isset($dataComp[0][4]))){ $dataComp[0][4] = ""; }
							if ($dataComp[0][4] != "") {
								if (isset($temp[$dataComp[0][4]])){ $ret['errmsg']   = $temp[$dataComp[0][4]]; }
								else{ $ret['errmsg'] = ""; }
							}
						}// end if count temp
					}// end if $dataComp[0][1]
					else{
						$ret['status'] = $resp;
						$ret['code']   = $resp;
					}// end else $dataComp[0][1]
				}// end else xml
				//Check If Use More value in Message ID
				if (($dataCheck[0][0] == "2") ||  ($dataCheck[0][0] == "3") || ($dataCheck[0][0] == "6") ||  ($dataCheck[0][0] == "7") || ($dataCheck[0][0] == "10") ||  ($dataCheck[0][0] == "11") || ($dataCheck[0][0] == "14") ||  ($dataCheck[0][0] == "15")) {
					GetValue::x6splitContBaris2data($dataComp2,$jml3,$param['moremessage_send'],"1");
					if ($jml3 <= "0") {
						$ret['status_send'] = "0";
						$ret['status'] = "00";
						$ret['code']   = "Choose More But Query Provider Not Set";
						return $ret;
					}
					$dataComp2[0][0] = stripcslashes($dataComp2[0][0]);
					if ($dataComp2[0][0] != "") {
						$temp2 = explode($dataComp2[0][0],$ret['code']);
						if (count($temp2) == 1) {
							$ret['status'] = "00";
							$ret['status_send'] = "0";
							$ret['code']   = $resp;
							return $ret;
						}else{
							if ($temp2[$dataComp2[0][1]] != "" ){ $ret['status'] = $temp2[$dataComp2[0][1]]; }
							if ($temp2[$dataComp2[0][2]] != "" ){ $ret['code']   = $temp2[$dataComp2[0][2]]; }
						}
					}// end if $dataComp2[0][0]
				}//End If Check If Use More value in Message ID
				$status_send = "0";
				$dtChk=$dataCheck[0][0];
				switch($dtChk){
					case "0":
					case "2":
						if (strlen($dataCheck[0][1]) == strlen($ret['status'])){ $status_send = "1"; }
					break;
					case "1":
					case "3":
						if (strlen($dataCheck[0][1]) == strlen($ret['code'])){ $status_send = "1"; }
					break;
					case "4": if (strlen($ret['status']) > strlen($dataCheck[0][1])){ $status_send = "1"; } break;
					case "5":
					case "7":
						if (strlen($ret['code']) > $dataCheck[0][1]){ $status_send = "1"; }
					break;
					case "6": if (strlen($ret['status']) > $dataCheck[0][1]){ $status_send = "1"; } break;
					case "8":
					case "10":
						if (@$dataCheck[0][1] == @$ret['status']){ $status_send = "1"; }
					break;
					case "9":
					case "11":
						if ($dataCheck[0][1] == $ret['code']){ $status_send = "1"; }
					break;
					case "12":
					case "14":
						$l = strlen($dataCheck[0][1]);
						if ($dataCheck[0][1] == substr($ret['status'],0,$l)) $status_send = "1";
					break;
					case "13":
					case "15":
						$l = strlen($dataCheck[0][1]);
						if ($dataCheck[0][1] == substr($ret['code'],0,$l)) $status_send = "1";
					break;
				}// end switch
				if ($status_send == "0" ) { $ret['status_send'] = "0"; }
				else{ $ret['status_send'] = "1"; }
			}// end if $jml x6splitContBaris2data 1
		}
		}
		$rs="";
		foreach($ret as $key => $val){
			$rs.=$key.":".$val.":;:";
		}
		$rs=substr($rs, 0, -3);
		
		return $ret;
	}// End GetValSendResponse
	
	public static function GetValDeliveryResponse($data){
		$rs = "";
		$rs['code'] = $data['delivery'];
		if($data['type_delivery'] == "0"){ $rs['status_delivery']="1"; }
		else{
			GetValue::x6splitContBaris2data($data_pending,$jml3a,$data['statusdlvr_pending'],1);
			GetValue::x6splitContBaris2data($data_undelivered,$jml3c,$data['statusdlvr_undelivered'],1);
			GetValue::x6splitContBaris2data($data_delivered,$jml3b,$data['statusdlvr_delivered'],1);
			
			if((in_array(trim($rs['code']),$data_pending[0])) OR ($rs['code'] == "")){ $rs['status_delivery']="3"; }
			elseif(in_array(trim($rs['code']),$data_delivered[0])){ $rs['status_delivery']="1"; }
			elseif(in_array(trim($rs['code']),$data_undelivered[0])){ $rs['status_delivery']="2"; }
			elseif($rs['code'] == "6"){ $rs['status_delivery']="6"; }
			else{ $rs['status_delivery']="4"; }
		}
		
		return $rs;
	}// end GetValDeliveryResponse
	
	public static function getSleep($prefix, $service){
		$rs		= 0;
		$srvc	= ucwords(strtolower($service));
		
		switch($prefix){
			case "tsel":
				$vsleep="loopSendTsel".$srvc;
				$rs=$GLOBALS[$vsleep];
			break;
			case "isat";
				$vsleep="loopSendIsat".$srvc;
				$rs=$GLOBALS[$vsleep];
			break;
			case "xl";
				$vsleep="loopSendXl".$srvc;
				$rs=$GLOBALS[$vsleep];
			break;
			case "other";
				$vsleep="loopSendOther".$srvc;
				$rs=$GLOBALS[$vsleep];
			break;
		}// end switch
		
		return $rs;
	}// end getSleep
	
	public static function getEmailResponse($dt){
		$rs = array();
		$dtpars = json_decode($dt);
		
		$rs["msgid"] = $dtpars->messages[0]->messageId;
		$rs["sts"] = 0;
		if(strtolower($dtpars->messages[0]->status->description) == "message sent to next instance"){
			$rs["sts"] = 1;
		}
		
		return $rs;
	}// end getEmailResponse
	
	public static function getEmailSts($dt){
		$rs = "";
		$data = strtolower($dt);
		switch($data){
			case "2":case "5": $rs = "1"; break;
			case "3":case "7":case "26": $rs = "3"; break;
			case "opened": $rs = "7"; break;
			case "clicked": $rs = "8"; break;
			case "6": $rs = "6"; break;
			default:
				$rs = "2";
		}// end switch
		
		return $rs;
	}// end getEmailSts
	
	public static function getWhatsappSts($dt){
		$rs = "";
		$data = strtolower($dt);
		switch($data){
			case "delivered": $rs = "1"; break;
			case "undelivered": $rs = "2"; break;
			case "read": $rs = "9"; break;
			default:
				$rs = "6";
		}// end switch
		
		return $rs;
	}// end getWhatsappSts
}
?>
